package org.kun.controller;

import static org.junit.Assert.*;

import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ram.domain.BoardVO;
import org.ram.domain.SearchVO;
import org.ram.persistence.SearchBoardDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations ={"file:src/main/webapp/WEB-INF/spring/**/*-context.xml"})
public class SearchDAOTest {

	private static final Logger logger= LoggerFactory.getLogger(SearchDAOTest.class);
	
	@Inject
	SearchBoardDAO dao;
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() throws Exception{

		logger.info(""+dao);
		
		SearchVO vo = new SearchVO();
		vo.setPage(1);
		vo.setStype("tc");
		vo.setKeyword("a");
		
		List<BoardVO> list = dao.search(vo);
		
		logger.info(list.toString());
	}

}
