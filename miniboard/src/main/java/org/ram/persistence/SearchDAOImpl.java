package org.ram.persistence;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.ram.domain.BoardVO;
import org.ram.domain.SearchVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class SearchDAOImpl implements SearchBoardDAO {

	@Inject
	private SqlSession session;
	
	private static String namespace="org.ram.mapper.SearchBoardMapper";
	
	private static final Logger logger = LoggerFactory.getLogger(SearchDAOImpl.class);

	
	@Override
	public List<BoardVO> search(SearchVO vo) throws Exception {
		
		logger.info("search.............................");
		logger.info(""+vo);
		return session.selectList(namespace+".search1",vo);
	}
	@Override
	public Integer searchcount(SearchVO vo) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne(namespace+".searchCount",vo);
	}
}
