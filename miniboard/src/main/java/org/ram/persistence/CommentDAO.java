package org.ram.persistence;

import java.util.List;

import org.ram.domain.CommentVO;


public interface CommentDAO {

	
	public List<CommentVO> list() throws Exception;
	
	public List<CommentVO> bnoList(Integer bno) throws Exception;
	
	
	public void newWritiong(CommentVO vo)throws Exception;
	public void firstWriting(CommentVO vo)throws Exception;
	
	public Integer check(CommentVO vo) throws Exception;
	
	//널이면 이거 한번
	public void checkNULL(CommentVO vo)throws Exception;
	
	//널이 아니면 이거 두개 순서대로 넣어주기 
	public void checkNotUPDATE(CommentVO vo)throws Exception;
	public void checkNotINSERT(CommentVO vo)throws Exception;
	
	public void lieDelete(Integer cno)throws Exception;
	public void lieDelete2()throws Exception;
	
	public Integer total(Integer bno)throws Exception;
	
	public void boardCommnetNumUPDATE(Integer bno)throws Exception;
	
}
