package org.ram.persistence;

import java.util.List;

import org.ram.domain.BoardVO;
import org.ram.domain.SearchVO;

public interface SearchBoardDAO {

	public List<BoardVO>search(SearchVO vo)throws Exception;
	public Integer searchcount(SearchVO vo)throws Exception;
}
