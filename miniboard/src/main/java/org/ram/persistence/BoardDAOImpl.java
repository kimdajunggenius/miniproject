package org.ram.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.mybatis.spring.SqlSessionTemplate;
import org.ram.domain.BoardVO;
import org.springframework.stereotype.Repository;

@Repository
public class BoardDAOImpl implements BoardDAO {
	
	@Inject
	private SqlSessionTemplate sqlsession;

	@Override
	public void create(BoardVO vo) throws Exception {
		sqlsession.insert("org.ram.persistence.boardMapper.create", vo);

	}

	@Override
	public BoardVO read(Integer bno) throws Exception {
		// TODO Auto-generated method stub
		return sqlsession.selectOne("org.ram.persistence.boardMapper.read", bno);
	}

	@Override
	public void update(BoardVO vo) throws Exception {
		sqlsession.update("org.ram.persistence.boardMapper.update", vo);
	}

	@Override
	public void delete(Integer bno) throws Exception {
		sqlsession.delete("org.ram.persistence.boardMapper.delete", bno);

	}

	@Override
	public List<BoardVO> list(Integer page) throws Exception {
		
		page = (page-1)*10;
		return sqlsession.selectList("org.ram.persistence.boardMapper.list",page);
		
	}
	@Override
	public Integer total() throws Exception {
		// TODO Auto-generated method stub
		return sqlsession.selectOne("org.ram.persistence.boardMapper.total");
	}

	@Override
	public void lieDelete(Integer bno) throws Exception {
		// TODO Auto-generated method stub
		sqlsession.update("org.ram.persistence.boardMapper.lieDelete",bno);
	}
	@Override
	public void lieDelete2() throws Exception {
		// TODO Auto-generated method stub
		sqlsession.update("org.ram.persistence.boardMapper.lieDelete2");
	}

	

}
