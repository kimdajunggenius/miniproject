package org.ram.persistence;

import org.ram.domain.MemberVO;

public interface MemberDAO {

	public void create(MemberVO vo)throws Exception;
	public MemberVO read(MemberVO vo)throws Exception;
	public void update(MemberVO vo)throws Exception;
	public void delete(String userid)throws Exception;
 
/*	public MemberVO checkID(String userid)throws Exception;*/
	
	
}
