package org.ram.persistence;

import java.util.List;

import javax.inject.Inject;

import org.mybatis.spring.SqlSessionTemplate;
import org.ram.domain.CommentVO;
import org.springframework.stereotype.Repository;

@Repository
public class CommentDAOImpl implements CommentDAO {

	@Inject
	SqlSessionTemplate sqlSession;
	
	@Override
	public List<CommentVO> list() throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.selectList("org.ram.persistence.commentMapper.list");
	}

	@Override
	public List<CommentVO> bnoList(Integer bno) throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.selectList("org.ram.persistence.commentMapper.bnoList",bno);
	}
	
	@Override
	public void newWritiong(CommentVO vo) throws Exception {
		// TODO Auto-generated method stub
		sqlSession.insert("org.ram.persistence.commentMapper.newWriting",vo);
	}

	@Override
	public Integer check(CommentVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("org.ram.persistence.commentMapper.check",vo);
	}

	@Override
	public void checkNULL(CommentVO vo) throws Exception {
		// TODO Auto-generated method stub
		sqlSession.insert("org.ram.persistence.commentMapper.checkNULL",vo);
	}

	@Override
	public void checkNotUPDATE(CommentVO vo) throws Exception {
		// TODO Auto-generated method stub
		sqlSession.update("org.ram.persistence.commentMapper.checkNotUPDATE",vo);
	}

	@Override
	public void checkNotINSERT(CommentVO vo) throws Exception {
		// TODO Auto-generated method stub
		sqlSession.insert("org.ram.persistence.commentMapper.checkNotINSERT",vo);
	}

	@Override
	public void lieDelete(Integer cno) throws Exception {
		// TODO Auto-generated method stub
		sqlSession.update("org.ram.persistence.commentMapper.lieDelete",cno);
	}
	@Override
	public void lieDelete2() throws Exception {
		// TODO Auto-generated method stub
		sqlSession.update("org.ram.persistence.commentMapper.lieDelete2");
	}
	
	@Override
	public Integer total(Integer bno) throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("org.ram.persistence.commentMapper.total",bno);
	}
	
	@Override
	public void firstWriting(CommentVO vo) throws Exception {
		// TODO Auto-generated method stub
		sqlSession.insert("org.ram.persistence.commentMapper.firstWriting",vo);
	}
	@Override
	public void boardCommnetNumUPDATE(Integer bno) throws Exception {
		// TODO Auto-generated method stub
	
		sqlSession.update("org.ram.persistence.commentMapper.boardCommnetNumUPDATE",bno); 
	}
}

