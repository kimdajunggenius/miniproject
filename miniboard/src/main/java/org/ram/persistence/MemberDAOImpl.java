package org.ram.persistence;

import javax.inject.Inject;

import org.mybatis.spring.SqlSessionTemplate;
import org.ram.domain.MemberVO;
import org.springframework.stereotype.Repository;

@Repository
public class MemberDAOImpl implements MemberDAO {

	@Inject
	private SqlSessionTemplate sqlsession;

	@Override
	public void create(MemberVO vo) throws Exception {
		sqlsession.insert("org.ram.persistence.memberMapper.create", vo);

	}

	@Override
	public MemberVO read(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sqlsession.selectOne("org.ram.persistence.memberMapper.read", vo);
	}

	@Override
	public void update(MemberVO vo) throws Exception {
		sqlsession.update("org.ram.persistence.memberMapper.update", vo);

	}

	@Override
	public void delete(String userid) throws Exception {

		sqlsession.delete("org.ram.persistence.memberMapper.delete", userid);

	}
	
	/*@Override
	public MemberVO checkID(String userid) throws Exception {
		// TODO Auto-generated method stub
		return sqlsession.selectOne("org.ram.persistence.memberMapper.checkid",userid);
	}*/
 
}
