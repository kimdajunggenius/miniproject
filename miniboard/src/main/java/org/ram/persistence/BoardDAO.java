package org.ram.persistence;

import java.util.List;

import org.ram.domain.BoardVO;

public interface BoardDAO {

	public void create(BoardVO vo)throws Exception;
	public BoardVO read(Integer bno)throws Exception;
	public void update(BoardVO vo)throws Exception;
	public void delete(Integer bno)throws Exception;
	public List<BoardVO> list(Integer page)throws Exception;
	public Integer total()throws Exception;
	
	public void lieDelete(Integer bno)throws Exception;
	public void lieDelete2()throws Exception;
	
}
