package org.ram.domain;

import java.sql.Date;

public class BoardVO {
	
	private Integer bno,hits,commentnum;
	private String writer,content,title,state;
	private Date regdate,modifydate;
	public Integer getBno() {
		return bno;
	}
	public void setBno(Integer bno) {
		this.bno = bno;
	}
	public Integer getHits() {
		return hits;
	}
	public void setHits(Integer hits) {
		this.hits = hits;
	}
	public Integer getCommentnum() {
		return commentnum;
	}
	public void setCommentnum(Integer commentnum) {
		this.commentnum = commentnum;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getModifydate() {
		return modifydate;
	}
	public void setModifydate(Date modifydate) {
		this.modifydate = modifydate;
	}
	@Override
	public String toString() {
		return "BoardVO [bno=" + bno + ", hits=" + hits + ", commentnum=" + commentnum + ", writer=" + writer
				+ ", content=" + content + ", title=" + title + ", state=" + state + ", regdate=" + regdate
				+ ", modifydate=" + modifydate + "]";
	}
	
	

}
