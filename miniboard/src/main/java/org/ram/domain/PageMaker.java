package org.ram.domain;

public class PageMaker {

	private int page;
	private int totalCount;
	
	private boolean prev, next;
	
	private int start, end;
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		
		if(page < 1){
			this.page = 1;
			return;
		}
		
		this.page = page;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {

		if(totalCount < 1){
			return;
		}
		
		this.totalCount = totalCount;
		
		calcPage();
	}
	private void calcPage() {
		
		int tempEnd = (int)(Math.ceil(page/10.0) * 10);
		
		this.start = tempEnd - 9; 
		
		if(tempEnd * 10 > this.totalCount){
			this.end = (int)Math.ceil(this.totalCount / 10.0);
		}else{
			this.end = tempEnd;
		}
		
		this.prev = this.start != 1;
		
		this.next = this.end * 10 < this.totalCount;
		
		
		
	}
	
	
	public boolean isPrev() {
		return prev;
	}
	public void setPrev(boolean prev) {
		this.prev = prev;
	}
	public boolean isNext() {
		return next;
	}
	public void setNext(boolean next) {
		this.next = next;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	@Override
	public String toString() {
		return "PageMaker [page=" + page + ", totalCount=" + totalCount + ", prev=" + prev + ", next=" + next
				+ ", start=" + start + ", end=" + end + "]";
	}
	

	
}
