package org.ram.domain;

public class SearchVO {

	private Integer page;
	private String stype;
	private String keyword;
	
	
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public String getStype() {
		return stype;
	}
	public void setStype(String stype) {
		this.stype = stype;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	@Override
	public String toString() {
		return "SearchVO [page=" + page + ", stype=" + stype + ", keyword=" + keyword + "]";
	}
	
	
	
}
