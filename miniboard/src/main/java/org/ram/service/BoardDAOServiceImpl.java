package org.ram.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.ram.domain.BoardVO;
import org.ram.domain.PageMaker;
import org.ram.domain.SearchVO;
import org.ram.persistence.BoardDAO;
import org.ram.persistence.SearchBoardDAO;
import org.springframework.stereotype.Service;

@Service
public class BoardDAOServiceImpl implements BoardDAOService {

	@Inject
	private BoardDAO bdao;
	
	@Inject
	private SearchBoardDAO sdao;
	
	@Override
	public void create(BoardVO vo) throws Exception {
		// TODO Auto-generated method stub
		bdao.create(vo);
	}

	@Override
	public BoardVO read(Integer bno) throws Exception {
		// TODO Auto-generated method stub
		return bdao.read(bno);
	}

	@Override
	public void update(BoardVO vo) throws Exception {
		// TODO Auto-generated method stub
		bdao.update(vo);
	}

	@Override
	public void delete(Integer bno) throws Exception {
		// TODO Auto-generated method stub
		bdao.delete(bno);
	}

	@Override
	public List<BoardVO> list(Integer page) throws Exception {
		// TODO Auto-generated method stub
		return bdao.list(page);
	}
	@Override
	public Integer total() throws Exception {
		// TODO Auto-generated method stub
		return bdao.total();
	}
	
	@Override
	public void lieDelete(Integer bno) throws Exception {
		// TODO Auto-generated method stub
		bdao.lieDelete(bno);
	}
	@Override
	public void lieDelete2() throws Exception {
		// TODO Auto-generated method stub
		bdao.lieDelete2();
	}
	@Override
	public void fullLieDelete(Integer bno) throws Exception {
		// TODO Auto-generated method stub
		bdao.lieDelete(bno);
		
		bdao.lieDelete2();
	}
	
	@Override
	public List<BoardVO> search(SearchVO svo) throws Exception {
		// TODO Auto-generated method stub
		return sdao.search(svo);
	}
	
	@Override
	public Map<String, Object> search2(SearchVO svo) throws Exception {
		// TODO Auto-generated method stub
		if(svo.getPage()==null)
		{
			svo.setPage(1);
		}
		Map<String,Object> map = new HashMap<String, Object>();
		PageMaker pageMaker = new PageMaker();
		pageMaker.setPage(svo.getPage());
		pageMaker.setTotalCount(sdao.searchcount(svo));		
		map.put("pageMaker", pageMaker);
		svo.setPage( (svo.getPage()-1)*10);
		map.put("pageList", sdao.search(svo));
		
		return map;
	}
}
