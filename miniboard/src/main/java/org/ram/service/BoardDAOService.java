package org.ram.service;

import java.util.List;
import java.util.Map;

import org.ram.domain.BoardVO;
import org.ram.domain.SearchVO;

public interface BoardDAOService {


	public void create(BoardVO vo)throws Exception;
	public BoardVO read(Integer bno)throws Exception;
	public void update(BoardVO vo)throws Exception;
	public void delete(Integer bno)throws Exception;
	public List<BoardVO> list(Integer page)throws Exception;
	public Integer total()throws Exception;
	
	public void lieDelete(Integer bno)throws Exception;
	public void lieDelete2()throws Exception;
	
	public void fullLieDelete(Integer bno)throws Exception;
	
	public List<BoardVO>search(SearchVO svo)throws Exception;
	
	public Map<String,Object> search2(SearchVO svo)throws Exception;
}
