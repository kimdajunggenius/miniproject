package org.ram.service;

import org.ram.domain.MemberVO;
import org.springframework.web.servlet.ModelAndView;



public interface MemberDAOService {
	
	public void create(MemberVO vo)throws Exception;
	public MemberVO read(MemberVO vo)throws Exception;
	public void update(MemberVO vo)throws Exception;
	public void delete(String userid)throws Exception;
	
	public String loginCheck(MemberVO vo)throws Exception;
	
	
}
