package org.ram.service;

import java.util.List;

import org.ram.domain.CommentVO;

public interface CommentDAOService {

	
	public List<CommentVO> bnoList(Integer bno) throws Exception;

	public void firstWriting(CommentVO vo)throws Exception;

	
	public Integer total(Integer bno)throws Exception;
	
	
	public void writerComment(CommentVO vo)throws Exception;	
	
	/*public void boardCommnetNumUPDATE(Integer bno)throws Exception;*/
}
