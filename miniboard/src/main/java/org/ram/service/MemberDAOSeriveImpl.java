package org.ram.service;

import javax.inject.Inject;

import org.ram.domain.MemberVO;
import org.ram.persistence.MemberDAO;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

@Service
public class MemberDAOSeriveImpl implements MemberDAOService {

	@Inject
	MemberDAO mdao;
	
	@Override
	public void create(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub

		mdao.create(vo);
	}

	@Override
	public MemberVO read(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		return mdao.read(vo);
	}

	@Override
	public void update(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		mdao.update(vo);
	}

	@Override
	public void delete(String userid) throws Exception {
		// TODO Auto-generated method stub
		mdao.delete(userid);
	}
	@Override
	public String loginCheck(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		MemberVO vo2 = mdao.read(vo);
		System.out.println("LOGIN CHECK : "+ vo2);
		if(vo2==null){
			return "null";
		}else{
			return vo2.getUserid();
		}
		
		
	}
}
