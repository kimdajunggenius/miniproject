package org.ram.controller;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.ram.domain.CommentVO;
import org.ram.domain.PageMaker;
import org.ram.service.CommentDAOService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/comment/")
public class CommentController {
	
	@Inject
	CommentDAOService cdao;
	
	@CrossOrigin
	@RequestMapping("/all/")
	public Map<String,Object> list()throws Exception{
		Map<String,Object>map = new HashMap<String, Object>();
		map.put("commentList",cdao.bnoList(1));
		
		return map; 
	}
	@CrossOrigin
	@RequestMapping("/all/{bno}")
	public Map<String,Object> list(@PathVariable("bno")Integer bno)throws Exception{
		Map<String,Object>map = new HashMap<String, Object>();
		PageMaker pageMaker = new PageMaker();
		pageMaker.setPage(1);
		
		map.put("commentList",cdao.bnoList(bno));
		return map; 
	}
	
	
	@CrossOrigin
	@RequestMapping(value="/",method=RequestMethod.POST)
	public void newComment(@RequestBody CommentVO vo)throws Exception{
		
		cdao.writerComment(vo);
		
	}
	@CrossOrigin
	@RequestMapping(value="/new/",method=RequestMethod.POST)
	public void firstNewCommnet(@RequestBody CommentVO vo)throws Exception{
		System.out.println("들어오노 반으잉없노"+vo);
		cdao.firstWriting(vo);
	}

}
