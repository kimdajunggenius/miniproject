package org.ram.controller;

import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.type.IntegerTypeHandler;
import org.ram.domain.BoardVO;
import org.ram.domain.PageMaker;
import org.ram.domain.SearchVO;
import org.ram.service.BoardDAOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/board2/")
public class BoardController2 {

	private static final Logger log = LoggerFactory.getLogger(BoardController2.class);
	
	@Inject
	BoardDAOService bdao;
	
	@RequestMapping(value = "/view/{bno}", method = RequestMethod.GET)
	public String view(@PathVariable("bno") Integer bno,Integer page,@CookieValue("loginID")String cookieID ,Model model)throws Exception{
		
		
		model.addAttribute("oneList",bdao.read(bno));
		model.addAttribute("cookieID",cookieID);
		
		return "view";
	}
	@RequestMapping(value = "/home/", method = RequestMethod.GET)
	public void fhome()throws Exception{	
	
	}
	@RequestMapping(value = "/home/{page}", method = RequestMethod.GET)
	public String home(@PathVariable("page")Integer page,@CookieValue("loginID")String cookieID , Model model)throws Exception{
		
		PageMaker pageMaker = new PageMaker();
		pageMaker.setPage(page);
		pageMaker.setTotalCount(bdao.total());
		
		model.addAttribute("pageMaker", pageMaker);
		model.addAttribute("pageList",bdao.list(page));
		model.addAttribute("cookieID",cookieID);
				

		
		return "home";
	}
		

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home2(SearchVO svo,@CookieValue("loginID")String cookieID , Model model)throws Exception{		
		Map<String,Object>map = bdao.search2(svo);

		model.addAttribute("pageMaker",map.get("pageMaker"));
		model.addAttribute("pageList",map.get("pageList"));
		model.addAttribute("cookieID",cookieID);
				

		
		return "home";
	}

	@RequestMapping(value = "/board", method = RequestMethod.GET)
	public String post(@CookieValue("loginID")String cookieID,Model model)throws Exception{
		
		model.addAttribute("cookieID",cookieID);
		return "boardWrite";
	}
	
	//글쓰기
	@RequestMapping(value = "/board_write", method = RequestMethod.POST)
	public ModelAndView writePOST(BoardVO vo)throws Exception{
		
		bdao.create(vo);
		ModelAndView mv = new ModelAndView("redirect:/board2/home/");
		     
		return mv;		
	}

	@Transactional
	@RequestMapping(value = "/delete/{page}&{bno}", method = RequestMethod.GET)
	public ModelAndView deleteGET(@PathVariable("page")Integer page,@PathVariable("bno")Integer bno)throws Exception{

		bdao.fullLieDelete(bno);
		
		ModelAndView mv = new ModelAndView("redirect:/board2/home/"+page);
		return mv;
	}
	
	//수정
	@RequestMapping(value="/board_modify/",method=RequestMethod.POST)
	public ModelAndView modifyPOST(BoardVO vo,Integer page)throws Exception{
		

		bdao.update(vo);
		
		ModelAndView mv = new ModelAndView("redirect:/board2/home/"+page);
		return mv;
	}
	@RequestMapping(value="/board_modify/{page}&{bno}",method=RequestMethod.GET)
	public String modifyGET(@PathVariable("page")Integer page,@PathVariable("bno")Integer bno,Model model)throws Exception{

		
		model.addAttribute("oneView",bdao.read(bno));
				
		return "modify";
	}
	

	
}
