package org.ram.controller;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.ram.domain.PageMaker;
import org.ram.service.BoardDAOService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/board/")
public class BoardController {

	@Inject
	BoardDAOService bdao;
	
	@CrossOrigin
	@RequestMapping(value="/all/{page}",method=RequestMethod.GET)
	public Map<String,Object> pageList(@PathVariable("page") Integer page)throws Exception{
		Map<String,Object> map = new HashMap<String, Object>();
		PageMaker pageMaker = new PageMaker();
		pageMaker.setPage(page);
		pageMaker.setTotalCount(bdao.total());
		map.put("pageList", bdao.list(page));
		map.put("pageMaker",pageMaker);		
		return map;	
	}
	
	@RequestMapping(value="/view",method=RequestMethod.GET)
	public String view()throws Exception{
		
		System.out.println("들어오니;;;");
		return "view";
	}
	
}
