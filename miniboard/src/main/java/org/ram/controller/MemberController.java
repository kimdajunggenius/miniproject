package org.ram.controller;

import javax.inject.Inject;

import org.ram.domain.MemberVO;
import org.ram.service.MemberDAOService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



@Controller
@RequestMapping("/member/")
public class MemberController {

	@Inject
	MemberDAOService mdao;
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String loginGET()throws Exception{
		
		return "login";
		
	}

	
	@RequestMapping(value="/login/",method=RequestMethod.POST)
	public String loginPOST(MemberVO vo,RedirectAttributes rttr,Model model)throws Exception{
	
		/*return "<script>eval('window.alert(\"hello world\")');</script>";*/
//		return "<script>eval('window.location=/board2/home/');</script>";
		
	
		ModelAndView model1 = new ModelAndView();
		String check=mdao.loginCheck(vo);
		
		if(check=="null"){
			rttr.addFlashAttribute("result", "fail");
			/*model1=new ModelAndView("redirect:/member/");
			return model1;*/
			return "redirect:/member/";
		}else{
			
			model.addAttribute("loginID",check);
//			model1.addObject("loginID",check);
			
			/*model1=new ModelAndView("redirect:/board2/home/");
			model.addAttribute("LoginID",check);
			return model1;*/
			return "/member/login";
//			return "redirect:/board2/home/";
		}
		
		
		/*return "login";*/
		
	}
//	@RequestMapping(value="/login/",method=RequestMethod.POST, produces="text/html")
//	public @ResponseBody String loginPOST2(MemberVO vo,RedirectAttributes rttr)throws Exception{
//		
//		return null;
//		/*return "<script>eval('window.alert(\"hello world\")');</script>";*/
////		return "<script>eval('window.location=/board2/home/');</script>";
//		
////		System.out.println("loginPOST에서"+vo);
////		
////		ModelAndView model1 = new ModelAndView();
////		String check=mdao.loginCheck(vo);
////		System.out.println("체크에 모가 뜨나 보자잉? "+check);
////		if(check=="null"){
////			rttr.addFlashAttribute("result", "fail");
////			/*model1=new ModelAndView("redirect:/member/");
////			return model1;*/
////			/*return "redirect:/member/";*/
////		}else{
////			System.out.println("먼저 들어오자누 너 근데 널로떨어지니?? : "+check);
////			//model1.addAttribute("loginID",check);
////			model1.addObject("loginID",check);
////			System.out.println("먼저 들어오자누 너 근데 널로떨어지니?? 왜 모델에 안들어가니 : "+check);
////			/*model1=new ModelAndView("redirect:/board2/home/");
////			model.addAttribute("LoginID",check);
////			return model1;*/
////			/*return "redirect:/member/";*/
////		}
////		
////		return model1;
////		/*return "login";*/
//		
//	}
	
	@RequestMapping(value="/register/",method=RequestMethod.GET)
	public void registerGET()throws Exception{
		
	}
	@RequestMapping(value="/register/",method=RequestMethod.POST)
	public ModelAndView registerPOST(MemberVO vo)throws Exception{
		
		
		mdao.create(vo);
		ModelAndView model=new ModelAndView("redirect:/member/");
		return model;
	}
	@RequestMapping(value="/logout/",method=RequestMethod.GET)
	public String logoutGET(MemberVO vo)throws Exception{
						
		return "redirect:/member/";
	}
	
}
