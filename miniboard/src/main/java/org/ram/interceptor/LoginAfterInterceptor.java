package org.ram.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.CookieGenerator;

public class LoginAfterInterceptor extends HandlerInterceptorAdapter{

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		
		Object obj=modelAndView.getModel().get("loginID");
		
		if(obj!=null)
		{
		
			CookieGenerator cookie = new CookieGenerator();
			cookie.setCookieMaxAge(60*60);
			cookie.setCookieName("loginID");
			cookie.setCookiePath("/");
			cookie.addCookie(response, (String) obj);
			modelAndView.getModel().put("next", "/board2/home/");
//			response.sendRedirect("/board2/home/");
		}else{
			
		}
		
		
	}

}
