package org.ram.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoginPreInterceptor extends HandlerInterceptorAdapter {


	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub

		
		
		if(request.getCookies()!=null)
		{
			Cookie[] cookie = request.getCookies();
			String loginCookie =null;
			for(Cookie ck : cookie){
				if (ck.getName().equals("loginID")) {
					loginCookie = ck.getValue();					
					break;
				}
			}
			if(loginCookie == null){
				response.sendRedirect("/member/");
				return false;
			}
			
		}else{
			response.sendRedirect("/member/");
			return false;
		}
		

		return true;
	}

}
