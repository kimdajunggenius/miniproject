package org.ram.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.CookieGenerator;

public class LogoutInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		String cookieValue ="";
		
		Cookie[] cookies = request.getCookies();
		if (cookies != null && cookies.length >= 1) {
			for (Cookie c : cookies) {
				if(c.getName().equals("loginID")){
					cookieValue = c.getValue();
					CookieGenerator cook = new CookieGenerator();
					cook.setCookieName(c.getName());
					cook.setCookieMaxAge(0);
					cook.addCookie(response, "");
					break;
				}
			}
			
		}
		return true;
	}
}
