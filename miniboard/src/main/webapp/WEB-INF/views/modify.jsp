<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Dashboard">
<meta name="keyword"
	content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

<title>DASHGUM - Bootstrap Admin Template</title>

<!-- Bootstrap core CSS -->
<link href="/resources/assets/css/bootstrap.css" rel="stylesheet">
<!--external css-->
<link href="/resources/assets/font-awesome/css/font-awesome.css"
	rel="stylesheet" />
<link rel="stylesheet" type="text/css"
	href="/resources/assets/js/gritter/css/jquery.gritter.css" />

<!-- Custom styles for this template -->
<link href="/resources/assets/css/style.css" rel="stylesheet">
<link href="/resources/assets/css/style-responsive.css" rel="stylesheet">
<link rel="stylesheet" href="/resources/assets/css/to-do.css">
</head>

<body>

	<section id="container"> <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
	<!--header start--> <header class="header black-bg">
	<div class="sidebar-toggle-box">
		<div class="fa fa-bars tooltips" data-placement="right"
			data-original-title="Toggle Navigation"></div>
	</div>
	<!--logo start--> <a href="index.html" class="logo"><b>TWO
			JJEONG</b></a>

	<div class="top-menu">
		<ul class="nav pull-right top-menu">
			<li><a class="logout" href="/member/logout/">Logout</a></li>
		</ul>
	</div>
	</header> <!--header end--> <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
	<!--sidebar start--> <aside>
	<div id="sidebar" class="nav-collapse ">
		<!-- sidebar menu start-->
		<ul class="sidebar-menu" id="nav-accordion">

			<p class="centered">
				<a href="profile.html"><img
					src="/resources/assets/img/ui-sam.jpg" class="img-circle"
					width="60"></a>
			</p>
			<h5 class="centered">Marcel Newman</h5>

			<li class="sub-menu dcjq-parent-li"><a href="javascript:;"
				class="dcjq-parent"> <i class="fa fa-desktop"></i> <span>메뉴</span>
					<span class="dcjq-icon"></span></a>
				<ul class="sub" style="display: none;">
					<li><a href="#">홈으로</a></li>
					<li><a href="#">로그아웃</a></li>
					<li><a href="#">Panels</a></li>
				</ul></li>

		</ul>
		<!-- sidebar menu end-->
	</div>
	</aside> <!--sidebar end--> <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
	<!--main content start--> <section id="main-content"> <section
		class="wrapper">

	<div class="row mt">
		<div class="col-lg-12">
			<div class="form-panel">
				<h4 class="mb">
					<i class="fa fa-angle-right"></i> ${oneView.bno}번째 글 수정하기
				</h4>
				<form class="form-horizontal style-form" method="post"
					action="/board2/board_modify/">
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">제목</label>
						<div class="col-sm-10">
							<input type="text" class="form-control round-form" name="title"
								value="${oneView.title}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">내용</label>
						<div class="col-sm-10">
							<textarea style="height: 200px; width: 100%" name="content">${oneView.content}</textarea>
						</div>
					</div>
					<input type="hidden" name="writer" value="${oneView.writer}">
					<input type="hidden" name="page" value="${page}">
					<input type="hidden" name="bno" value="${oneView.bno}">

					<input id="modiEnd" type="submit" class="btn btn-default" value="수정완료">
					<!-- <button id="writeEnd" type="button" class="btn btn-default">등록</button> -->
					<button type="button" class="btn btn-default">취소</button>
				</form>
			</div>
		</div>
		<!-- col-lg-12-->
	</div>
	</div>

	</section> <! --/wrapper --> </section><!-- /MAIN CONTENT --> <!--main content end--> </section>

	<!-- js placed at the end of the document so the pages load faster -->
	<script src="/resources/assets/js/jquery.js"></script>
	<!-- <script src="/resources/assets/js/jjquery-1.8.3.min.js"></script> -->
	<script src="/resources/assets/js/bootstrap.min.js"></script>
	<script class="include" type="text/javascript"
		src="/resources/assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="/resources/assets/js/jquery.scrollTo.min.js"></script>
	<script src="/resources/assets/js/jquery.nicescroll.js"
		type="text/javascript"></script>


	<!--common script for all pages-->
	<script src="/resources/assets/js/common-scripts.js"></script>

	<!--script for this page-->
	<script type="text/javascript"
		src="/resources/assets/js/gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript"
		src="/resources/assets/js/gritter-conf.js"></script>

	<script>
		//custom select box
		$("#modiEnd").on("click", function() {
			console.log("수정 들어온다");

			$(".form-horizontal style-form").submit();
		});
	</script>

</body>
</html>
