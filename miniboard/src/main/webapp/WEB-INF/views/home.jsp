<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Dashboard">
<meta name="keyword"
	content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

<title>Welcome</title>

<!-- Bootstrap core CSS -->
<link href="/resources/assets/css/bootstrap.css" rel="stylesheet">
<!--external css-->
<link href="/resources/assets/font-awesome/css/font-awesome.css"
	rel="stylesheet" />

<!-- Custom styles for this template -->
<link href="/resources/assets/css/style.css" rel="stylesheet">
<link href="/resources/assets/css/style-responsive.css" rel="stylesheet">

</head>


<body>

	<section id="container">
		<!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
		<!--header start-->
		<header class="header black-bg">
			<div class="sidebar-toggle-box">
				<div class="fa fa-bars tooltips" data-placement="right"
					data-original-title="Toggle Navigation"></div>
			</div>
			<!--logo start-->
			<a href="index.html" class="logo"><b>TWOJJEONG</b></a>
			<!--logo end-->
			<div class="nav notify-row" id="top_menu">
				<!--  notification start -->

				<!--  notification end -->
			</div>
			<div class="top-menu">
				<ul class="nav pull-right top-menu">
					<li><a class="logout" href="/member/logout/">Logout</a></li>
				</ul>
			</div>
		</header>
		<!--header end-->

		<!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
		<!--sidebar start-->
		<aside>
			<div id="sidebar" class="nav-collapse ">
				<!-- sidebar menu start-->
				<ul class="sidebar-menu" id="nav-accordion">

					<p class="centered"></p>
					<h5 class="centered">Welcome Home!</h5>
					<h5 class="centered">Jungsoo Hwang</h5>

					<li class="sub-menu dcjq-parent-li"><a href="javascript:;"
						class="dcjq-parent"> <i class="fa fa-desktop"></i> <span>로그인
								/ 로그아웃</span> <span class="dcjq-icon"></span></a></li>
					<li class="sub-menu dcjq-parent-li"><a href="javascript:;"
						class="dcjq-parent"> <i class="fa fa-cogs"></i> <span>회원
								정보</span> <span class="dcjq-icon"></span></a>
						<ul class="sub" style="display: none;">
							<li><a href="#">회원 정보 수정</a></li>
							<li><a href="#">회원 탈퇴</a></li>
						</ul></li>

				</ul>
				<!-- sidebar menu end-->
			</div>
		</aside>
		<!--sidebar end-->

		<!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<h3>
					<i class="fa fa-angle-right"></i> 게시판
				</h3>
				<div class="row">

					<div class="col-md-12">
						<div class="content-panel">
							<h4>
								<i class="fa fa-angle-right"></i> 게시판
							</h4>
							<hr>
							<table class="table">
								<thead>

									<tr>
										<th>#</th>
										<th></th>
										<th>제목</th>
										
										<th>작성자</th>
										<th>regdate</th>
										<th>modifydate</th>
										<th>조회수</th>										
									</tr>
								</thead>
								<tbody>
									<form class="oneViewForm" action="">
										<c:forEach items="${pageList}" var="pList">
											<tr>

												<td><a class="oneView" href="${pList.bno}">${pList.bno}</a></td>
												<td><a class="oneView" href="${pList.bno}"> </a></td>
												<td><a class="oneView" href="${pList.bno}">${pList.title}..[${pList.commentnum}]</a></td>
												
												<td><a class="oneView" href="${pList.bno}">${pList.writer}</a></td>
												<td>${pList.regdate}</td>
												<td>${pList.modifydate}</td>
												<td>${pList.hits}</td>

											</tr>
										</c:forEach>
										<input type="hidden" name="page" value="${pageMaker.page}">
									</form>
								</tbody>

							</table>
							<br />

							<div class="row">
								<div class="col-md-8 col-sm-offset-4">
								<form action="/board2/home">
									<select class="col-md-2" name="stype">
										<option value="t">제목</option>
										<option value="c">내용</option>
										<option value="w">작성자</option>
										<option value="tc">작성자+내용</option>
										<option value="cw">내용+작성자</option>
										<option value="tcw">제목+내용+작성자</option>
									</select> 
								<input class="col-md-4" type="text" placeholder="검색"
										name="keyword"> 
								<input class="col-md-1 btn btn-success" type="submit"
										value="검색하기">
								
										</form>
								</div>
							</div>
								

							


							<center>
								<%-- ${(pageMaker.prev==true?'inline;':'none')} --%>
								<a class="btn btn-default" href='${param.stype==null?(pageMaker.start-1):"home?stype="}${param.stype==null?"":param.stype}
										${param.stype==null?"":"&keyword="}${param.stype==null?"":param.keyword}
										${param.stype==null?"":"&page="}${param.stype==null?"":(pageMaker.start-1)}'
									style="display:${(pageMaker.prev==true?'inline;':'none')} ">
									◀</a>
								<%-- <a class="btn btn-default" href='${idx}'> ◀</a> --%>
								<%-- ${pageMaker.prev==true?block:none} --%>
								<c:forEach begin="${pageMaker.start}" end="${pageMaker.end}"
									var="idx">
									<a
										class="${idx==pageMaker.page?'btn btn-warning':'btn btn-default' }"
										href='${param.stype==null?idx:"home?stype="}${param.stype==null?"":param.stype}
										${param.stype==null?"":"&keyword="}${param.stype==null?"":param.keyword}
										${param.stype==null?"":"&page="}${param.stype==null?"":idx}
										'> ${idx}</a>
								</c:forEach>
								<a class="btn btn-default" href='${param.stype==null?(pageMaker.end+1):"home?stype="}${param.stype==null?"":param.stype}
										${param.stype==null?"":"&keyword="}${param.stype==null?"":param.keyword}
										${param.stype==null?"":"&page="}${param.stype==null?"":(pageMaker.end+1)}'
									style="display:${(pageMaker.next==true?'inline;':'none')} ">▶
								</a> &nbsp;
								<button id="writebtn" type="button" class="btn btn-info">글작성</button>
							</center>
						</div>
						<! --/content-panel -->
					</div>
					<!-- /col-md-12 -->
			</section>
			<! --/wrapper -->
		</section>
		<!-- /MAIN CONTENT -->

		<!--main content end-->

	</section>

	<!-- js placed at the end of the document so the pages load faster -->
	<script src="/resources/assets/js/jquery.js"></script>
	<script src="/resources/assets/js/bootstrap.min.js"></script>
	<script class="include" type="text/javascript"
		src="/resources/assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="/resources/assets/js/jquery.scrollTo.min.js"></script>
	<script src="/resources/assets/js/jquery.nicescroll.js"
		type="text/javascript"></script>


	<!--common script for all pages-->
	<script src="/resources/assets/js/common-scripts.js"></script>

	<!--script for this page-->

	<script>
		//custom select box

		$(document).ready(function() {

			$("#writebtn").on("click", function() {
				self.location = "/board2/board";
			});
			
			$(".oneView").on("click",function(e){
				e.preventDefault();
				console.log("아 이거 뜨냐 불편해죽것네");
				var $this=$(this);
				console.log($this.attr("href"));
				$(".oneViewForm").attr("action","/board2/view/"+$this.attr("href"));
				$(".oneViewForm").submit();
			});

		});
	</script>

</body>
</html>
