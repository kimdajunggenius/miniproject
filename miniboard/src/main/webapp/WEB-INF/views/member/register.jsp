<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Dashboard">
<meta name="keyword"
	content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

<title>DASHGUM - Bootstrap Admin Template</title>

<!-- Bootstrap core CSS -->
<link
	href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.css"
	rel="stylesheet">
<!--external css-->
<link
	href="${pageContext.request.contextPath}/resources/assets/font-awesome/css/font-awesome.css"
	rel="stylesheet" />

<!-- Custom styles for this template -->
<link
	href="${pageContext.request.contextPath}/resources/assets/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/assets/css/style-responsive.css"
	rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<!-- **********************************************************************************************************************************************************
MAIN CONTENT
*********************************************************************************************************************************************************** -->

	<div id="login-page">
		<div class="container">

			<form class="form-login" action="/member/register/" method="post">
				<h2 class="form-login-heading">sign up now</h2>
				<div class="login-wrap">
					<input type="text" class="form-control" name="userid"
						placeholder="User ID" autofocus> <br> 
						<input type="password" class="form-control" name="userpw"
						placeholder="Password"> <br> 
						<input type="text"	class="form-control" name="username" placeholder="User Name"><br>
					<input type="text" class="form-control" name="email" placeholder="User Mail"> <br>
					 <input type="text"	class="form-control" name="phone" placeholder="User Phone">
		<br> <input value="SIGN UP" class="btn btn-theme btn-block"  type="button">
					<!-- 	<i class="fa fa-lock"></i> -->
					
					<input type="button" value="Cancel" class="btn btn-theme btn-block">
				</div>

				<!-- Modal -->
				<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
					tabindex="-1" id="myModal" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">

							<div class="modal-footer">
								<button data-dismiss="modal" class="btn btn-default"
									type="button">Cancel</button>
								<button class="btn btn-theme" type="button">Submit</button>
							</div>
						</div>
					</div>
				</div>
				<!-- modal -->

			</form>

		</div>
	</div>

	<!-- js placed at the end of the document so the pages load faster -->
	<script src="/resources/assets/js/jquery.js"></script>
	<script src="/resources/assets/js/bootstrap.min.js"></script>

	<!--BACKSTRETCH-->
	<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
	<script type="text/javascript"
		src="/resources/assets/js/jquery.backstretch.min.js"></script>
	<script>
    $.backstretch("/resources/assets/img/login-bg.jpg", {speed: 500});

    $(document).ready(function () {
        var $login=$(".form-login");
        var $signup=$(".btn btn-theme btn-block"); 
		
        
        $login.on("click","input[type='button']",function () {
            
            var $this=$(this);

            var btn=$this[0].defaultValue;
            var data={
                userid:$login.find("input[name='userid']").val(),
                userpw:$login.find("input[name='userpw']").val(),
                username:$login.find("input[name='username']").val(),
                email:$login.find("input[name='email']").val(),
                phone:$login.find("input[name='phone']").val()
            };
            if(btn == "SIGN UP")
            {     
	   			if(data.userid=="" ||data.userid=="입력하지 않았습니다."){
	            	
	            	$login.find("input[name='userid']").focus();
	            	$login.find("input[name='userid']").val("입력하지 않았습니다.");
	            	return;
	            }
	            if(data.userpw=="" ||data.userpw=="입력하지 않았습니다."){
	            	
	            	$login.find("input[name='userpw']").focus();
	            	$login.find("input[name='userpw']").val("입력하지 않았습니다.");
	            	return;
	            }
	  			 if(data.username=="" ||data.username=="입력하지 않았습니다."){
	  	          	
	            	$login.find("input[name='username']").focus();
	            	$login.find("input[name='username']").val("입력하지 않았습니다.");
	            	return;
	            }
	            if(data.email=="" ||data.email=="입력하지 않았습니다."){
	            	
	            	$login.find("input[name='email']").focus();
	            	$login.find("input[name='email']").val("입력하지 않았습니다.");
	            	return;
	            }
	            if(data.phone=="" || data.phone=="입력하지 않았습니다."){            	
	            	$login.find("input[name='phone']").focus();
	            	$login.find("input[name='phone']").val("입력하지 않았습니다.");
	            	return;            	
	            }
	            $login.submit(); 
            }else if(btn == "Cancel"){
            	self.location="/member/";            	
            }
            
        });
        
    });



</script>


</body>
</html>
