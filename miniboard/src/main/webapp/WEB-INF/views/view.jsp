<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword"
          content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>DASHGUM - Bootstrap Admin Template</title>

    <!-- Bootstrap core CSS -->
    <link href="/resources/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="/resources/assets/font-awesome/css/font-awesome.css"
          rel="stylesheet" />
    <link rel="stylesheet" type="text/css"
          href="/resources/assets/js/gritter/css/jquery.gritter.css" />

    <!-- Custom styles for this template -->
    <link href="/resources/assets/css/style.css" rel="stylesheet">
    <link href="/resources/assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="/resources/assets/css/to-do.css">
</head>

<body>
<style>
    .comment{
        display: none;
        background-color: chartreuse;
        position: absolute;
        top:50%;
        left: 50%;
        border-radius: 12px;
        border: 5px solid red;

        width: 400px;
        height: 300px;
        padding: 30px;
        margin-left: -200px;
        margin-top: -150px;
        z-index: 1000;
    }

    input[type=button]{
        width: 100px;
        height: 50px;
        background-color: #4CAF50;
        color: white;
        padding: 5px 5px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }
    }
    input[type=text]{
        width: 97%;
        clear: right;

    }
    
	.commentInput{
	display: none;
	} 
</style>

</style>


<div class="comment">
    <input class="comTitle" type="text" name="title" placeholder="타이틀" val="">
    <!-- <input class="comWriter"type="text" name="writer" placeholder="작성자" val=""> -->
    <input data-type="post" type="button" value="작성완료">
    <input type="button" value="취소">
</div>

<section id="container"> <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
    <!--header start--> <header class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right"
                 data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start--> <a href="index.html" class="logo"><b>TWO
        JJEONG</b></a>

        <div class="top-menu">
            <ul class="nav pull-right top-menu">
                <li><a class="logout" href="/member/logout/">Logout</a></li>
            </ul>
        </div>
    </header> <!--header end--> <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
    <!--sidebar start--> <aside>
        <div id="sidebar" class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">

                <p class="centered">
                    <a href="profile.html"><img
                            src="/resources/assets/img/ui-sam.jpg" class="img-circle"
                            width="60"></a>
                </p>
                <h5 class="centered">Marcel Newman</h5>

                <li class="sub-menu dcjq-parent-li"><a href="javascript:;"
                                                       class="dcjq-parent"> <i class="fa fa-desktop"></i> <span>메뉴</span>
                    <span class="dcjq-icon"></span></a>
                    <ul class="sub" style="display: none;">
                        <li><a href="#">홈으로</a></li>
                        <li><a href="#">로그아웃</a></li>
                        <li><a href="#">Panels</a></li>
                    </ul></li>

            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside> <!--sidebar end--> <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
    <!--main content start--> <section id="main-content"> <section
            class="wrapper">
        <div class="row mt">

            <div class="col-lg-12 col-md-12 col-sm-12">
                <! -- ALERTS EXAMPLES -->
                <div class="showback">
                    <h4>
                        <i class="fa fa-angle-right"></i> Board View
                    </h4>
                    <div id="getBno" data-bno="${oneList.bno}" class="alert alert-danger">#${oneList.bno}</div>
                    <div class="alert alert-success">제목: ${oneList.title}</div>
                    <div class="alert alert-info">내용: ${oneList.content}</div>
                    <div class="alert alert-warning">작성자: ${oneList.writer}</div>
                    <div class="alert alert-danger">날짜: ${oneList.regdate}</div>

                    <div class="panel-body">
                        <a class="btn btn-default btn-sm pull-right" href="/board2/board_modify/${param.page}&${oneList.bno}">수정</a>
                        <a id="deletebtn" class="btn btn-default btn-sm pull-right"href="/board2/delete/${param.page}&${oneList.bno}" >삭제</a>
                        <a class="btn btn-default btn-sm pull-right" href="/board2/home/${param.page}">목록</a>
                    </div>
                </div>

            </div>

        </div>

        <div class="row mt">
            <div class="col-md-12">
                <section class="task-panel tasks-widget">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h5>
                                <i class="fa fa-tasks"></i> 댓글보기
                            </h5>
                        </div>
                        <br>
                    </div>
                    <div class="panel-body">
                        <div class="task-content">

                            <ul class="task-list">
                            </ul>
                        </div>

                        <div class=" add-task-row">


                            <form  class="iwant"  action="/comment/new/" method="get">
                                <div class="form-group">                                    
                                    <input type="text" class="form-control" name="content" placeholder="댓글작성 " >
                                    <input type="hidden" name="writer" value="${cookieID}">
                                    <input type="hidden" name="bno" value='${oneList.bno}'>
                                    
                                </div>
                                <button data-type="post" id="newCommentBtn" type="submit" class="btn btn-theme">Sign in</button>
                            </form>

                            <br/>
                            <span class="badge bg-success">이전</span> <span
                                class="badge bg-warning">1</span> <span class="badge bg-warning">2</span>
                            <span class="badge bg-warning">3</span> <span
                                class="badge bg-success">다음</span>
                        </div>
                    </div>
                </section>
            </div>
            <!-- /col-md-12-->
        </div>

    </section> <! --/wrapper --> </section><!-- /MAIN CONTENT --> <!--main content end--> </section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- js placed at the end of the document so the pages load faster -->
<script src="/resources/assets/js/jquery.js"></script>
<!-- <script src="/resources/assets/js/jjquery-1.8.3.min.js"></script> -->
<script src="/resources/assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript"
        src="/resources/assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="/resources/assets/js/jquery.scrollTo.min.js"></script>
<script src="/resources/assets/js/jquery.nicescroll.js"
        type="text/javascript"></script>


<!--common script for all pages-->
<script src="/resources/assets/js/common-scripts.js"></script>

<!--script for this page-->
<script type="text/javascript"
        src="/resources/assets/js/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript"
        src="/resources/assets/js/gritter-conf.js"></script>
<script type="text/javascript"
        src="/resources/assets/js/CommentService.js"></script>


<script>
    //custom select box
    $(document).ready(function(){

    	var $list=$(".task-list");
        var $comment =  $(".comment");
        var url=commentController.getURL();
        
        var bno= $("#getBno").attr("data-bno");
        
        console.log(bno);
        
        commentController.showCommentList(url+bno);

        /* $list.on("click","li",function () {
            var $this=$(this);
            console.log($this.attr("data-cno"));            
             var commentvo={
                    
                    bno:$this.attr("data-bno"),
                    ref:$this.attr("data-ref"),
                    step:$this.attr("data-step"),
                    lvl:$this.attr("data-lvl"),
                    
                }; 
            $comment.show(500);
            $comment.on("click","input",function () {
            	
                var $this2=$(this);
                var btn= $this2[0].defaultValue;
             
                var url2="http://192.168.0.11:8080/comment/";
                
           commentvo.content=$(".comTitle").val();
                
                
                
                commentvo.writer=$(".iwant").find("input[name='writer']").val();

                if(btn == "작성완료"){
                	 
                    commentController.sendData(url2,commentvo,$this2,function(){
                    	
                            console.log("success.......");
                            commentController.showCommentList(url+bno);
                            $(".comTitle").val("");
                            
                    });
                }
                if(btn){
                	commentvo={};
                 	$comment.hide(500);
                }
                
            });
        }); */

       
		$(".iwant").on("click","button",function(e){
			 e.preventDefault(); 
			
			/* $(".form-inline").find("input[name='bno']").val(3); */
			console.log($(".iwant").find("input"));
			console.log($(".iwant").find("input[name='bno']").val());
			console.log($(".iwant").find("input[name='writer']").val());
			 var $this=$(this);
			var url3="http://192.168.0.11:8080/comment/new/";
			 var commentvo={
		                bno:$(".iwant").find("input[name='bno']").val(),
		                content:$(".iwant").find("input[name='content']").val(),
		                writer:$(".iwant").find("input[name='writer']").val()		                
		            };
			 console.log(commentvo);
			 console.log($this.attr("data-type"));
			 console.log(url3);
			 commentController.sendData(url3,commentvo,$this,function(){
             	
                 console.log("success.......");
                 commentController.showCommentList(url+bno);
                 $(".iwant").find("input[name='content']").val("");
             
         });
			 
		});
		
		$(".task-list").on("click","#commentInput",function(){
			
			$(this).find("#commentInput").attr("type","hidden");
			
			console.log($(this));
			$("#commentInput").attr("type","hidden");
			console.log("이건 cno다잉 ",$("#commentInput").attr("data-cna"));
			
			console.log("찍어보기");
		});
		$list.on("click","button",function(){
			var $that= $(this);
			var btnName=$that[0].attributes[0].value;
			if(btnName=='btn btn-danger btn-xs'){
				console.log('삭제버튼');				
			}else if(btnName=='btn btn-success btn-xs'){
				console.log("댓글달기버튼");
				console.log($that.parent().parent().parent().attr("data-cno"));
				var idnum=$that.parent().parent().parent().attr("data-cno");
				var id = $('#commentInput'+idnum+'');
				
				console.log(id);
				/* $(id).hide(500); */
				id.show(500);
				/* id.show(500); */
				
				
			}else{
				console.log("수정버튼");
			}

		});
		$list.on("click","input[type='button']",function(){
			var $that = $(this);
			var url2="http://192.168.0.11:8080/comment/";
			console.log($that.parent());
			var thatP=$that.parent();
			var data={
					
					cno:thatP.attr("data-cno"),
					bno:thatP.attr("data-bno"),
					ref:thatP.attr("data-ref"),
					step:thatP.attr("data-step"),
					lvl:thatP.attr("data-lvl"),
					writer:$(".iwant").find("input[name='writer']").val(),
					content:thatP.find("input[type='text']").val()
			};
			
			commentController.sendData(url2,data,thatP,function(){
            	
                console.log("success.......");
                commentController.showCommentList(url+data.bno);
                console.log(url+data.bno);
                data={};
                
        	});
			
			console.log(data);
			console.log("버튼눌리겟지???");
		});
		
		
        
    });

</script>

</body>
</html>
