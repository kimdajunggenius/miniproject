/**
 * Created by BitCamp on 2016-07-12.
 */
var commentController = (function () {

	
	
    var url="http://192.168.0.11:8080/comment/all/";

    function showCommentList(url2) {

        $.getJSON(url2,function(result){
        	
        	var $list=$(".task-list");
        	var str='';
            var list=result.commentList;
            for(var i in list){
                str+= '<li data-cno="'+list[i].cno+'" data-bno="'+list[i].bno+'" data-ref="'+list[i].ref+'" data-step="'+list[i].step+'" data-lvl="'+list[i].lvl+'"  style="text-indent:'+(20*list[i].lvl)+'px ">'+
                        '<div class="task-title">'+
                        '<span class="task-title-sp">'+(list[i].lvl==0?"":"└>")+list[i].writer + ' : '+list[i].content+'     '+list[i].modifydate+'</span>'+
                        ' <span class="badge bg-warning">new</span>'+
                        '<div class="pull-right hidden-phone">'+
                        '<button class="btn btn-success btn-xs">'+
                        '       <i class=" fa fa-check"></i>'+
                        '</button><button class="btn btn-primary btn-xs">'+
                        '<i class="fa fa-pencil"></i></button>'+
                        '<button class="btn btn-danger btn-xs">'+
                        '<i class="fa fa-trash-o "></i>'+
                        '</button></div></div></li>'+
                        '<li class="commentInput" id="commentInput'+list[i].cno+'" style="display:none"'+
                        'data-type="post" data-cno="'+list[i].cno+'" data-bno="'+list[i].bno+'" data-ref="'+list[i].ref+'" data-step="'+list[i].step+'" data-lvl="'+list[i].lvl+'">'+
                        '<input  class="form-control"  type="text" placeholder="댓글달내용" name="content">'+
                        '<input class="btn btn-theme" type="button" value="댓글달기"></li>' ; 
            }
            $list.html(str);
        });
    }

    function getURL() {
        return url;
    }

    function sendData(geturl,data,$this,success) {

        $.ajax({
            type:$this.attr("data-type"),
            url:geturl,
            data:JSON.stringify(data),
            contentType:"application/json; charset=UTF-8",
            success:success
        });
    }
    
   

    return{
        showCommentList:showCommentList,
        getURL:getURL,
        sendData:sendData
        
    }

})();