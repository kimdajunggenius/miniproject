package org.ram.controller;

import javax.inject.Inject;

import org.gm.domain.EmoticonVO;
import org.gm.persistence.EmoticonDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*-context.xml" })

public class KunTest {

	private static final Logger logger = LoggerFactory.getLogger(KunTest.class);

	@Inject
	EmoticonDAO dao;

	// 전체검색
	@Test
	public void testselectList() throws Exception {
		logger.info("이모티콘 테스트입니다!!!!");
		dao.selectList(1);
	}

	//조건검색(확인하셈요)
	@Test
	public void testselectLike() throws Exception {
		logger.info("이모티콘 조건검색!!");
		String word = "좋아";
		
		EmoticonVO vo = new EmoticonVO();
		//#{emotion}#{checked} #{word}
		vo.setEmotion("좋음");
		vo.setState("N");
		vo.setChecked("N");

		dao.selectLike(vo, 1, word);
	}
	
	
	// 추가
	@Test
	public void testinsert() throws Exception {
		logger.info("이모티콘 테스트입니다!!!!");

		EmoticonVO vo = new EmoticonVO();
		vo.setEmotion("나쁨");
		vo.setEmoticon("C:\bad1.jpg");
		vo.setStage(2);
		vo.setWord1("아니요");
		vo.setWord2("아니");
		vo.setWord3("아니아니아니");

		dao.insert(vo);
	}
	
	// 수정
	@Test
	public void testupdate() throws Exception {
		logger.info("이모티콘 테스트입니다!!!!");

		EmoticonVO vo = new EmoticonVO();
		vo.setEmotion("나쁨");
		vo.setEmoticon("C:\bad1.jpg");
		vo.setStage(2);
		vo.setWord1("아니요");
		vo.setWord2("아니");
		vo.setWord3("아니아니");
		vo.setChecked("N");
		vo.setEmono(3);

		dao.update(vo);
	}
	
	//삭제
	@Test
	public void testDelete() throws Exception{
		logger.info("이모티콘 테스트입니다!!!!");

		EmoticonVO vo = new EmoticonVO();
		vo.setEmono(4);
		
		dao.delete(vo);
	}


}
