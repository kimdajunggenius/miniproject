package org.ram.controller;

import static org.junit.Assert.*;


import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.HistoryVO;
import org.gm.domain.MemberVO;
import org.gm.domain.PageMaker;
import org.gm.domain.SearchCalendarVO;
import org.gm.domain.SearchMemVO;
import org.gm.persistence.ChartDAO;
import org.gm.persistence.LarDAO;
import org.gm.persistence.SearchMemDAO;
import org.gm.service.HistoryDAOService;
import org.gm.service.MemberDAOService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations ={"file:src/main/webapp/WEB-INF/spring/**/*-context.xml"})
public class RamTest {

	@Inject
	private HistoryDAOService hdao;
	
	@Inject
	private LarDAO ldao;
	
	@Inject
	private MemberDAOService mdao;
	
	@Inject
	private SearchMemDAO srdao;
	
	@Inject
	private ChartDAO cdao;
	
	@Test
	public void test()throws Exception {

		ChartVO vo = new ChartVO();
		vo.setMno("dmdm");
		
		System.out.println(hdao.avgChartNowMonthPreList(vo));

	}
	@Test
	public void larTest()throws Exception {
		
		System.out.println(mdao.memLarMedChartList("dmdm","2016","2"));

	}
	
	@Test
	public void CeiTe()throws Exception{
		
		double t2=161;
		System.out.println(t2/10);
		System.out.println((int)Math.ceil((double)t2/10));
		
		PageMaker pageMaker= new PageMaker();
		pageMaker.setPage(12);
		pageMaker.setTotal(195);
		
		System.out.println(pageMaker);
		
	}

	@Test
	public void searchTest()throws Exception{
		SearchMemVO vo = new SearchMemVO();
		
		vo.setGender("gt");
		vo.setAge("at");
		vo.setLocation("lt");
		
		
//		vo.setKeyword("dmdm");
		System.out.println(mdao.searchMem(vo));
//		System.out.println("�˻��� ����� :"+srdao.searchCount(vo));
//		System.out.println("�˻� :"+srdao.searchMem(vo));
	}
	@Test
	public void defaultmem()throws Exception{
		
		System.out.println(mdao.defaultmemLarMedChartList("dmdm"));
	}
	
	@Test
	public void SearchCalTEST()throws Exception{
		SearchCalendarVO vo = new SearchCalendarVO();
		vo.setMno("dmdm");
		vo.setBefore("2016-01-01");
		vo.setAfter("2016-12-01");
		
		System.out.println(mdao.searchCalChart(vo));
//		System.out.println(cdao.searchChartList(vo));
	}
	
	@Test
	public void budgetTEST()throws Exception{
		
		String mno= "dmdm";
		
		System.out.println(cdao.memBudget(mno));
		
//		MemberVO vo =mdao.getMember(mno);
		
//		System.out.println(vo);
		
//		System.out.println(cdao.budGenAgeAvgChart(vo.getGender(),1700000, vo.getBirthday()));
	}
	
	@Test
	public void hisInsertTest()throws Exception{
		/*HistoryVO vo = new HistoryVO();
		SimpleDateFormat date =new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		
		Date todate = date.parse("2016-08-12 17:33:12");
		
		
		
		System.out.println(todate);
		vo.setMno("Ramssang");
		vo.setSname("�ѻ��");
		vo.setPaytype("����");
		vo.setPrice(3000);
		vo.setBtime(todate);
		vo.setMtype("W");
		
		hdao.insert(vo);*/
		
	}
}
