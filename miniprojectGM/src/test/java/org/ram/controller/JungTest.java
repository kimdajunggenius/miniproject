package org.ram.controller;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.gm.domain.StoreVO;
import org.gm.service.MonthlyAverageDAOService;
import org.gm.service.StoreDAOService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations ={"file:src/main/webapp/WEB-INF/spring/**/*-context.xml"})
public class JungTest {

	@Inject
	MonthlyAverageDAOService mdao;
	
	@Inject 
	StoreDAOService sdao;
	

	@Test
	public void testage() throws Exception {
		//월별 평균 나이대별 그래프
		mdao.getMonthlyAge("2016", "1");
		System.out.println("Testing!");
		System.out.println(mdao.toString());
		
	}
	
	@Test
	public void testbudget() throws Exception {
		//월별 평균 예산별 그래프
		mdao.getMonthlyBudget("2016", "1");
		System.out.println("Testing!");
		System.out.println(mdao.toString());
		
	}
	
	@Test
	public void testStoreCount() throws Exception {
		//토탈 카운트
		sdao.getTotalStore();
		System.out.println("Testing!");
		System.out.println(sdao.toString());
		
		
	}
	
	@Test
	public void testStoreList() throws Exception {
		//리스트
		sdao.storeList(2);
		System.out.println("Testing!");
		System.out.println(sdao.toString());
		
		
	}

	@Test
	public void testUpdate() throws Exception {
		//수정
		StoreVO vo = new StoreVO();
		vo.setSno(3);
		vo.setAddress("서울특별시 강남구 역삼동 821-1");
		vo.setSname("GS25 강남메트로점");
		vo.setCstate("y");
		vo.setLar("생활편의");
		vo.setMed("마트/편의점");
		vo.setLat(37.498611610575225);
		vo.setLng(127.02805475354877);
		sdao.updateStore(vo);
		System.out.println("Testing!");
		
		
	}
	
	@Test
	public void testDelete() throws Exception {
		//가게 삭제
		sdao.deleteStore(10);
		
	}
	
	@Test
	public void testNullStore() throws Exception {
		//NULL 있는 가게 찾기
		sdao.storeNullList(1);
		
	}
}
