package org.ram.controller;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.LarVO;
import org.gm.domain.MedVO;
import org.gm.persistence.ChartDAO;
import org.gm.persistence.HistoryDAO;
import org.gm.persistence.LarDAO;
import org.gm.persistence.MedDAO;
import org.gm.service.HistoryDAOService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations ={"file:src/main/webapp/WEB-INF/spring/**/*-context.xml"})
public class HistoryTest {

	@Inject
	private HistoryDAO hdao;

	@Inject
	private LarDAO ldao;

	@Inject
	private MedDAO mdao;

	@Inject
	private HistoryDAOService hsdao;

	@Inject
	private ChartDAO cdao;

	@Test
	public void test()throws Exception {

		System.out.println(hdao.pageList(1).toString());

	}
	@Test
	public void test2()throws Exception {

		List<List<LarVO>>list = new ArrayList<>();
		String month;
		LarVO vo = new LarVO();
		vo.setLar("식비");
		vo.setMno("dmdm");
		vo.setYear("2016");
		for(int i=1; i<=31; i++){

			if(i<10){
				month="0"+i;				
			}else{
				month=""+i;				
			}
			vo.setMonth(month);

			//			list.add(hdao.larList(vo));
			ldao.insert(hdao.larList(vo));
		}		


		System.out.println(list);

	}
	@Test
	public void AllInsertLarMedChart()throws Exception{
		String month;
		String mno="qwerty@naver.com";
		String year="2016";
		String[] lar={"식비","학비","문화생활비","건강관리비","의류미용비","차량유지비","주거생활비","유흥비"
				,"생활편의"};

		LarVO vo = new LarVO();
		vo.setMno(mno); 
		vo.setYear(year);

		String[] med={"식사/간식","카페","식재료","영화/공연","게임/음악","도서","쇼핑","여행/숙박","운동/다이어트",
				"병원비/약값","요양비","의류/잡화","미용","세탁비","기름값","정비/세차","주차/통행",
				"생활용품","학원/강의","술집","취미생활","마트/편의점","전자제품"};

		MedVO mvo = new MedVO();

		mvo.setMno(mno);
		mvo.setYear(year);


		for(int j=0;j<lar.length;j++){
			vo.setLar(lar[j]);
			for(int i=1; i<=12; i++){

				if(i<10){
					month="0"+i;				
				}else{
					month=""+i;				
				}
				try{
					vo.setMonth(month);
					ldao.insert(hdao.larList(vo));
				}catch(Exception e){
					continue;
				}
			}		
		}

		for(int j=0;j<med.length;j++){
			mvo.setMed(med[j]);
			for(int i=1; i<=12; i++){

				if(i<10){
					month="0"+i;				
				}else{
					month=""+i;				
				}
				try{
					mvo.setMonth(month);					 
					mdao.insert(hdao.medList(mvo));

				}catch(Exception e){
					continue;
				}

			}		
		}
		
		ChartVO cvo = new ChartVO();
		cvo.setMno(mno);
		cvo.setYear(year);
	 
		for(int i=1; i<13; i++){
			if(i<10){
				cvo.setMonth("0"+i);
			}else{
				cvo.setMonth(""+i);	
			}
			cdao.chartInsert(cdao.chartMonList(cvo));
		}
		
		for(int i=1; i<13; i++){
			if(i<10){
				cvo.setMonth(""+i);
			}else{
				cvo.setMonth(""+i);	
			}
			//			System.out.println(cdao.chartMonList(vo));
			cdao.totalUpdate(cvo);
		}
		
		

	}


	@Test
	public void larInsertTest()throws Exception {

		String month;
		String[] lar={"식비","학비","문화생활비","건강관리비","의류미용비","차량유지비","주거생활비","유흥비"
				,"생활편의"};

		LarVO vo = new LarVO();
		vo.setMno("hello2@naver.com");
		vo.setYear("2016");
		for(int j=0;j<lar.length;j++){
			vo.setLar(lar[j]);
			for(int i=1; i<=12; i++){

				if(i<10){
					month="0"+i;				
				}else{
					month=""+i;				
				}
				vo.setMonth(month);
				ldao.insert(hdao.larList(vo));
			}		
		}

	}

	@Test
	public void medInsertTestDmdm()throws Exception {

		List<MedVO>list = new ArrayList<>();
		String month;
		String[] med={"식사/간식","카페","식재료","영화/공연","게임/음악","도서","쇼핑","여행/숙박","운동/다이어트",
				"병원비/약값","요양비","의류/잡화","미용","세탁비","기름값","정비/세차","주차/통행",
				"생활용품","학원/강의","술집","취미생활","마트/편의점","전자제품"};

		MedVO vo = new MedVO();

		vo.setMno("hello2@naver.com");
		vo.setYear("2016");
		for(int j=0;j<med.length;j++){
			vo.setMed(med[j]);
			for(int i=1; i<=12; i++){

				if(i<10){
					month="0"+i;				
				}else{
					month=""+i;				
				}
				try{
					vo.setMonth(month);
					list.add(hdao.medList(vo));
					mdao.insert(hdao.medList(vo));

				}catch(Exception e){
					continue;
				}

			}		
		}



		System.out.println(list);

	}//으으꺼 중분류

	@Test
	public void chartInsertTest()throws Exception{

		ChartVO cvo = new ChartVO();
		cvo.setMno("dodo2@naver.com");
		cvo.setYear("2016");
		cvo.setBudget(1100000);
		for(int i=1; i<13; i++){
			if(i<10){
				cvo.setMonth("0"+i);
			}else{
				cvo.setMonth(""+i);	
			}
			cdao.chartInsert(cdao.chartMonList(cvo));
		}
		
	}

	@Test
	public void chartTotalUpdateTest()throws Exception{

		ChartVO cvo = new ChartVO();
		cvo.setMno("hello2@naver.com");
		cvo.setYear("2016");

		for(int i=1; i<13; i++){
			if(i<10){
				cvo.setMonth(""+i);
			}else{
				cvo.setMonth(""+i);	
			}
			//			System.out.println(cdao.chartMonList(vo));
			cdao.totalUpdate(cvo);
		}
	}

	@Test
	public void chartMonthListTest()throws Exception{

		ChartVO vo = new ChartVO();
		vo.setMno("dmdm");
		vo.setYear("2016");
		vo.setMonth("1");

		System.out.println(cdao.chartMonthList(vo));
	}
	@Test
	public void chartAllListTest()throws Exception{

		ChartVO vo = new ChartVO();
		vo.setMno("dmdm");


		System.out.println(cdao.chartAllList(vo));
	}




}
