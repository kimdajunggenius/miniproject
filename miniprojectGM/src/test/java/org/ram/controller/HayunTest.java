package org.ram.controller;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.LcategoryVO;
import org.gm.domain.McategoryVO;
import org.gm.persistence.LcategoryDAO;
import org.gm.persistence.McategoryDAO;
import org.gm.service.ChartDAOService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations ={"file:src/main/webapp/WEB-INF/spring/**/*-context.xml"})
public class HayunTest {
	
	private static final Logger logger = LoggerFactory.getLogger(HayunTest.class);

	@Inject
	private LcategoryDAO ldao;
	
	@Inject
	private McategoryDAO mdao;
	
	@Inject
	private ChartDAOService cdao;

	@Test
	public void allLcategorytest() throws Exception{
		
		logger.info("대분류카테고리 전체찍기........................");
		System.out.println(ldao.lcategoryAllList());
		
	}

	@Test
	public void insertcategorytest() throws Exception{
		LcategoryVO vo=new LcategoryVO();
		logger.info("인서트대분류카테고리......................");
		vo.setLar("식비");
		ldao.lcategoryInsert(vo);
		
	}
	@Test
	public void oneRead()throws Exception{
		LcategoryVO vo=new LcategoryVO();
		logger.info("대분류 하나읽기.......................");
		vo.setLcno(1);
		ldao.lcategoryOneList(vo);
	}
	
	@Test
	public void updateLcategory()throws Exception{
		LcategoryVO vo=new LcategoryVO();
		vo.setLar("교육비");
		vo.setLcno(1);
		ldao.lcategoryUpdate(vo);
	}
	
	@Test
	public void deleteLcategory()throws Exception{
		ldao.lcategoryDelete(3);
	}
	
	@Test
	public void totalMcategory()throws Exception{
		McategoryVO vo=new McategoryVO();
		vo.setLcno(1);
		System.out.println(mdao.mcategoryAllList(vo));
	}
	
	@Test
	public void insertMed()throws Exception{
		McategoryVO vo=new McategoryVO();
		vo.setMed("일식");
		vo.setLcno(1);
		mdao.mcategoryInsert(vo);
		
	}
	
	@Test
	public void oneMcategory()throws Exception{
		McategoryVO vo=new McategoryVO();
		vo.setMcno(3);
		System.out.println(mdao.mcategoryOneList(vo));
	}
	
	@Test
	public void medUpdate()throws Exception{
		McategoryVO vo=new McategoryVO();
		vo.setMed("중식");
		vo.setMcno(4);
		mdao.mcategoryUpdate(vo);
	}
	
	@Test
	public void medDelete()throws Exception{
		mdao.mcategoryDelete(3);
	}
	
	@Test
	public void budgetTest()throws Exception{
		
		ChartVO vo=new ChartVO();
		vo.setMno("dmdm");
		vo.setBudget(12345);
		vo.setYear("2016");
		vo.setMonth("10");
		
		cdao.modifyTwoBudget(vo);
		
	}
}
