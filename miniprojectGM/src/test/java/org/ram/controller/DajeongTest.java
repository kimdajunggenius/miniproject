package org.ram.controller;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.HistoryVO;
import org.gm.domain.MemberVO;
import org.gm.persistence.ChartDAO;
import org.gm.persistence.DiaryDAO;
import org.gm.persistence.MemberDAO;
import org.gm.persistence.RecommendDAO;
import org.gm.persistence.TotalHistoryDAO;
import org.gm.service.HistoryDAOService;
import org.gm.service.MemberDAOService;
import org.gm.task.MakeRecommend;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*-context.xml" })

public class DajeongTest {

	@Inject
	TotalHistoryDAO dao;

	@Inject
	ChartDAO cdao;

	@Inject
	HistoryDAOService hdao;

	@Inject
	MemberDAOService mdao;

	@Inject
	MemberDAO mmdao;

	@Inject
	private DiaryDAO ddao;

	@Inject
	RecommendDAO rdao;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void montlyTest() {
		dao.getTotalMonthly(2016);
	}

	@Test
	public void totalTest() {
		Map<String, String> period = new HashMap<>();
		period.put("start", "2016-01-01");
		period.put("end", "2016-12-31");
		System.out.println(dao.getTotalPieAnnuallyLCategory(period));

	}

	// 특정 사용자 한달 history list
	@Test
	public void memList() throws Exception {
		ChartVO vo = new ChartVO();
		vo.setMno("dmdm");
		vo.setYear("2016");
		vo.setMonth("1");
		System.out.println(hdao.memChartMonthList(vo));
	}

	// 특정 사용자 history 1개 넣어보장
	@Test
	public void insertHis() throws Exception {
		HistoryVO vo = new HistoryVO();
		vo.setMno("dmdm");
		vo.setMtype("w");
		vo.setPaytype("현금");
		vo.setPrice(20000);

		String str = "2016-9-17 14:25:00";
//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//		Date d = dateFormat.parse(str);

		vo.setBtime(str);
		System.out.println("...........오잉???");
		System.out.println(vo);

		hdao.insert(vo);
	}

	// 히스토리 하나 가져오자
	@Test
	public void getHis() throws Exception {
		HistoryVO vo = new HistoryVO();
		vo.setHno(5816);
		System.out.println(hdao.getOne(5816));

	}

	// 특정 회원 특정 날짜 히스토리 목록
	@Test
	public void getHisOneDay() throws Exception {
		Map<String, Object> vo = new HashMap<>();
		/*vo.put("mno", "dmdm");

		String str = "2016-9-17";

		vo.put("btime1", str);
		vo.put("btime2", str);

		System.out.println(hdao.getOneDay(vo));*/
	}

	// 추천 한명 테스트
	@Test
	public void getRecommendMembers() throws Exception {
		Map<String, Object> vo = new HashMap<>();
		vo.put("mno", "1119001074834282");
		vo.put("start1", "20:00:00");
		vo.put("start2", "24:00:00");
		vo.put("end1", "00:00:00");
		vo.put("end2", "03:59:59");

		List<Map<String, Object>> list = rdao.getResultMembers(vo);

		System.out.println("다정 테스트에 오신걸 환영합니다................1");
		// System.out.println(list.get(0).get("mno"));
		//
		String mno = (String) list.get(0).get("mno");
		Integer budget = (Integer) list.get(0).get("budget");
		System.out.println((String) list.get(0).get("mno"));
		System.out.println((Integer) list.get(0).get("budget"));

		//
		if (budget.equals(null)) {
			System.out.println("으아ㅏㅏㅏㅏㅏㅏㅏㅏㅏ 비어또................!!!");
			return;
		}

		Map<String, Object> vo2 = new HashMap<>();
		vo2.put("another", mno);
		vo2.put("start1", "20:00:00");
		vo2.put("start2", "24:00:00");
		vo2.put("end1", "00:00:00");
		vo2.put("end2", "03:59:59");

		// List<Map<String, Object>> list2 = rdao.getAnotherFavoriteStores(vo2);
		// System.out.println("다정 테스트에 오신걸 환영합니다................1");
		// System.out.println(list2.get(0).get("mno"));
		// list2.get(0).put("timezone", "저녁");
		// list2.get(0).put("mno","food@naver.com");
		// rdao.insertRecommend(list2.get(0));
	}

	@Test
	public void getMemberFavoriteStore() {

		List<MemberVO> memberList = new ArrayList<>();
		try {
			memberList = mmdao.getAllMemberList();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		for (MemberVO memberVO : memberList) {
			try {
				Map<String, Object> target = new HashMap<>();
				target.put("mno", memberVO.getMno());
				target.put("start1", "20:00:00");
				target.put("start2", "24:00:00");
				target.put("end1", "00:00:00");
				target.put("end2", "03:59:59");

				System.out.println("...........................ㅎㅎ");
				System.out.println(rdao.getMemberFavoriteStore(target).get("mno"));
				System.out.println("...........................ㅎㅎ");
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	// 회원 가져오기 테스트
	@Test
	public void memberTest() throws Exception {
		// System.out.println(mmdao.getAllMemberList());
		MakeRecommend rec = new MakeRecommend();
		rec.scheduleRun();
	}

	@Test
	public void getResultMember() throws Exception {
		Map<String, Object> target = new HashMap<>();
		target.put("mno", "soso@naver.com");
		target.put("start1", "20:00:00");
		target.put("start2", "24:00:00");
		target.put("end1", "00:00:00");
		target.put("end2", "03:59:59");
		List<Map<String, Object>> memberList = rdao.getResultMembers(target);

		for (Map<String, Object> map : memberList) {
			System.out.println(map.get("mno") + "가 이 가게를 좋아합니다.");
			// step3(mno, map, timezone);
		}
	}

	@Test
	public void getAnotherFavoriteStore() throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("another", "rich@naver.com");
		map.put("target", "food@naver.com");
		map.put("lar", "식비");
		map.put("start1", "20:00:00");
		map.put("start2", "24:00:00");
		map.put("end1", "00:00:00");
		map.put("end2", "03:59:59");

		Map<String, Object> recommendresult = rdao.getAnotherFavoriteStore(map);
		System.out.println(recommendresult);
	}

	@Test
	// 추천 엔진 베타테스트
	public void recommendCBT() throws Exception {
		String[] morningZone = { "아침", "04:00:00", "08:00:00", "08:00:00", "11:59:59" };

		Map<String, Object> target = new HashMap<>();
		target.put("mno", "food@naver.com");
		target.put("start1", morningZone[1]);
		target.put("start2", morningZone[2]);
		target.put("end1", morningZone[3]);
		target.put("end2", morningZone[4]);

		Map<String, Object> tmp = rdao.getMemberFavoriteStore(target);
		String lar = tmp.get("lar").toString();

		List<Map<String, Object>> memberList = rdao.getResultMembers(target);
		for (Map<String, Object> map : memberList) {
			if (Integer.parseInt(map.get("count").toString()) < 1) {
				continue;
			}
			Map<String, Object> third = new HashMap<>();
			third.put("target", "food@naver.com");
			third.put("another", map.get("mno"));
			third.put("start1", morningZone[1]);
			third.put("start2", morningZone[2]);
			third.put("end1", morningZone[3]);
			third.put("end2", morningZone[4]);
			third.put("lar", lar);
			System.out.println("이거 가지고 마지막 추천을 할거거든");
			System.out.println(third.toString());
			Map<String, Object> recommendresult = rdao.getAnotherFavoriteStore(third);
			if (Integer.parseInt(recommendresult.get("countall").toString()) < 1) {
				System.out.println("추천못행...");
			} else {
				recommendresult.put("timezone", morningZone[0]);
				rdao.insertRecommend(recommendresult);
			}
		}

	}
	
	private Map<String, String> dawn = new HashMap<>();
	private Map<String, String> morning = new HashMap<>();
	private Map<String, String> day = new HashMap<>();
	private Map<String, String> night = new HashMap<>();
	private Map<String, String> lar = new HashMap<>();
	private Map<Integer, String> good = new HashMap<>();
	private Map<Integer, String> bad = new HashMap<>();
	private Map<String, Map<Integer, String>> emoticon = new HashMap<>();
	
	
	@Test
	public void diaryTest() {
		dawn.put("좋음", "피곤했지만 기운이 났다."); dawn.put("나쁨", "피곤해 죽겠는데 미치게 만드는군.");
		morning.put("좋음", "덕분에 기분 좋은 하루를 시작할 수 있었다."); morning.put("나쁨", "아 침부터 기분이 거지 같았다.");
		day.put("좋음", "힘이 났다!"); day.put("나쁨", "힘이 빠졌다.");
		night.put("좋음", "기분 좋게 하루를 마감할 수 있었다."); night.put("나쁨", "무리가 너무 안좋았다.");
		lar.put("식비", "내 입을 ");				lar.put("문화생활비", "내 문화생활을 ");		lar.put("건강관리비", "내 건강을 ");		lar.put("품위유지비", "내 외모를 ");		lar.put("차량유지비", "내 애마를 ");		lar.put("주거생활비", "내 생활을 ");lar.put("교육비", "나의 지식을 ");			lar.put("인간관계비", "나의 인간관계를 ");	lar.put("유흥비", "나의 유흥을 ");		lar.put("금융보험비", "나의 금융 관리를 ");	lar.put("저축", "나의 재산 관리를 ");		lar.put("생활편의", "내 생활에 필요한 것들을 ");
		good.put(1, "그냥 괜찮게 해주었고 ");		good.put(2, "흡족하게 해주었고 ");		good.put(3, "즐거움으로 채워주었고 ");		good.put(4, "행복하게 만들어주었고 ");good.put(5, "정말 완벽하게 만족시켜주었고 ");
		bad.put(1, "그저 그렇게 만들어주었고 ");		bad.put(2, "언짢게 만들었고 ");		bad.put(3, "불쾌하게 만들었고 ");			bad.put(4, "망쳐버렸고 ");bad.put(5, "최악으로 만들어버렸고 ");
		emoticon.put("좋음", good);			emoticon.put("나쁨", bad);
		
		
		// 오늘 날짜
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1); 
		String today = df.format(cal.getTime());

		List<MemberVO> memberList = new ArrayList<>();

		try {
			memberList = mmdao.getAllMemberList();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		Map<String, Object> map = new HashMap<>();

		for (int j=0; j<4; j++) {
			try {
				map.put("mno", memberList.get(j).getMno());
				map.put("date", today);
				List<Map<String, Object>> history = ddao.getOneDayDawn(map);
				for (int i = 0; i < history.size(); i++) {
					history.get(i).put("lar_ment", lar.get(history.get(i).get("lar"))+emoticon.get(history.get(i).get("emotion")).get(history.get(i).get("stage")));
					int random = (int) (Math.random() * 3 + 1);
					history.get(i).put("emoticon_ment", history.get(i).get("word"+random));
					if(i==0){
						int stage = Integer.parseInt(history.get(i).get("stage").toString());
						if(stage == 5){
							history.get(i).put("time_ment", dawn.get(history.get(i).get("emotion")));
						}
					}
					System.out.println(history.get(i));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
