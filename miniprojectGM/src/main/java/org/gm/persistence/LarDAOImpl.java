package org.gm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.LarVO;
import org.gm.domain.MemberVO;
import org.gm.domain.SearchCalendarVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class LarDAOImpl implements LarDAO {

	@Inject
	private SqlSessionTemplate sql;
	
	@Override
	public void insert(LarVO vo) throws Exception {
		// TODO Auto-generated method stub
		sql.insert("org.gm.persistence.larMapper.insert",vo);
	}

	@Override
	public List<LarVO> memLarMonthList(LarVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectList("org.gm.persistence.larMapper.memLarMonList",vo);
	}

	@Override
	public List<LarVO> memOneMonthLarList(String mno, String year, String month) throws Exception {
		// TODO Auto-generated method stub
		Map<String, String> map = new HashMap<>();
		map.put("mno",mno);
		map.put("year",year);
		map.put("month",month);
		return sql.selectList("org.gm.persistence.larMapper.memLarMonList",map);
	}

	@Override
	public List<LarVO> defaultmemOneMonthLarList(String mno) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectList("org.gm.persistence.larMapper.defaultmemLarMonList",mno);
	}
	
	//lar2입니다
	@Override
	public List<LarVO> defaultmemOneMonthLarList2(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectList("org.gm.persistence.larMapper.defaultmemLarMonList2",vo);
	}

	@Override
	public List<LarVO> searchLarSumList(SearchCalendarVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectList("org.gm.persistence.larMapper.searchLarSumList",vo);
	}

	
	
	
}
