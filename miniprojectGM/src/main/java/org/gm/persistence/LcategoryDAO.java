package org.gm.persistence;

import java.util.List;

import org.gm.domain.LcategoryVO;

public interface LcategoryDAO {

	public List<LcategoryVO> lcategoryAllList()throws Exception;
	
	public void lcategoryInsert(LcategoryVO vo)throws Exception;
	
	public LcategoryVO lcategoryOneList(LcategoryVO vo)throws Exception;
	
	public void lcategoryUpdate(LcategoryVO vo)throws Exception;
	
	public void lcategoryDelete(Integer lcno)throws Exception;
}
