package org.gm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.MedVO;
import org.gm.domain.SearchCalendarVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MedDAOImpl implements MedDAO {
	
	@Inject
	private SqlSessionTemplate sql;

	@Override
	public void insert(MedVO vo) throws Exception {
		// TODO Auto-generated method stub

		sql.insert("org.gm.persistence.medMapper.insert",vo);
	}

	@Override
	public List<MedVO> memMedMonList(MedVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectList("org.gm.persistence.medMapper.memMedMonList",vo);
	}

	@Override
	public List<MedVO> memOneMonthMedList(String mno, String year, String month) throws Exception {
		// TODO Auto-generated method stub
		Map<String, String> map = new HashMap<>();
		map.put("mno",mno);
		map.put("year",year);
		map.put("month",month);
		return sql.selectList("org.gm.persistence.medMapper.memMedMonList",map);
	}

	@Override
	public List<MedVO> defaultmemOneMonthMedList(String mno) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectList("org.gm.persistence.medMapper.defaultmemMedMonList",mno);
	}
	
	//Med2 입니다
	@Override
	public List<MedVO> defaultmemOneMonthMedList2(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectList("org.gm.persistence.medMapper.defaultmemMedMonList2",vo);
	}

	@Override
	public List<MedVO> searchMedSumList(SearchCalendarVO vo) throws Exception {
		// TODO Auto-generated method stub
				
		return sql.selectList("org.gm.persistence.medMapper.searchMedSumList",vo);
	}

	
	
}
