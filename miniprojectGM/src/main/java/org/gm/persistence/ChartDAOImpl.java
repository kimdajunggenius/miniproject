package org.gm.persistence;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.SearchCalendarVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ChartDAOImpl implements ChartDAO {

	@Inject
	private SqlSessionTemplate sql;
	
	@Override
	public ChartVO chartMonList(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne("org.gm.persistence.chartMapper.chartMonList",vo);
	}

	@Override
	public void chartInsert(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		sql.insert("org.gm.persistence.chartMapper.chartInsert",vo);
	}

	@Override
	public void totalUpdate(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		sql.update("org.gm.persistence.chartMapper.totalUpdate",vo);
	}

	@Override
	public ChartVO chartMonthList(ChartVO vo) throws Exception {
		// �듅�젙 �궗�슜�옄 �븳�떖 history list 媛��졇�삤湲�
		return sql.selectOne("org.gm.persistence.chartMapper.chartMonthList",vo);
	}

	@Override
	public List<ChartVO> chartAllList(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectList("org.gm.persistence.chartMapper.chartAllList",vo);
	}

	@Override
	public ChartVO avgChartNowMonthPreList(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne("org.gm.persistence.chartMapper.avgChartNowMonthPreList",vo);
	}

	@Override
	public ChartVO memChartMonthList(String mno, String year, String month) throws Exception {
		// TODO Auto-generated method stub
		Map<String, String> map = new HashMap<>();
		map.put("mno",mno);
		map.put("year",year);
		map.put("month",month);
		return sql.selectOne("org.gm.persistence.chartMapper.chartMonthList",map);
	}

	@Override
	public List<ChartVO> memChartAllList(String mno) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectList("org.gm.persistence.chartMapper.chartAllList",mno);
	}

	
//	현재까지 사용한거 전달까지 평균 가져온거야
	@Override
	public ChartVO memAvgChartNowMonthPreList(String mno ) throws Exception {
		// TODO Auto-generated method stub
		 
		return sql.selectOne("org.gm.persistence.chartMapper.avgChartNowMonthPreList",mno);
	}

	
//	현재 달꺼 가져오는거
	@Override
	public ChartVO defaultmemChartMonthList(String mno) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne("org.gm.persistence.chartMapper.defaultchartMonthList",mno);
	}

	
	@Override
	public ChartVO searchChartList(SearchCalendarVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne("org.gm.persistence.chartMapper.searchChartSumList",vo);
	}

	
//	 占쏙옙占쏙옙占� 占쏙옙占쏙옙, 占쏙옙占쎄에 占쏙옙占쏙옙 占쏙옙占� , 占쏙옙占쏙옙/占쏙옙占쏙옙/占쏙옙占싱대에 占쏙옙占쏙옙 占쏙옙占� 
	@Override
	public Integer memBudget(String mno) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne("org.gm.persistence.chartMapper.memBudget",mno);
	}

	
	@Override
	public Integer nextBudget(String mno) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne("org.gm.persistence.chartMapper.nextBudget",mno);
	}

	@Override
	public Integer nextNextBudget(String mno) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne("org.gm.persistence.chartMapper.nextNextBudget",mno);
	}
	
	
	

	@Override
	public void budgetUpdate(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		sql.update("org.gm.persistence.chartMapper.budgetUpdate",vo);
	}	
	
	@Override
	public void budgetInsert(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		sql.insert("org.gm.persistence.chartMapper.budgetInsert",vo);
		
	}

	@Override
	public ChartVO budAvgChart(Integer budget) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne("org.gm.persistence.chartMapper.budAvgChart",budget);
	}

	@Override
	public ChartVO budGenAgeAvgChart(String gender, Integer budget, Date birthday) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		map.put("gender", gender);
		map.put("budget", budget);
		map.put("birthday", birthday);
		return sql.selectOne("org.gm.persistence.chartMapper.budGenAgeAvgChart",map);
	}

	@Override
	public ChartVO defaultmemChartMonthList2(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne("org.gm.persistence.chartMapper.defaultchartMonthList2",vo);
	}

	@Override
	public ChartVO avgChartNowMonthPreList2(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne("org.gm.persistence.chartMapper.avgChartNowMonthPreList2",vo);
	}

	@Override
	public ChartVO budGenAgeAvgChart2(ChartVO vo, String gender, Date birthday) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		map.put("gender", gender);
		map.put("birthday", birthday);
		map.put("budget", vo.getBudget());
		map.put("month", vo.getMonth());
		map.put("year", vo.getYear());
		
		return sql.selectOne("org.gm.persistence.chartMapper.budGenAgeAvgChart2", map);
	}
	
	
}
