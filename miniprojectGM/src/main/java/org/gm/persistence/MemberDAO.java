package org.gm.persistence;

import java.util.List;

import org.gm.domain.MemberVO;
import org.gm.dto.LoginDTO;

public interface MemberDAO {

	public void createMember(MemberVO vo)throws Exception;
	
//	public MemberVO getMember(MemberVO vo)throws Exception;
	
	public void facebookCreateMember(MemberVO vo)throws Exception;
	
	public void updateMember(MemberVO vo)throws Exception;
	
	public void memberToAdmin(String mno)throws Exception;
	
	public void adminToMember(String mno)throws Exception;
	
	public void deleteMember(String mno)throws Exception;
	
	public List<MemberVO> getMemberList(Integer page)throws Exception;
	
	public List<MemberVO> getAllMemberList() throws Exception;

	public MemberVO getMember(String mno)throws Exception;
	
	public Integer getMemberTotal()throws Exception;
	
	public Integer checkLogin(MemberVO vo) throws Exception;
	
	public Integer checkLoginFacebook(MemberVO vo) throws Exception;
	
	public MemberVO checkLoging(MemberVO vo) throws Exception;
	
}
