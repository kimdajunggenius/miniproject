package org.gm.persistence;

import java.util.List;

import org.gm.domain.HistoryVO;
import org.gm.domain.LarVO;
import org.gm.domain.MedVO;

public interface HistoryDAO {

	public List<HistoryVO> pageList(Integer page)throws Exception;
	
	public LarVO larList(LarVO vo)throws Exception;
	
	public MedVO medList(MedVO vo)throws Exception;
	
	public void insert(HistoryVO vo)throws Exception;
	
	public HistoryVO getOne(Integer hno) throws Exception;
	
	public List<HistoryVO> getOneDay(HistoryVO vo) throws Exception;
	

	//하윤추가
	public void memhistoryListUpdate(HistoryVO vo) throws Exception;
	
	public void oneDelete(Integer hno) throws Exception;
	
	public void memhistoryOneDelete(Integer hno)throws Exception;
	
	public Integer totalcount() throws Exception;
	
}
