package org.gm.persistence;

import java.util.List;

import javax.inject.Inject;

import org.gm.domain.MemberVO;
import org.gm.domain.SearchMemVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SearchMemDAOImpl implements SearchMemDAO {

	
	@Inject
	private SqlSessionTemplate sql;
	private static String mapperName="org.gm.persistence.memberMapper.";
	
	
	@Override
	public List<MemberVO> searchMem(SearchMemVO vo) throws Exception {
		// TODO Auto-generated method stub
		
		if(vo.getPage()==null || vo.getPage()<1){
			vo.setPage(1);
		}
		Integer page=vo.getPage();
		vo.setPage((page-1)*10);
		return sql.selectList(mapperName+"memSearch",vo);
	}

	@Override
	public Integer searchCount(SearchMemVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne(mapperName+"memSearchCount",vo);
	}

}
