package org.gm.persistence;

import java.util.List;

import javax.inject.Inject;

import org.gm.domain.HistoryVO;
import org.gm.domain.LarVO;
import org.gm.domain.MedVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class HistoryDAOImpl implements HistoryDAO {

	@Inject
	private SqlSessionTemplate sql;
	
	Logger logger = LoggerFactory.getLogger(HistoryDAOImpl.class);
	
	@Override
	public Integer totalcount() throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne("org.gm.persistence.historyMapper.history_total");
	}

	@Override
	public List<HistoryVO> pageList(Integer page) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("pageList");
		System.out.println(page);
		if(page == null){
			page= 1;
		}
		return sql.selectList("org.gm.persistence.historyMapper.pageList",page);
		
	}

	@Override
	public LarVO larList(LarVO vo) throws Exception {
		// TODO Auto-generated method stub
		
		return sql.selectOne("org.gm.persistence.historyMapper.larList",vo);
		
	}

	@Override
	public MedVO medList(MedVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne("org.gm.persistence.historyMapper.medList",vo);
	}

	@Override
	public void insert(HistoryVO vo) throws Exception {
		// TODO Auto-generated method stub
		sql.insert("org.gm.persistence.historyMapper.insert",vo);
	}

	@Override
	public HistoryVO getOne(Integer hno) throws Exception {
		logger.info("history 하나 가져오기..................hno:"+hno);
		return sql.selectOne("org.gm.persistence.historyMapper.getOne", hno);
	}

	@Override
	public List<HistoryVO> getOneDay(HistoryVO vo) throws Exception {
		logger.info("하루 history list 가져오기................."+vo);
		return sql.selectList("org.gm.persistence.historyMapper.getOneDayList",vo);
	}

	
	@Override
	public void memhistoryListUpdate(HistoryVO vo) throws Exception {
		logger.info("history 하나 수정하기................."+vo);
		sql.update("org.gm.persistence.historyMapper.memhistoryListUpdate",vo);
		
	}

	@Override
	public void memhistoryOneDelete(Integer hno) throws Exception {
		// TODO Auto-generated method stub
		sql.delete("org.gm.persistence.historyMapper.memhistoryOneDelete",hno);
				
		
	}
	
	
	
	@Override
	public void oneDelete(Integer hno) throws Exception {
		logger.info("history 하나 삭제하기................."+hno);
		sql.update("org.gm.persistence.historyMapper.deleteOne",hno);
		
	}
	

}
