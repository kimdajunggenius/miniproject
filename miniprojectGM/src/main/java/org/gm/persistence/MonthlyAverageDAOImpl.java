package org.gm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.MonthlyAverageVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class MonthlyAverageDAOImpl implements MonthlyAverageDAO {

	@Inject
	private SqlSessionTemplate sql;
	
	@Override
	public List<MonthlyAverageVO> getMonthlygender(String year, String month) {
		// TODO Auto-generated method stub
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("year", year);
		map.put("month", month);
		System.out.println(map);
		
		
		return sql.selectList("org.gm.persistence.monthlyAverageMapper.monthAverageGender",map); 
	}

	@Override
	public List<MonthlyAverageVO> getMonthlyage(String year, String month) throws Exception {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("year", Integer.parseInt(year));
		map.put("month", Integer.parseInt(month));
		System.out.println(map);
		return sql.selectList("org.gm.persistence.monthlyAverageMapper.monthAverageAge",map);
	}

	@Override
	public List<MonthlyAverageVO> getMonthlybudget(String year, String month) throws Exception {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("year", Integer.parseInt(year));
		map.put("month", Integer.parseInt(month));
		System.out.println(map);
		return sql.selectList("org.gm.persistence.monthlyAverageMapper.monthAverageBudget",map);
		
	}

}
