package org.gm.persistence;

import java.util.List;

import org.gm.domain.LcategoryVO;
import org.gm.domain.McategoryVO;
import org.gm.domain.SearchStoreVO;
import org.gm.domain.StoreStatisticsVO;
import org.gm.domain.StoreVO;

public interface StoreDAO {
	
	public void insertStore(StoreVO vo) throws Exception;
	public void updateStore(StoreVO vo) throws Exception;
	public void deleteStore(Integer sno) throws Exception;
	public void restoreStore(Integer sno) throws Exception;
	
	public Integer getTotalStore() throws Exception; //total number of the stores
	public Integer getSearchStore(SearchStoreVO vo) throws Exception; //total number of searched stores
	public Integer getNullStore() throws Exception; //total number of null stores
	
	public StoreVO getOneStore(Integer sno) throws Exception; //get one store
	public List<StoreVO> storeList(Integer page) throws Exception; //total List 
	public List<StoreVO> searchStoreList(SearchStoreVO vo) throws Exception; //search List
	public List<StoreVO> storeNullList(Integer page) throws Exception; //null List 
	
	public List<LcategoryVO> getLcategoryList() throws Exception;//get Lcategory list for searching
	public List<McategoryVO> getMcategoryList(Integer lcno) throws Exception;//get Mcategory list from selected Lcategory element
	
	public List<StoreStatisticsVO> getMonthlyChart(StoreStatisticsVO vo) throws Exception; //get consuming data of selected year on specific store
	public List<StoreStatisticsVO> getDailyChart(StoreStatisticsVO vo) throws Exception; //get consuming data of selected date on specific store

}
