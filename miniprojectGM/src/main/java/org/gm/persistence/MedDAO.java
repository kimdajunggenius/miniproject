package org.gm.persistence;

import java.util.List;

import org.gm.domain.ChartVO;
import org.gm.domain.MedVO;
import org.gm.domain.SearchCalendarVO;

public interface MedDAO {

	public void insert(MedVO vo)throws Exception;
	
	public List<MedVO> memMedMonList(MedVO vo)throws Exception;
	
	public List<MedVO> memOneMonthMedList(String mno,String year,String month)throws Exception;
	
	public List<MedVO> defaultmemOneMonthMedList(String mno)throws Exception;
	//med2입니다
	public List<MedVO> defaultmemOneMonthMedList2(ChartVO vo)throws Exception;
	
	public List<MedVO> searchMedSumList(SearchCalendarVO vo)throws Exception;
	
}
