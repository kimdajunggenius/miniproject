package org.gm.persistence;

import java.util.List;

import org.gm.domain.MemberVO;
import org.gm.domain.SearchMemVO;

public interface SearchMemDAO {

	public List<MemberVO> searchMem(SearchMemVO vo)throws Exception;
	public Integer searchCount(SearchMemVO vo)throws Exception;
	
	
}
