package org.gm.persistence;

import java.util.List;
import java.util.Map;

import org.gm.domain.ChartVO;
import org.gm.domain.HistoryMemberChartVO;
import org.gm.domain.HistoryStoreChart;
import org.gm.domain.MonthTotalVO;

public interface TotalHistoryDAO {
	
	// line
	public List<MonthTotalVO> getTotalMonthly(Integer year);
	
	public List<ChartVO> getTotalDaily(MonthTotalVO vo);
	
	// line graph for store
	public List<HistoryStoreChart> getTotalPieAnnuallyLCategory(Map<String, String> period);
	
	public List<HistoryStoreChart> getTotalPieAnnuallyMCategory(Map<String, String> period);
	
	public List<HistoryStoreChart> getTotalPieAnnuallyLocal(Map<String, String> period);

	// line graph for member
	
	public List<HistoryMemberChartVO> getTotalPieAnnuallyPaytype(Map<String, String> period);
	public List<HistoryMemberChartVO> getTotalPieAnnuallyGender(Map<String, String> period);
	public List<HistoryMemberChartVO> getTotalPieAnnuallyAge(Map<String, String> period);
	public List<HistoryMemberChartVO> getTotalPieAnnuallyMemaddress(Map<String, String> period);
}
