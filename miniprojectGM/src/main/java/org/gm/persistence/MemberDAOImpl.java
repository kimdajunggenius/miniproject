package org.gm.persistence;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.MemberVO;
import org.gm.dto.LoginDTO;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MemberDAOImpl implements MemberDAO {


	@Inject
	private SqlSessionTemplate sql;
	
	private static String mapperName="org.gm.persistence.memberMapper.";
	
	//로그인체크
	@Override
	public MemberVO checkLoging(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		
		return sql.selectOne(mapperName+"checkLoging", vo);
	}

	@Override
	public void createMember(MemberVO vo) throws Exception {

		sql.insert(mapperName+"createMember",vo);
	}

	@Override
	public void updateMember(MemberVO vo) throws Exception {

		sql.update(mapperName+"updateMember",vo);
	}

	@Override
	public void memberToAdmin(String mno) throws Exception {

		sql.update(mapperName+"memberToAdmin",mno);
	}

	@Override
	public void adminToMember(String mno) throws Exception {

		sql.update(mapperName+"adminToMember",mno);
	}

	@Override
	public void deleteMember(String mno) throws Exception {

		sql.update(mapperName+"deleteMember",mno);
	}

	@Override
	public List<MemberVO> getMemberList(Integer page) throws Exception {

		page=(page-1)*10;
		return sql.selectList(mapperName+"getMemberList",page);
	}
	
	@Override
	public List<MemberVO> getAllMemberList() throws Exception {
		return sql.selectList(mapperName+"getAllMemberList");
	}

	@Override
	public MemberVO getMember(String mno) throws Exception {

		return sql.selectOne(mapperName+"getMember",mno);
	}

	@Override
	public Integer getMemberTotal() throws Exception {

		return sql.selectOne(mapperName+"getMemberTotal");
	}

	@Override
	public Integer checkLogin(MemberVO vo) throws Exception {

		return sql.selectOne(mapperName+"check",vo);
	}

	@Override
	public Integer checkLoginFacebook(MemberVO vo) throws Exception {

		return sql.selectOne(mapperName+"checkf",vo);
	}

	@Override
	public void facebookCreateMember(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		sql.insert(mapperName+"facebookCreateMember",vo);
	}

/*	@Override
	public MemberVO getMember(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		return sql.selectOne(mapperName+"getMember",vo);
	}
*/
	
		

}
