package org.gm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.EmoticonVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class EmoticonDAOImpl implements EmoticonDAO {

	private static final Logger logger = LoggerFactory.getLogger(EmoticonDAOImpl.class);
	
	@Inject 
	private SqlSessionTemplate sqlsession;
	
	
	
	@Override
	public Integer totalcount() throws Exception {
		// TODO Auto-generated method stub
		return sqlsession.selectOne("org.gm.persistence.emoticonMapper.emoticon_total");
	}

	@Override
	public List<EmoticonVO> selectList(Integer page) throws Exception {
		// TODO Auto-generated method stub
		logger.info("이모티콘의 모든리스트 ");
		page = (page-1)*10;
		
		return sqlsession.selectList("org.gm.persistence.emoticonMapper.emoticon_selectlist", page);
	}

	@Override
	public List<EmoticonVO> selectLike(EmoticonVO vo,Integer page,String word) throws Exception {
		// TODO Auto-generated method stub
		logger.info("이모티콘의 검색리스트 ");
		Map<String, Object> map = new HashMap<>();
		map.put("vo", vo);
		map.put("page", page);
		map.put("word", word);
		logger.info(map.toString());
		return sqlsession.selectList("org.gm.persistence.emoticonMapper.emoticon_selectlike", map);
	}

	@Override
	public void insert(EmoticonVO vo) throws Exception {
		// TODO Auto-generated method stub
		logger.info("이모티콘을 추가 ");
		sqlsession.insert("org.gm.persistence.emoticonMapper.emoticon_insert", vo);

	}

	@Override
	public void update(EmoticonVO vo) throws Exception {
		// TODO Auto-generated method stub
		logger.info("이모티콘을 수정 ");
		sqlsession.insert("org.gm.persistence.emoticonMapper.emoticon_update", vo);

	}

	@Override
	public void delete(EmoticonVO vo) throws Exception {
		// TODO Auto-generated method stub
		logger.info("이모티콘을 삭제");
		sqlsession.insert("org.gm.persistence.emoticonMapper.emoticon_delete", vo);

	}

}
