package org.gm.persistence;

import java.util.List;

import javax.inject.Inject;

import org.gm.domain.LcategoryVO;
import org.gm.domain.McategoryVO;
import org.gm.domain.SearchStoreVO;
import org.gm.domain.StoreStatisticsVO;
import org.gm.domain.StoreVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class StoreDAOImpl implements StoreDAO {

	@Inject
	private SqlSessionTemplate sql;
	

	@Override
	public void insertStore(StoreVO vo) throws Exception {
		sql.insert("org.gm.persistence.storeMapper.createStore",vo);
	}

	@Override
	public void updateStore(StoreVO vo) throws Exception {
		sql.update("org.gm.persistence.storeMapper.updateStore",vo);

	}

	@Override
	public void deleteStore(Integer sno) throws Exception {
		sql.update("org.gm.persistence.storeMapper.deleteStore",sno);
	}


	@Override
	public Integer getTotalStore() throws Exception {
		
		return sql.selectOne("org.gm.persistence.storeMapper.totalStore");
	}

	@Override
	public Integer getSearchStore(SearchStoreVO vo) throws Exception {
		return sql.selectOne("org.gm.persistence.storeMapper.searchStore", vo);
	}

	@Override
	public List<StoreVO> storeList(Integer page) throws Exception {
		page = (page-1)*10;
		return sql.selectList("org.gm.persistence.storeMapper.storeList", page);
	}
	
	@Override
	public void restoreStore(Integer sno) throws Exception {
		sql.update("org.gm.persistence.storeMapper.restoreStore", sno);
	}

	@Override
	public List<StoreVO> storeNullList(Integer page) throws Exception {
		page = (page-1)*10;
		return sql.selectList("org.gm.persistence.storeMapper.storeNullList", page);
	}

	@Override
	public StoreVO getOneStore(Integer sno) throws Exception {
		return sql.selectOne("org.gm.persistence.storeMapper.selectOne", sno);
	}

	@Override
	public Integer getNullStore() throws Exception {
		return sql.selectOne("org.gm.persistence.storeMapper.nullStore");
	}

	@Override
	public List<LcategoryVO> getLcategoryList() throws Exception {
		return sql.selectList("org.gm.persistence.storeMapper.selectLar");
	}

	@Override
	public List<McategoryVO> getMcategoryList(Integer lcno) throws Exception {
		return sql.selectList("org.gm.persistence.storeMapper.selectMed",lcno);
	}

	@Override
	public List<StoreVO> searchStoreList(SearchStoreVO vo) throws Exception {
		vo.setPage((vo.getPage()-1)*10);
		System.out.println(vo);
		return sql.selectList("org.gm.persistence.storeMapper.searchList", vo);
	}

	@Override
	public List<StoreStatisticsVO> getMonthlyChart(StoreStatisticsVO vo) throws Exception {
		
		return sql.selectList("org.gm.persistence.storeMapper.yearChart", vo);
	}

	@Override
	public List<StoreStatisticsVO> getDailyChart(StoreStatisticsVO vo) throws Exception {
		return sql.selectList("org.gm.persistence.storeMapper.dateChart", vo);
	}

}
