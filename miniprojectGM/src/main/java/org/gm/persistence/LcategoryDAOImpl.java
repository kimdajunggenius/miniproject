package org.gm.persistence;

import java.util.List;

import javax.inject.Inject;

import org.gm.domain.LcategoryVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class LcategoryDAOImpl implements LcategoryDAO {

	@Inject
	private SqlSessionTemplate sql;
	
	Logger logger = LoggerFactory.getLogger(LcategoryDAOImpl.class);
	
	@Override
	public List<LcategoryVO> lcategoryAllList() throws Exception {
		logger.info(".......All List..........................");
		return sql.selectList("org.gm.persistence.categoryMapper.lcategoryList");
	}

	@Override
	public void lcategoryInsert(LcategoryVO vo) throws Exception {
		
		 sql.insert("org.gm.persistence.categoryMapper.lcategoryInsert",vo);

	}

	@Override
	public LcategoryVO lcategoryOneList(LcategoryVO vo) throws Exception {

		return sql.selectOne("org.gm.persistence.categoryMapper.lcategoryOne",vo);

	}

	@Override
	public void lcategoryUpdate(LcategoryVO vo) throws Exception {

		sql.update("org.gm.persistence.categoryMapper.lcategoryUpdate",vo);

	}

	@Override
	public void lcategoryDelete(Integer lcno) throws Exception {

		sql.delete("org.gm.persistence.categoryMapper.lcategoryDelete",lcno);

	}

}

