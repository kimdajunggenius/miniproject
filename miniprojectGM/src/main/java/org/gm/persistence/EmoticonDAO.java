package org.gm.persistence;

import java.util.List;

import org.gm.domain.EmoticonVO;

public interface EmoticonDAO {
	
	public Integer totalcount() throws Exception;
	public List<EmoticonVO> selectList(Integer page) throws Exception;
	public List<EmoticonVO> selectLike(EmoticonVO vo,Integer page, String word) throws Exception;
	public void insert(EmoticonVO vo) throws Exception;
	public void update(EmoticonVO vo) throws Exception;
	public void delete(EmoticonVO vo) throws Exception;
	
}
