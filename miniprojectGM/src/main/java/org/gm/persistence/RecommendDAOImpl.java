package org.gm.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.RecommendVO;
import org.gm.domain.StoreVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class RecommendDAOImpl implements RecommendDAO {
	
	@Inject
	SqlSessionTemplate sqlSession;
	
	Logger logger = LoggerFactory.getLogger(RecommendDAOImpl.class);
	

	@Override
	public Map<String, Object> getMemberFavoriteStore(Map<String, Object> map) throws Exception {
		logger.info("STEP1.사용자가 가장 좋아하는 가게 하나 .........................");
		return sqlSession.selectOne("org.gm.persistence.RecommendMapper.getFavoriteStore", map);
	}

	@Override
	public List<Map<String, Object>> getResultMembers(Map<String, Object> map) throws Exception {
		
		logger.info("STEP2.추천을 위한 다른 멤버 가져오기 .........................");
		return sqlSession.selectList("org.gm.persistence.RecommendMapper.getOthersBasedOnTargetFavoriteStore", map);
	}

	@Override
	public Map<String, Object> getAnotherFavoriteStore(Map<String, Object> map) throws Exception {
		logger.info("STEP3.다른 멤버가 좋아하는 가게 하나.........................");
		return sqlSession.selectOne("org.gm.persistence.RecommendMapper.getAnotherFavoriteStore", map);
	}

	@Override
	public void insertRecommend(Map<String, Object> map) throws Exception {
		logger.info("STEP4.찾은 가게들을 DB에 넣자.....................");
		sqlSession.selectList("org.gm.persistence.RecommendMapper.insertRecommend", map);
	}

	@Override
	public List<StoreVO> getList(RecommendVO vo) throws Exception {
		logger.info("추천 목록 가져오기");
		return sqlSession.selectList("org.gm.persistence.storeMapper.getList", vo);
	}

	@Override
	public Integer getListNumber(RecommendVO vo) throws Exception {
		logger.info("추천되는 가게 갯수");
		return sqlSession.selectOne("org.gm.persistence.RecommendMapper.getListNumber", vo);
	}

	@Override
	public List<Map<String, Object>> getRecommendList(RecommendVO vo) throws Exception {
		logger.info("추천되는 가게 리스트");
		return sqlSession.selectList("org.gm.persistence.RecommendMapper.getRecommendList", vo);
	}

	
}
