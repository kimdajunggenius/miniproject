package org.gm.persistence;

import java.sql.Date;
import java.util.List;

import org.gm.domain.ChartVO;
import org.gm.domain.SearchCalendarVO;

public interface ChartDAO {
	
	public ChartVO chartMonList(ChartVO vo)throws Exception;
	
	public void chartInsert(ChartVO vo)throws Exception;
	
	public void totalUpdate(ChartVO vo)throws Exception;
	
	public ChartVO chartMonthList(ChartVO vo)throws Exception;
	
	public List<ChartVO> chartAllList(ChartVO vo)throws Exception;
	
	public ChartVO avgChartNowMonthPreList(ChartVO vo)throws Exception;
	
//	memberController���� ���ϰ� ������� ������
	public ChartVO memChartMonthList(String mno,String year,String month)throws Exception;
	
	public ChartVO defaultmemChartMonthList(String mno )throws Exception;
	
	public List<ChartVO> memChartAllList(String mno)throws Exception;
	
	public ChartVO memAvgChartNowMonthPreList(String mno )throws Exception;
	
	public void budgetInsert(ChartVO vo)throws Exception;
	
	public ChartVO searchChartList(SearchCalendarVO vo)throws Exception;
	
//	budAvgChart , budGenAgeAvgChart, memBudget
	
	public Integer memBudget(String mno)throws Exception;
	
	public Integer nextBudget(String mno)throws Exception;
	
	public Integer nextNextBudget(String mno)throws Exception;
	
	public void budgetUpdate(ChartVO vo)throws Exception;
		
	public ChartVO budAvgChart(Integer budget)throws Exception;
	
	public ChartVO budGenAgeAvgChart(String gender,Integer budget,Date birthday)throws Exception;
	
	
//	년도랑 월 값 받아서 사용자의 1달 정보 보기 
	public ChartVO defaultmemChartMonthList2(ChartVO vo)throws Exception;
//	년도랑 월 값 받아서 받은 월 값의 전달꺼까지 더한 평균 정보 보기  
	public ChartVO avgChartNowMonthPreList2(ChartVO vo)throws Exception;
	
	public ChartVO budGenAgeAvgChart2(ChartVO vo,String gender, Date birthday)throws Exception;

}
