package org.gm.persistence;

import java.util.List;
import java.util.Map;

import org.gm.domain.RecommendVO;
import org.gm.domain.StoreVO;

public interface RecommendDAO {
	
	public Map<String, Object> getMemberFavoriteStore(Map<String, Object> map) throws Exception;
	
	public List<Map<String, Object>> getResultMembers(Map<String, Object> map) throws Exception;
	
	public Map<String, Object> getAnotherFavoriteStore(Map<String, Object> map) throws Exception;
	
	public void insertRecommend(Map<String, Object> map) throws Exception;
	
	public List<StoreVO> getList(RecommendVO vo) throws Exception;
	
	public Integer getListNumber(RecommendVO vo) throws Exception;
	
	public List<Map<String, Object>> getRecommendList(RecommendVO vo) throws Exception;

}
