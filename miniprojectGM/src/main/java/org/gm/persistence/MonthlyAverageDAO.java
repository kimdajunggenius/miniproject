package org.gm.persistence;

import java.util.List;

import org.gm.domain.MonthlyAverageVO;

public interface MonthlyAverageDAO {

	public List<MonthlyAverageVO> getMonthlygender(String year, String month) throws Exception;
	public List<MonthlyAverageVO> getMonthlyage(String year, String month) throws Exception;
	public List<MonthlyAverageVO> getMonthlybudget(String year, String month) throws Exception;
	
}
