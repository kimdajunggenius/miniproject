package org.gm.persistence;

import java.util.List;

import org.gm.domain.McategoryVO;

public interface McategoryDAO {

	public List<McategoryVO> mcategoryAllList(McategoryVO vo)throws Exception;
	
	public McategoryVO mcategoryOneList(McategoryVO vo)throws Exception;
	
	public void mcategoryInsert(McategoryVO vo)throws Exception;
	
	public void mcategoryUpdate(McategoryVO vo)throws Exception;
	
	public void mcategoryDelete(Integer mcno)throws Exception;
}
