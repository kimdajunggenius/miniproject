package org.gm.persistence;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.DiaryVO;
import org.gm.domain.HistoryVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class DiaryDAOImpl implements DiaryDAO {
	
	@Inject
	private SqlSessionTemplate sql;
	
	Logger logger = LoggerFactory.getLogger(HistoryDAOImpl.class);

	@Override
	public List<Map<String, Object>> getOneDayDawn(Map<String, Object> vo) throws Exception {
		logger.info(vo.get("mno")+"의 새벽 history.....................");
		return sql.selectList("org.gm.persistence.DiaryMapper.getOneDayListDawn",vo);
	}

	@Override
	public List<Map<String, Object>> getOneDayMorning(Map<String, Object> vo) throws Exception {
		logger.info(vo.get("mno")+"의 아침 history.....................");
		return sql.selectList("org.gm.persistence.DiaryMapper.getOneDayListMorning",vo);
	}

	@Override
	public List<Map<String, Object>> getOneDayDay(Map<String, Object> vo) throws Exception {
		logger.info(vo.get("mno")+"의 낮 history.....................");
		return sql.selectList("org.gm.persistence.DiaryMapper.getOneDayListDay",vo);
	}

	@Override
	public List<Map<String, Object>> getOneDayNight(Map<String, Object> vo) throws Exception {
		logger.info(vo.get("mno")+"의 저녁 history.....................");
		return sql.selectList("org.gm.persistence.DiaryMapper.getOneDayListNight",vo);
	}

	@Override
	public void insertDiary(Map<String, Object> diary) throws Exception {
		logger.info(diary.get("mno")+"다이어리 한 줄 추가..................");
		sql.insert("org.gm.persistence.DiaryMapper.insertDiary", diary);
	}

	@Override
	public List<DiaryVO> getOneDayDiary(DiaryVO vo) throws Exception {
		logger.info("다이어리 하루 치 가져오쟈................"+vo);
		return sql.selectList("org.gm.persistence.DiaryMapper.selectOneDayDiary",vo);
	}

	@Override
	public List<Map<String, Object>> getOneDayEmotion(DiaryVO vo) throws Exception {
		logger.info("다이어리 감정 점수계산을 위한 데이터 가져오기................"+vo);
		return sql.selectList("org.gm.persistence.DiaryMapper.selectOneDayEmotion",vo);
	}

	@Override
	public int getSum(DiaryVO vo) throws Exception {
		logger.info("다이어리 기록날짜 까지 쓴 돈 합계 가져오기..............");
		return sql.selectOne("org.gm.persistence.DiaryMapper.selectSumPrice", vo);
	}

	@Override
	public void insertScore(DiaryVO vo) throws Exception {
		logger.info("계산한 이모티콘 점수, 히스토리 점수 저장하기 ..............");
		sql.insert("org.gm.persistence.DiaryMapper.insertScore", vo);
	}

	@Override
	public DiaryVO getScoreComment(DiaryVO vo) throws Exception {
		logger.info("계산한 이모티콘 점수&코멘트, 히스토리 점수&코멘트 가져오기  ..............");
		return sql.selectOne("org.gm.persistence.DiaryMapper.selectOneDayScore", vo);
	}
	
	
	
}
