package org.gm.persistence;

import java.util.List;
import java.util.Map;

import org.gm.domain.DiaryVO;
import org.gm.domain.HistoryVO;

public interface DiaryDAO {

	public List<Map<String, Object>> getOneDayDawn(Map<String, Object> vo) throws Exception;

	public List<Map<String, Object>> getOneDayMorning(Map<String, Object> vo) throws Exception;

	public List<Map<String, Object>> getOneDayDay(Map<String, Object> vo) throws Exception;

	public List<Map<String, Object>> getOneDayNight(Map<String, Object> vo) throws Exception;
	
	public void insertDiary(Map<String, Object> diary) throws Exception;
	
	public List<DiaryVO> getOneDayDiary(DiaryVO vo) throws Exception;
	
	public List<Map<String, Object>> getOneDayEmotion(DiaryVO vo) throws Exception;
	
	// 일기 기록 날짜까지 price 합계
	public int getSum(DiaryVO vo) throws Exception;
	
	// 점수 저장
	public void insertScore(DiaryVO vo) throws Exception;
	
	// 코멘트랑 점수 있는거 있자너 그거 가져와
	public DiaryVO getScoreComment(DiaryVO vo) throws Exception;

}
