package org.gm.persistence;

import java.util.List;

import org.gm.domain.ChartVO;
import org.gm.domain.LarVO;
import org.gm.domain.SearchCalendarVO;

public interface LarDAO {

	public void insert(LarVO vo)throws Exception;
	
	public List<LarVO> memLarMonthList(LarVO vo)throws Exception;
	
	public List<LarVO> memOneMonthLarList(String mno,String year,String month)throws Exception;
	
	public List<LarVO> defaultmemOneMonthLarList(String mno)throws Exception;
	//lar2입니다
	public List<LarVO> defaultmemOneMonthLarList2(ChartVO vo)throws Exception;
	
	public List<LarVO> searchLarSumList(SearchCalendarVO vo)throws Exception;
	
}
