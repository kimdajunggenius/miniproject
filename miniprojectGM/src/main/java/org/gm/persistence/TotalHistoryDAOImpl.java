package org.gm.persistence;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.HistoryMemberChartVO;
import org.gm.domain.HistoryStoreChart;
import org.gm.domain.MonthTotalVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class TotalHistoryDAOImpl implements TotalHistoryDAO {
	
	@Inject SqlSessionTemplate sqlSession;
	
	private Logger logger = LoggerFactory.getLogger(TotalHistoryDAOImpl.class);

	@Override
	public List<MonthTotalVO> getTotalMonthly(Integer year) {
		logger.info("get montly total....................");
		return sqlSession.selectList("org.gm.persistence.historyMapper.totalHistoryMonthly", year);
	}

	@Override
	public List<ChartVO> getTotalDaily(MonthTotalVO vo) {
		logger.info("get Daily total....................");
		return sqlSession.selectList("org.gm.persistence.historyMapper.totalHistoryDaily", vo);
	}

	@Override
	public List<HistoryStoreChart> getTotalPieAnnuallyLCategory(Map<String, String> period) {
		return sqlSession.selectList("org.gm.persistence.historyMapper.PeriodLCategory", period);
	}

	@Override
	public List<HistoryStoreChart> getTotalPieAnnuallyMCategory(Map<String, String> period) {
		return sqlSession.selectList("org.gm.persistence.historyMapper.PeriodMCategory", period);
	}

	@Override
	public List<HistoryStoreChart> getTotalPieAnnuallyLocal(Map<String, String> period) {
		return sqlSession.selectList("org.gm.persistence.historyMapper.PeriodLocal", period);
	}

	@Override
	public List<HistoryMemberChartVO> getTotalPieAnnuallyPaytype(Map<String, String> period) {
		return sqlSession.selectList("org.gm.persistence.historyMapper.PeriodPay", period);
	}

	@Override
	public List<HistoryMemberChartVO> getTotalPieAnnuallyGender(Map<String, String> period) {
		return sqlSession.selectList("org.gm.persistence.historyMapper.PeriodGender", period);
	}

	@Override
	public List<HistoryMemberChartVO> getTotalPieAnnuallyAge(Map<String, String> period) {
		return sqlSession.selectList("org.gm.persistence.historyMapper.PeriodAge", period);
	}

	@Override
	public List<HistoryMemberChartVO> getTotalPieAnnuallyMemaddress(Map<String, String> period) {
		return sqlSession.selectList("org.gm.persistence.historyMapper.PeriodMemaddress", period);
	}
	
	

}
