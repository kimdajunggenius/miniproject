
package org.gm.persistence;

import java.util.List;

import javax.inject.Inject;

import org.gm.domain.LcategoryVO;
import org.gm.domain.McategoryVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class McategoryDAOImpl implements McategoryDAO {
	
	@Inject
	private SqlSessionTemplate sql;
	
	private Logger logger = LoggerFactory.getLogger(McategoryDAOImpl.class);

	@Override
	public List<McategoryVO> mcategoryAllList(McategoryVO vo) throws Exception {

		logger.info("중분류 카테고리 가져오기...."+vo);
		return sql.selectList("org.gm.persistence.categoryMapper.mcategoryList",vo);
	}

	@Override
	public McategoryVO mcategoryOneList(McategoryVO vo) throws Exception {

		
		return sql.selectOne("org.gm.persistence.categoryMapper.mcategoryOne",vo);

	}

	@Override
	public void mcategoryInsert(McategoryVO vo) throws Exception {

		sql.insert("org.gm.persistence.categoryMapper.mcategoryInsert", vo);
	}

	@Override
	public void mcategoryUpdate(McategoryVO vo) throws Exception {

		sql.update("org.gm.persistence.categoryMapper.mcategoryUpdate", vo);

	}

	@Override
	public void mcategoryDelete(Integer mcno) throws Exception {

		sql.delete("org.gm.persistence.categoryMapper.mcategoryDelete", mcno);

	}

}