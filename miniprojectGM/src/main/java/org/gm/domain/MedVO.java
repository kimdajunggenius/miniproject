package org.gm.domain;

public class MedVO {

	private Integer medno,price;
	private String mno,med,year,month;
	public Integer getMedno() {
		return medno;
	}
	public void setMedno(Integer medno) {
		this.medno = medno;
	}

	 
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getMno() {
		return mno;
	}
	public void setMno(String mno) {
		this.mno = mno;
	}
	public String getMed() {
		return med;
	}
	public void setMed(String med) {
		this.med = med;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	@Override
	public String toString() {
		return "MedVO [medno=" + medno + ", price=" + price + ", mno=" + mno + ", med=" + med + ", year=" + year
				+ ", month=" + month + "]";
	}
	 
	
	
	
}
