package org.gm.domain;

import java.sql.Date;

public class HistoryStoreChart {
	
	private Integer hno, price;
	private String lar, med, address;
	private Date date;
	public Integer getHno() {
		return hno;
	}
	public void setHno(Integer hno) {
		this.hno = hno;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getLar() {
		return lar;
	}
	public void setLar(String lar) {
		this.lar = lar;
	}
	public String getMed() {
		return med;
	}
	public void setMed(String med) {
		this.med = med;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "HistoryStoreChart [hno=" + hno + ", price=" + price + ", lar=" + lar + ", med=" + med + ", address="
				+ address + ", date=" + date + "]";
	}
	
}
