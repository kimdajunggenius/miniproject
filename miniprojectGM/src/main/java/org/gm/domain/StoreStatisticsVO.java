package org.gm.domain;

import java.util.Date;

public class StoreStatisticsVO {
	
	Integer age, sno, price; 
	Integer year, month, day, hour;
	String gender, before, after, dateonly, address;
	
	
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDateonly() {
		return dateonly;
	}
	public void setDateonly(String dateonly) {
		this.dateonly = dateonly;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Integer getSno() {
		return sno;
	}
	public void setSno(Integer sno) {
		this.sno = sno;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getDay() {
		return day;
	}
	public void setDay(Integer day) {
		this.day = day;
	}
	public Integer getHour() {
		return hour;
	}
	public void setHour(Integer hour) {
		this.hour = hour;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBefore() {
		return before;
	}
	public void setBefore(String before) {
		this.before = before;
	}
	public String getAfter() {
		return after;
	}
	public void setAfter(String after) {
		this.after = after;
	}
	@Override
	public String toString() {
		return "StoreStatisticsVO [age=" + age + ", sno=" + sno + ", price=" + price + ", year=" + year + ", month="
				+ month + ", day=" + day + ", hour=" + hour + ", gender=" + gender + ", before=" + before + ", after="
				+ after + ", dateonly=" + dateonly + ", address=" + address + "]";
	}	
}
