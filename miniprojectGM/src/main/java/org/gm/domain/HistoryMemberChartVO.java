package org.gm.domain;

import java.sql.Date;

public class HistoryMemberChartVO {
	
	private Integer hno;
	private Date date;
	private String paytype, gender, age, memaddress;
	private Integer price;
	
	public Integer getHno() {
		return hno;
	}
	public void setHno(Integer hno) {
		this.hno = hno;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getPaytype() {
		return paytype;
	}
	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getMemaddress() {
		return memaddress;
	}
	public void setMemaddress(String memaddress) {
		this.memaddress = memaddress;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "HistoryMemberChartVO [hno=" + hno + ", date=" + date + ", paytype=" + paytype + ", gender=" + gender
				+ ", age=" + age + ", memaddress=" + memaddress + ", price=" + price + "]";
	}
	
}
