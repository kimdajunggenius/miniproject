package org.gm.domain;

public class SearchStoreVO {
	
	String search, lar, med;
	Integer lcno, mcno, page;
	
	
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public String getLar() {
		return lar;
	}
	public void setLar(String lar) {
		this.lar = lar;
	}
	public String getMed() {
		return med;
	}
	public void setMed(String med) {
		this.med = med;
	}
	public Integer getLcno() {
		return lcno;
	}
	public void setLcno(Integer lcno) {
		this.lcno = lcno;
	}
	public Integer getMcno() {
		return mcno;
	}
	public void setMcno(Integer mcno) {
		this.mcno = mcno;
	}
	@Override
	public String toString() {
		return "SearchStoreVO [search=" + search + ", lar=" + lar + ", med=" + med + ", lcno=" + lcno + ", mcno=" + mcno
				+ ", page=" + page + "]";
	}

}
