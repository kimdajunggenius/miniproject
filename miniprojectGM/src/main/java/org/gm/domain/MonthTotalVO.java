package org.gm.domain;

public class MonthTotalVO {
	Integer year;
	Integer month;
	Integer total;
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	@Override
	public String toString() {
		return "MonthTotalVO [year=" + year + ", month=" + month + ", total=" + total + "]";
	}
	

}
