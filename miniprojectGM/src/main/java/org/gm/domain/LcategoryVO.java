package org.gm.domain;

import java.sql.Date;

public class LcategoryVO {

	private Integer lcno;
	private String lar,state;
	private Date regdate,modidate;
	public Integer getLcno() {
		return lcno;
	}
	public void setLcno(Integer lcno) {
		this.lcno = lcno;
	}
	public String getLar() {
		return lar;
	}
	public void setLar(String lar) {
		this.lar = lar;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getModidate() {
		return modidate;
	}
	public void setModidate(Date modidate) {
		this.modidate = modidate;
	}
	@Override
	public String toString() {
		return "LcategoryVO [lcno=" + lcno + ", lar=" + lar + ", state=" + state + ", regdate=" + regdate
				+ ", modidate=" + modidate + "]";
	}
	
	
}
