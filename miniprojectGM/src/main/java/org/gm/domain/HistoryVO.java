package org.gm.domain;

public class HistoryVO {

	private Integer hno,sno,price,emono;
	private String mno,sname,mtype;
	private Double lat,lng;
	private String btime, paytype,state;
	
	public Integer getHno() {
		return hno;
	}
	public void setHno(Integer hno) {
		this.hno = hno;
	}
	public Integer getSno() {
		return sno;
	}
	public void setSno(Integer sno) {
		this.sno = sno;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getMno() {
		return mno;
	}
	public void setMno(String mno) {
		this.mno = mno;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getMtype() {
		return mtype;
	}
	public void setMtype(String mtype) {
		this.mtype = mtype;
	}
	
	public Double getLat() {
		return lat;
	}
	public String getBtime() {
		return btime;
	}
	public void setBtime(String btime) {
		this.btime = btime;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLng() {
		return lng;
	}
	public void setLng(Double lng) {
		this.lng = lng;
	}
	public Integer getEmono() {
		return emono;
	}
	public void setEmono(Integer emono) {
		this.emono = emono;
	}
	public String getPaytype() {
		return paytype;
	}
	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Override
	public String toString() {
		return "HistoryVO [hno=" + hno + ", sno=" + sno + ", price=" + price + ", emono=" + emono + ", mno=" + mno
				+ ", sname=" + sname + ", mtype=" + mtype + ", btime=" + btime + ", lat=" + lat + ", lng=" + lng
				+ ", paytype=" + paytype + ", state=" + state + "]";
	}
	
}
