package org.gm.domain;

public class CategoryVO {
	
	private Integer catno;
	private String lar;
	private String med;
	public Integer getCatno() {
		return catno;
	}
	public void setCatno(Integer catno) {
		this.catno = catno;
	}
	public String getLar() {
		return lar;
	}
	public void setLar(String lar) {
		this.lar = lar;
	}
	public String getMed() {
		return med;
	}
	public void setMed(String med) {
		this.med = med;
	}
	@Override
	public String toString() {
		return "CategoryVO [catno=" + catno + ", lar=" + lar + ", med=" + med + "]";
	}
}
