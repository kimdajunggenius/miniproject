package org.gm.domain;

public class LarVO {

	private Integer larno,price;
	private String mno,lar,year,month;
	public Integer getLarno() {
		return larno;
	}
	public void setLarno(Integer larno) {
		this.larno = larno;
	}

	
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getMno() {
		return mno;
	}
	public void setMno(String mno) {
		this.mno = mno;
	}
	public String getLar() {
		return lar;
	}
	public void setLar(String lar) {
		this.lar = lar;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	@Override
	public String toString() {
		return "LarVO [larno=" + larno + ", price=" + price + ", mno=" + mno + ", lar=" + lar + ", year=" + year
				+ ", month=" + month + "]";
	}
	
	
	
}
