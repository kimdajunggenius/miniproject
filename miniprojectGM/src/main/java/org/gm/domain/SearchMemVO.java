package org.gm.domain;

public class SearchMemVO {

	private Integer page;
	private String gender,location,keyword,age;
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
 
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "SearchMemVO [page=" + page + ", gender=" + gender + ", location=" + location + ", keyword=" + keyword
				+ ", age=" + age + "]";
	}
 
	
	
}
