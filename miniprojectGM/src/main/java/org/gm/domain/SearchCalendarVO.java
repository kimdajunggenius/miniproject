package org.gm.domain;

public class SearchCalendarVO {

	private String mno,before,after;

	public String getMno() {
		return mno;
	}

	public void setMno(String mno) {
		this.mno = mno;
	}

	public String getBefore() {
		return before;
	}

	public void setBefore(String before) {
		this.before = before;
	}

	public String getAfter() {
		return after;
	}

	public void setAfter(String after) {
		this.after = after;
	}

	@Override
	public String toString() {
		return "SearchCalendarVO [mno=" + mno + ", before=" + before + ", after=" + after + "]";
	}
	
}
