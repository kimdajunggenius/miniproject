package org.gm.domain;

public class RecommendChartVO {
	private Integer rno,count;
	private String lar,med,who;
	public Integer getRno() {
		return rno;
	}
	public void setRno(Integer rno) {
		this.rno = rno;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public String getLar() {
		return lar;
	}
	public void setLar(String lar) {
		this.lar = lar;
	}
	public String getMed() {
		return med;
	}
	public void setMed(String med) {
		this.med = med;
	}
	public String getWho() {
		return who;
	}
	public void setWho(String who) {
		this.who = who;
	}
	@Override
	public String toString() {
		return "RecommendChartVO [rno=" + rno + ", count=" + count + ", lar=" + lar + ", med=" + med + ", who=" + who
				+ "]";
	}
	
	

}
