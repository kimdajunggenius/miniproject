package org.gm.domain;

import java.util.Date;

public class MonthlyAverageVO {
	
	private Integer cno,budget,total, age;
	private Integer day1,day2,day3,day4,day5,day6,day7,day8,day9,day10,day11,day12,day13,
	day14,day15,day16,day17,day18,day19,day20,day21,day22,day23,day24,day25,day26,day27,day28
	,day29,day30,day31;
	private Double avg;
	private Date birthday;
	private String mno,year,month,gender;
	public Integer getCno() {
		return cno;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public void setCno(Integer cno) {
		this.cno = cno;
	}
	public Integer getBudget() {
		return budget;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public void setBudget(Integer budget) {
		this.budget = budget;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getDay1() {
		return day1;
	}
	public void setDay1(Integer day1) {
		this.day1 = day1;
	}
	public Integer getDay2() {
		return day2;
	}
	public void setDay2(Integer day2) {
		this.day2 = day2;
	}
	public Integer getDay3() {
		return day3;
	}
	public void setDay3(Integer day3) {
		this.day3 = day3;
	}
	public Integer getDay4() {
		return day4;
	}
	public void setDay4(Integer day4) {
		this.day4 = day4;
	}
	public Integer getDay5() {
		return day5;
	}
	public void setDay5(Integer day5) {
		this.day5 = day5;
	}
	public Integer getDay6() {
		return day6;
	}
	public void setDay6(Integer day6) {
		this.day6 = day6;
	}
	public Integer getDay7() {
		return day7;
	}
	public void setDay7(Integer day7) {
		this.day7 = day7;
	}
	public Integer getDay8() {
		return day8;
	}
	public void setDay8(Integer day8) {
		this.day8 = day8;
	}
	public Integer getDay9() {
		return day9;
	}
	public void setDay9(Integer day9) {
		this.day9 = day9;
	}
	public Integer getDay10() {
		return day10;
	}
	public void setDay10(Integer day10) {
		this.day10 = day10;
	}
	public Integer getDay11() {
		return day11;
	}
	public void setDay11(Integer day11) {
		this.day11 = day11;
	}
	public Integer getDay12() {
		return day12;
	}
	public void setDay12(Integer day12) {
		this.day12 = day12;
	}
	public Integer getDay13() {
		return day13;
	}
	public void setDay13(Integer day13) {
		this.day13 = day13;
	}
	public Integer getDay14() {
		return day14;
	}
	public void setDay14(Integer day14) {
		this.day14 = day14;
	}
	public Integer getDay15() {
		return day15;
	}
	public void setDay15(Integer day15) {
		this.day15 = day15;
	}
	public Integer getDay16() {
		return day16;
	}
	public void setDay16(Integer day16) {
		this.day16 = day16;
	}
	public Integer getDay17() {
		return day17;
	}
	public void setDay17(Integer day17) {
		this.day17 = day17;
	}
	public Integer getDay18() {
		return day18;
	}
	public void setDay18(Integer day18) {
		this.day18 = day18;
	}
	public Integer getDay19() {
		return day19;
	}
	public void setDay19(Integer day19) {
		this.day19 = day19;
	}
	public Integer getDay20() {
		return day20;
	}
	public void setDay20(Integer day20) {
		this.day20 = day20;
	}
	public Integer getDay21() {
		return day21;
	}
	public void setDay21(Integer day21) {
		this.day21 = day21;
	}
	public Integer getDay22() {
		return day22;
	}
	public void setDay22(Integer day22) {
		this.day22 = day22;
	}
	public Integer getDay23() {
		return day23;
	}
	public void setDay23(Integer day23) {
		this.day23 = day23;
	}
	public Integer getDay24() {
		return day24;
	}
	public void setDay24(Integer day24) {
		this.day24 = day24;
	}
	public Integer getDay25() {
		return day25;
	}
	public void setDay25(Integer day25) {
		this.day25 = day25;
	}
	public Integer getDay26() {
		return day26;
	}
	public void setDay26(Integer day26) {
		this.day26 = day26;
	}
	public Integer getDay27() {
		return day27;
	}
	public void setDay27(Integer day27) {
		this.day27 = day27;
	}
	public Integer getDay28() {
		return day28;
	}
	public void setDay28(Integer day28) {
		this.day28 = day28;
	}
	public Integer getDay29() {
		return day29;
	}
	public void setDay29(Integer day29) {
		this.day29 = day29;
	}
	public Integer getDay30() {
		return day30;
	}
	public void setDay30(Integer day30) {
		this.day30 = day30;
	}
	public Integer getDay31() {
		return day31;
	}
	public void setDay31(Integer day31) {
		this.day31 = day31;
	}
	public Double getAvg() {
		return avg;
	}
	public void setAvg(Double avg) {
		this.avg = avg;
	}
	public String getMno() {
		return mno;
	}
	public void setMno(String mno) {
		this.mno = mno;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "MonthlyAverageVO [cno=" + cno + ", budget=" + budget + ", total=" + total + ", age=" + age + ", day1="
				+ day1 + ", day2=" + day2 + ", day3=" + day3 + ", day4=" + day4 + ", day5=" + day5 + ", day6=" + day6
				+ ", day7=" + day7 + ", day8=" + day8 + ", day9=" + day9 + ", day10=" + day10 + ", day11=" + day11
				+ ", day12=" + day12 + ", day13=" + day13 + ", day14=" + day14 + ", day15=" + day15 + ", day16=" + day16
				+ ", day17=" + day17 + ", day18=" + day18 + ", day19=" + day19 + ", day20=" + day20 + ", day21=" + day21
				+ ", day22=" + day22 + ", day23=" + day23 + ", day24=" + day24 + ", day25=" + day25 + ", day26=" + day26
				+ ", day27=" + day27 + ", day28=" + day28 + ", day29=" + day29 + ", day30=" + day30 + ", day31=" + day31
				+ ", avg=" + avg + ", birthday=" + birthday + ", mno=" + mno + ", year=" + year + ", month=" + month
				+ ", gender=" + gender + "]";
	}
	
	

}
