package org.gm.domain;

public class StoreVO {

	private Integer sno;
	private Double lat,lng;
	private String sname,lar,med,address,cstate;
	private String state;
	
	
	public Integer getSno() {
		return sno;
	}
	public void setSno(Integer sno) {
		this.sno = sno;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLng() {
		return lng;
	}
	public void setLng(Double lng) {
		this.lng = lng;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getLar() {
		return lar;
	}
	public void setLar(String lar) {
		this.lar = lar;
	}
	public String getMed() {
		return med;
	}
	public void setMed(String med) {
		this.med = med;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCstate() {
		return cstate;
	}
	public void setCstate(String cstate) {
		this.cstate = cstate;
	}
	@Override
	public String toString() {
		return "StoreVO [sno=" + sno + ", lat=" + lat + ", lng=" + lng + ", sname=" + sname + ", lar=" + lar + ", med="
				+ med + ", address=" + address + ", cstate=" + cstate + ", state=" + state + "]";
	}
	
	

}
