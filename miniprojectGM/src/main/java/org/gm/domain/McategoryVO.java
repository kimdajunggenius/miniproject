package org.gm.domain;

import java.sql.Date;

public class McategoryVO {

	private Integer mcno,lcno;
	private String med,state;
	private Date regdate,modidate;
	public Integer getMcno() {
		return mcno;
	}
	public void setMcno(Integer mcno) {
		this.mcno = mcno;
	}
	public Integer getLcno() {
		return lcno;
	}
	public void setLcno(Integer lcno) {
		this.lcno = lcno;
	}
	public String getMed() {
		return med;
	}
	public void setMed(String med) {
		this.med = med;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getModidate() {
		return modidate;
	}
	public void setModidate(Date modidate) {
		this.modidate = modidate;
	}
	@Override
	public String toString() {
		return "McategoryVO [mcno=" + mcno + ", lcno=" + lcno + ", med=" + med + ", state=" + state + ", regdate="
				+ regdate + ", modidate=" + modidate + "]";
	}
	
	
}
