package org.gm.domain;

public class EmoticonVO {
	
	private Integer emono, stage;
	private String emotion,emoticon,state,word1,word2,word3,checked;
	public Integer getEmono() {
		return emono;
	}
	public void setEmono(Integer emono) {
		this.emono = emono;
	}
	public Integer getStage() {
		return stage;
	}
	public void setStage(Integer stage) {
		this.stage = stage;
	}
	public String getEmotion() {
		return emotion;
	}
	public void setEmotion(String emotion) {
		this.emotion = emotion;
	}
	public String getEmoticon() {
		return emoticon;
	}
	public void setEmoticon(String emoticon) {
		this.emoticon = emoticon;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getWord1() {
		return word1;
	}
	public void setWord1(String word1) {
		this.word1 = word1;
	}
	public String getWord2() {
		return word2;
	}
	public void setWord2(String word2) {
		this.word2 = word2;
	}
	public String getWord3() {
		return word3;
	}
	public void setWord3(String word3) {
		this.word3 = word3;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	@Override
	public String toString() {
		return "EmoticonVO [emono=" + emono + ", stage=" + stage + ", emotion=" + emotion + ", emoticon=" + emoticon
				+ ", state=" + state + ", word1=" + word1 + ", word2=" + word2 + ", word3=" + word3 + ", checked="
				+ checked + "]";
	}
	
	

	
	

}
