package org.gm.domain;

import java.sql.Date;

public class RecommendVO {
	
	private Integer recno;
	private String timezone,lar,med,recommend,mno;
	private Date date, first, last;
	
	public Date getFirst() {
		return first;
	}
	public void setFirst(Date first) {
		this.first = first;
	}
	public Date getLast() {
		return last;
	}
	public void setLast(Date last) {
		this.last = last;
	}
	public Integer getRecno() {
		return recno;
	}
	public void setRecno(Integer recno) {
		this.recno = recno;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getLar() {
		return lar;
	}
	public void setLar(String lar) {
		this.lar = lar;
	}
	public String getMed() {
		return med;
	}
	public void setMed(String med) {
		this.med = med;
	}
	public String getRecommend() {
		return recommend;
	}
	public void setRecommend(String recommend) {
		this.recommend = recommend;
	}
	public String getMno() {
		return mno;
	}
	public void setMno(String mno) {
		this.mno = mno;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		
		this.date = date;
	}
	@Override
	public String toString() {
		return "RecommendVO [recno=" + recno + ", timezone=" + timezone + ", lar=" + lar + ", med=" + med
				+ ", recommend=" + recommend + ", mno=" + mno + ", date=" + date + ", first=" + first + ", last=" + last
				+ "]";
	}
}
