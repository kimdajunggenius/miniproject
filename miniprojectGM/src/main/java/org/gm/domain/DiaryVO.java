package org.gm.domain;

public class DiaryVO {

	private Integer diaryno,price,hno;
	private Integer escore, hscore;
	private String mno,sname, lar_ment,emoticon_ment,time_ment;
	private String ecomment, hcomment;
	private String eventtime,regdate;
	public Integer getDiaryno() {
		return diaryno;
	}
	public DiaryVO setDiaryno(Integer diaryno) {
		this.diaryno = diaryno;
		return this;
	}
	public Integer getPrice() {
		return price;
	}
	public DiaryVO setPrice(Integer price) {
		this.price = price;
		return this;
	}
	public Integer getHno() {
		return hno;
	}
	public DiaryVO setHno(Integer hno) {
		this.hno = hno;
		return this;
	}
	public String getMno() {
		return mno;
	}
	public DiaryVO setMno(String mno) {
		this.mno = mno;
		return this;
	}
	public String getSname() {
		return sname;
	}
	public DiaryVO setSname(String sname) {
		this.sname = sname;
		return this;
	}
	public String getLar_ment() {
		return lar_ment;
	}
	public DiaryVO setLar_ment(String lar_ment) {
		this.lar_ment = lar_ment;
		return this;
	}
	public String getEmoticon_ment() {
		return emoticon_ment;
	}
	public DiaryVO setEmoticon_ment(String emoticon_ment) {
		this.emoticon_ment = emoticon_ment;
		return this;
	}
	public String getTime_ment() {
		return time_ment;
	}
	public DiaryVO setTime_ment(String time_ment) {
		this.time_ment = time_ment;
		return this;
	}
	public String getEventtime() {
		return eventtime;
	}
	public DiaryVO setEventtime(String eventtime) {
		this.eventtime = eventtime;
		return this;
	}
	public String getRegdate() {
		return regdate;
	}
	public DiaryVO setRegdate(String regdate) {
		this.regdate = regdate;
		return this;
	}
	public Integer getEscore() {
		return escore;
	}
	public DiaryVO setEscore(Integer escore) {
		this.escore = escore;
		return this;
	}
	public Integer getHscore() {
		return hscore;
	}
	public DiaryVO setHscore(Integer hscore) {
		this.hscore = hscore;
		return this;
	}
	public String getEcomment() {
		return ecomment;
	}
	public DiaryVO setEcomment(String ecomment) {
		this.ecomment = ecomment;
		return this;
	}
	public String getHcomment() {
		return hcomment;
	}
	public DiaryVO setHcomment(String hcomment) {
		this.hcomment = hcomment;
		return this;
	}
	@Override
	public String toString() {
		return "DiaryVO [diaryno=" + diaryno + ", price=" + price + ", hno=" + hno + ", escore=" + escore + ", hscore="
				+ hscore + ", mno=" + mno + ", sname=" + sname + ", lar_ment=" + lar_ment + ", emoticon_ment="
				+ emoticon_ment + ", time_ment=" + time_ment + ", ecomment=" + ecomment + ", hcomment=" + hcomment
				+ ", eventtime=" + eventtime + ", regdate=" + regdate + "]";
	}
	
}
