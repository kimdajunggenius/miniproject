package org.gm.domain;

public class PageMaker {

	private Integer page=1, total ,start, end;
	private boolean prev,next;
	
	public Integer getStart() {
		return start;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		if(page<1){
			this.page=1;
			return;
		}
		
		this.page = page;
	}
	public Integer getTotal() {
		return total;
	}
	
	public void setTotal(Integer total) {

		if(total < 1){
			return;
		}
		
		this.total = total;
		
		calcPage();
	}
	private void calcPage() {
		
		int tempEnd = (int)(Math.ceil(page/10.0) * 10);
		
		this.start = tempEnd - 9; 
		
		if(tempEnd * 10 > this.total){
			this.end = (int)Math.ceil(this.total / 10.0);
		}else{
			this.end = tempEnd;
		}
		
		this.prev = this.start != 1;
		
		this.next = this.end * 10 < this.total;

	}
	
	public Integer getEnd() {
		return end;
	}
	public boolean isPrev() {
		return prev;
	}
	public void setPrev(boolean prev) {
		this.prev = prev;
	}
	public boolean isNext() {
		return next;
	}
	public void setNext(boolean next) {
		this.next = next;
	}
	@Override
	public String toString() {
		return "PageMaker [page=" + page + ", total=" + total + ", start=" + start + ", end=" + end + ", prev=" + prev
				+ ", next=" + next + "]";
	}
	
 
	
	
	
	
}
