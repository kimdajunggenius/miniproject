package org.gm.controller;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.gm.domain.ChartVO;
import org.gm.domain.LarVO;
import org.gm.domain.MemberVO;
import org.gm.domain.PageMaker;
import org.gm.domain.SearchCalendarVO;
import org.gm.domain.SearchMemVO;
import org.gm.dto.LoginDTO;
import org.gm.service.MemberDAOService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/member/")
public class MemberController {

	@Inject
	private MemberDAOService mdao;
	
	//로그인책 (GET)webstorm
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public void memberLogingGET(@ModelAttribute("dto") LoginDTO dto) throws Exception{
		
	}
	
	
	//로그인책 (POST)
	@RequestMapping(value = "/loginPost/", method = RequestMethod.POST)
	public String loginPOST(@RequestBody LoginDTO dto, MemberVO vo)throws Exception{		
		
		System.out.println("멤버 브이오를 확인해 보십시오!!!!!!!!!!!!!!!!!!!!!!!!!" + vo);
		System.out.println("멤버 디디디디티티티티티토오오오오오오 확인해 보십시오!!!!!!!!!!!!!!!!!!!!!!!!!" + dto);
		System.out.println(" 디티오" + dto.getMno());
		System.out.println(" 디티오" + dto.getMpw());
		
		vo.setMno(dto.getMno());
		vo.setMpw(dto.getMpw());
		System.out.println(" 비오" + vo.getMno());
		System.out.println("뷔오 " + vo.getMpw());
		
		System.out.println("멤버 뷔오 확인해 보십시오!!!!!!!!!!!!!!!!!!!!!!!!!" + vo);
		
		//로그인 가능정보
//		mdao.checkLogin(vo);
		
		System.out.println("멤버 브이오를 확인해 보십시오@@@@@@@@@@@@@@@@@@@@@@@@@" + vo);
		

		//계정있는거 확인하고 리턴
		if( mdao.checkLoging(vo).getMno() == vo.getMno() && mdao.checkLoging(vo).getMpw() == vo.getMpw()){
			return "true";
		}

		return "false";
	}
	
	
	//로그인 백업
	/*@RequestMapping(value="/login", method=RequestMethod.POST)
	public Map<String, Object> memberLoging(MemberVO vo) throws Exception{
		
		Map<String, Object> map = new HashMap<>();
		map.put("MemberInfo", mdao.checkLoging(vo));
		
		return map;
	}*/
	
	@RequestMapping(value="/check", method=RequestMethod.POST)
	public Integer checkLogin(MemberVO vo) throws Exception{
		Integer value = mdao.checkLogin(vo);
		System.out.println("check...default................."+value);
		return value;
	}
	
/*	@RequestMapping(value="/checkf/{email}", method=RequestMethod.POST)
	public Integer checkLoginFacebook(@PathVariable("email") String email) throws Exception{
		Integer facebookValue = mdao.checkLoginFacebook(email);
		System.out.println("check...facebook................."+facebookValue);
		System.out.println("check...facebook................."+email);
		return facebookValue;
	}*/
	
	@RequestMapping(value="/all/",method=RequestMethod.GET)
	public Map<String, Object> memberAllList()throws Exception{
		Map<String, Object> map = new HashMap<>();
		map.put("MemberList",mdao.getMemberList(1));
		PageMaker pageMaker= new PageMaker();
		pageMaker.setTotal(mdao.getMemberTotal());
		map.put("pageMaker", pageMaker);
		
		return map;
	}
	
	@RequestMapping(value="/all/{page}",method=RequestMethod.GET)
	public Map<String,Object> memberPageList(@PathVariable("page") Integer page)throws Exception{
		Map<String, Object> map = new HashMap<>();
		map.put("MemberList",mdao.getMemberList(page));
		PageMaker pageMaker= new PageMaker();
		pageMaker.setPage(page);
		pageMaker.setTotal(mdao.getMemberTotal());
		map.put("pageMaker", pageMaker);
		
		return map;
	}
	//회원가입
	@RequestMapping(value="/",method=RequestMethod.POST)
	public void memberCreate(@RequestBody MemberVO vo)throws Exception{

		
		mdao.createMember(vo);
	}
	
	@RequestMapping(value="/getmember/",method=RequestMethod.POST)
	public MemberVO getMember(@RequestBody MemberVO vo)throws Exception{

		
		return mdao.getMember(vo.getMno());
	}
	
	@RequestMapping(value="/",method=RequestMethod.PUT)
	public void memberUpdate(@RequestBody MemberVO vo)throws Exception{

		
		mdao.updateMember(vo);
	}
	
	@RequestMapping(value="/",method=RequestMethod.DELETE)
	public void memberDelete(@RequestBody String mno)throws Exception{		
		mdao.deleteMember(mno);
	}
	@RequestMapping(value="/chart/",method=RequestMethod.POST)
	public Map<String,Object> memberOneMonthChartList(@RequestBody LarVO vo)throws Exception{

 
		Map<String, Object> map = new HashMap<>();
		map.put("memChartList",mdao.memLarMedChartList(vo.getMno(), vo.getYear(), vo.getMonth()));
		map.put("getMember",mdao.getMember(vo.getMno()));
		return map;
	}
	
	@RequestMapping(value="/searchchart/",method=RequestMethod.POST)
	public Map<String,Object> searchmemberChartList(@RequestBody SearchCalendarVO vo)throws Exception{

 		
		return mdao.searchCalChart(vo);
	}
	

	
	@RequestMapping(value="/defaultchart/",method=RequestMethod.POST)
	public Map<String,Object> defaultmemberOneMonthChartList(@RequestBody MemberVO vo)throws Exception{

		System.out.println("mnnnnnnnnmmmmmmmmmmoooooooooooooooooo"+vo.getMno());
		return mdao.defaultmemLarMedChartList(vo.getMno());
		
	}
	
	@RequestMapping(value="/defaultchart2/",method=RequestMethod.POST)
	public Map<String,Object> defaultmemberOneMonthChartList2(@RequestBody ChartVO vo)throws Exception{

		System.out.println("~~~~~~~~~~~defaultchart2~~~~~~~~~~~~~"+vo.getMno());
		return mdao.defaultmemLarMedChartList2(vo);
		
	}
	
	@RequestMapping(value="/search",method=RequestMethod.POST)
	public Map<String,Object> memberSearch(@RequestBody SearchMemVO vo)throws Exception{

		
		return mdao.searchMem(vo);
	}
	
	@RequestMapping(value="/jsontest",method=RequestMethod.POST)
	public String Jteset(@RequestBody MemberVO vo)throws Exception{

		System.out.println("JSON TEST!!!!!!!!!!     "+vo);
		
		if(mdao.checkLoginFacebook(vo)==1){
			return "OK";
		}else{
			return "False";
		}
		
	
	}
	
	@RequestMapping(value="/getstate",method=RequestMethod.POST)
	public String getState(@RequestBody MemberVO vo)throws Exception{
		System.out.println("VO가 뭐 들어오나 보자잉~ :"+vo);
		MemberVO vo2= mdao.getMember(vo.getMno());
		String modeState="";
		System.out.println("무슨 모드게 전 : "+vo2.getMode());
		if(vo2.getMode()==null){
			modeState="A";
		}else{
			if(vo2.getMode().equals("천사")){
				modeState="A";
			} else {
				modeState="D";
			}
	
		}	
		System.out.println("무슨 모드게 후 : "+modeState);
		return modeState;
	}
	
//	@CrossOrigin
//	@RequestMapping(value="/search2",method=RequestMethod.GET)
//	public Map<String,Object> searchmemberGET(SearchMemVO vo)throws Exception{
//		System.out.println("占쏙옙占쏙옙講占쏙옙求째占�2");
//		System.out.println(vo);
//		
//		return mdao.searchMem(vo);
//	}
//	
	
}
