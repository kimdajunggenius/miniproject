package org.gm.controller;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.gm.domain.LcategoryVO;
import org.gm.domain.McategoryVO;
import org.gm.persistence.LcategoryDAO;
import org.gm.persistence.McategoryDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/category")
public class CategoryController {

   @Inject
   LcategoryDAO ldao;
   
   @Inject
   McategoryDAO mdao;
   
   Logger logger = LoggerFactory.getLogger(CategoryController.class);
   
   //리스트 다뿌리는것. JSON으로 바꿈
   @RequestMapping(value="/larAll", method=RequestMethod.GET) // url은 간단하게 
   public List<LcategoryVO> lcategoryAllList()throws Exception{
      
      List<LcategoryVO> list=ldao.lcategoryAllList();
      
      return list;
   }
   
   //대분류 하나씩 읽어오기
   @RequestMapping(value="/lar", method=RequestMethod.GET)   // url 대분류 중분류 구분할 수 있게햇음
   public LcategoryVO lcategoryOneList(LcategoryVO vo)throws Exception{
      return ldao.lcategoryOneList(vo);
   }
   
   //대분류 생성
   @RequestMapping(value="/lar", method=RequestMethod.POST)   // url을 동일하게 주고 method 만 다른 방식으로 주기
   public void lcategoryInsert(@RequestBody LcategoryVO vo)throws Exception{
      logger.info("create category lar ..................."+vo);
      ldao.lcategoryInsert(vo);
      
   }
   // 대분류 수정
   @RequestMapping(value="/lar", method=RequestMethod.PUT)
   public void lcategoryUpdate(@RequestBody LcategoryVO vo)throws Exception{
      System.out.println("here.....updating......."+vo);
      
      ldao.lcategoryUpdate(vo);
   }
   
   // 대분류 삭제
   @RequestMapping(value="/lar", method=RequestMethod.DELETE)
   public void lcategoryDelete(@RequestBody Integer lcno)throws Exception{
      logger.info("L Category delete................."+lcno);
      ldao.lcategoryDelete(lcno);
   }
   
   //중분류 JSON데이터만들기
   
   @RequestMapping(value="/medAll", method=RequestMethod.GET)
   public List<McategoryVO> mcategoryAllList(McategoryVO vo)throws Exception{	 

	   List<McategoryVO> list=mdao.mcategoryAllList(vo);
	   
	   return list;
   }
   //중분류 하나읽기
   @RequestMapping(value="/med", method=RequestMethod.GET)
   public McategoryVO mcategoryOneList(McategoryVO vo)throws Exception{
	   return mdao.mcategoryOneList(vo);
   }
   
   //중분류 추가
   @RequestMapping(value="/med",method=RequestMethod.POST)
   public void mcategoryInsert(@RequestBody McategoryVO vo)throws Exception{
	   System.out.println("here.................."+vo); 
	   mdao.mcategoryInsert(vo);
   }
   
   //중분류 수정
   @RequestMapping(value="/med",method=RequestMethod.PUT)
   public void mcategoryUpdate(@RequestBody McategoryVO vo)throws Exception{
	   logger.info("M Category update.............."+vo);
	   mdao.mcategoryUpdate(vo);
   }
   
   //중분류 삭제
   @RequestMapping(value="/med",method=RequestMethod.DELETE)
   public void mcategoryDelete(@RequestBody Integer mcno)throws Exception{
	   mdao.mcategoryDelete(mcno);
   }
}