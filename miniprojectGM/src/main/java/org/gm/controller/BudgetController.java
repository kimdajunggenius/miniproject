package org.gm.controller;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.persistence.ChartDAO;
import org.gm.service.ChartDAOService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/budget/")
public class BudgetController {
	
	@Inject
	private ChartDAO cdao;
	
	@Inject
	private ChartDAOService cdaos;
	
	@RequestMapping(value="/oneBudget/{mno}/",method=RequestMethod.GET)
	public Integer budgetList(@PathVariable("mno") String mno)throws Exception{
		return cdao.memBudget(mno);	
		
	}
	
	@RequestMapping(value="/twoBudget/{mno}/", method=RequestMethod.GET)
	public Integer nextBudgetList(@PathVariable("mno") String mno)throws Exception{
		return cdao.nextBudget(mno);
	}
	
	@RequestMapping(value="/threeBudget/{mno}/", method=RequestMethod.GET)
	public Integer nextNextBudgetList(@PathVariable("mno") String mno)throws Exception{
		return cdao.nextNextBudget(mno);
	}
	
	@RequestMapping(value="/budgetUpdate", method=RequestMethod.PUT)
	public void oneBudgetUpdate(@RequestBody ChartVO vo)throws Exception{
		cdaos.modifyOneBudget(vo);
	}
	
	@RequestMapping(value="/budgetUpdateTwo", method=RequestMethod.PUT)
	public void twoBudgetUpdate(@RequestBody ChartVO vo)throws Exception{
		cdaos.modifyTwoBudget(vo);
	}
	
	@RequestMapping(value="/budgetUpdateThree", method=RequestMethod.PUT)
	public void threeBudgetUpdate(@RequestBody ChartVO vo)throws Exception{
		cdaos.modifyThreeBudget(vo);
	}
	
	
	
	
}
