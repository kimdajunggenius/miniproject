package org.gm.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.LarVO;
import org.gm.domain.MedVO;
import org.gm.domain.MonthlyAverageVO;
import org.gm.persistence.MonthlyAverageDAO;
import org.gm.service.HistoryDAOService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/average/")
public class AverageController {

	@Inject
	private MonthlyAverageDAO mdao;
	
	@CrossOrigin
	@RequestMapping(value="/gender/",method=RequestMethod.POST)
	public Map<String, Object> genderMonthList(@RequestBody MonthlyAverageVO vo)throws Exception{
		System.out.println("/gender!!");
		
		Map<String, Object>map = new HashMap<>();
		map.put("genderList", mdao.getMonthlygender((vo.getYear()),(vo.getMonth())));
		map.put("budgetList", mdao.getMonthlybudget(vo.getYear(),vo.getMonth()));
		map.put("ageList", mdao.getMonthlyage(vo.getYear(),vo.getMonth()));
		return map;
	}

	
	@CrossOrigin
	@RequestMapping(value="/age/",method=RequestMethod.POST)
	public Map<String, Object> ageMonthList(@RequestBody MonthlyAverageVO vo)throws Exception{
		System.out.println("/age!!");
		
		Map<String, Object>map = new HashMap<>();
		
		
		return map;
	}
	
	@CrossOrigin
	@RequestMapping(value="/budget/",method=RequestMethod.POST)
	public Map<String, Object> budgetMonthList(@RequestBody MonthlyAverageVO vo)throws Exception{
		System.out.println("/budget!!");
		
		Map<String, Object>map = new HashMap<>();
		
		
		return map;
	}
	
}
