package org.gm.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.HistoryVO;
import org.gm.domain.LarVO;
import org.gm.domain.MedVO;
import org.gm.domain.PageMaker;
import org.gm.persistence.HistoryDAO;
import org.gm.service.HistoryDAOService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin
@RequestMapping("/history")
public class UserHistoryController {

	@Inject
	private HistoryDAOService hdaos;
	
	@Inject
	private HistoryDAO hdao;
	
	//히스토리 리스트
	@CrossOrigin
	@RequestMapping(value="/historylist/{page}", method=RequestMethod.GET)
	public Map<String, Object> historyList(@PathVariable("page") Integer page) throws Exception{
		System.out.println("히스토리 리스트입니다!!");
		System.out.println(page);
		Map<String, Object> map = new HashMap<>();
		PageMaker pageMaker= new PageMaker();
		pageMaker.setPage(page);
		pageMaker.setTotal(hdao.totalcount());
		
		map.put("historyList", hdao.pageList(page));
		map.put("pageMaker", pageMaker);
		
		return map;
	}
	
	@CrossOrigin
	@RequestMapping(value="/historylist", method=RequestMethod.GET)
	public Map<String, Object> historyList() throws Exception{
		System.out.println("히스토리 리스트입니다!!");
		Map<String, Object> map = new HashMap<>();
		PageMaker pageMaker= new PageMaker();
		pageMaker.setPage(1);
		pageMaker.setTotal(hdao.totalcount());
		
		map.put("historyList", hdao.pageList(1));
		map.put("pageMaker", pageMaker);
		
		return map;
	}
	
	
	
	
	@RequestMapping(value="/med/",method=RequestMethod.POST)
	public Map<String, Object> memMedMonthList(@RequestBody MedVO vo)throws Exception{
		System.out.println("mmmmmmmmmmeeeeedddddddddLIST......");
		Map<String, Object>map = new HashMap<>();
		
		map.put("memMedList", hdaos.memMedMonList(vo));
		
		return map;
	}

	@RequestMapping(value="/lar/",method=RequestMethod.POST)
	public Map<String, Object> memLarMonthList(@RequestBody LarVO vo)throws Exception{
		System.out.println("LaaaaaaarrrrrrrrrrrrrLIST......");
		Map<String, Object>map = new HashMap<>();
		
		map.put("memLarList", hdaos.memLarMonList(vo));
		
		return map;
	}

	@RequestMapping(value="/chart/",method=RequestMethod.POST)
	public Map<String, Object> memChartMonthList(@RequestBody ChartVO vo)throws Exception{
		System.out.println("LaaaaaaarrrrrrrrrrrrrLIST......");
		Map<String, Object>map = new HashMap<>();
		
		map.put("memChartList",hdaos.memChartMonthList(vo));
		
		return map;
	}

	@RequestMapping(value="/avgchart/",method=RequestMethod.POST)
	public Map<String, Object> memChartAvgMonthList(@RequestBody ChartVO vo)throws Exception{
		System.out.println("LaaaaaaarrrrrrrrrrrrrLIST......");
		Map<String, Object>map = new HashMap<>();
		
		map.put("memAvgChartList",hdaos.avgChartNowMonthPreList(vo));
		
		return map;
	}
	
	// hno를 이용해 history 하나 가져오기 (안드로이드)
	@RequestMapping(value="/one", method=RequestMethod.GET)
	public HistoryVO oneHistory(Integer hno) throws Exception{
		
		return hdaos.getOne(hno);
	}
		
	// mno, btime을 이용해 하루 history 가져오기 (안드로이드)
	@RequestMapping(value="/oneday", method=RequestMethod.GET)
	public List<HistoryVO> oneDayHistory(HistoryVO vo) throws Exception{
		
		System.out.println("하루 history 가져와 볼까?.........................."+vo);
		
		return hdao.getOneDay(vo);
	}
	
	@RequestMapping(value="/nodetest/",method=RequestMethod.POST)
	public void nodetest(@RequestBody HistoryVO vo)throws Exception{
		System.out.println("노드 테스트 VO 뭐들어오나 보자 !!");
		
		System.out.println(vo);
 
//		SimpleDateFormat date =new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		
//		Date todate = date.parse("2016-08-12 17:33:12");
 	
//		System.out.println(todate);
//		vo.setMno("Ramssang");
//		vo.setSname("�ѻ��");

//		vo.setPrice(3000);
//		vo.setBtime(todate);
 
		
		hdaos.insert(vo);
		
	}

	
	//유저결제내역수정
	@RequestMapping(value="/oneUpdate", method=RequestMethod.PUT)
	public void memOneHistoryUpdate(@RequestBody HistoryVO vo)throws Exception{
		
		hdao.memhistoryListUpdate(vo);
	}
	
	//유저결제내역 추가
	@RequestMapping(value="/oneInsert", method=RequestMethod.POST)
	public void memhistoryListInsert(@RequestBody HistoryVO vo)throws Exception{
		 hdao.insert(vo);	
	}
	
	// 유저결제내역 삭제
	
	@RequestMapping(value="/oneDelete", method=RequestMethod.DELETE)
	public void memOneHistoryDelete(@RequestBody Integer hno)throws Exception{
		
		hdao.oneDelete(hno);
	}
	
	
	
	
}
