package org.gm.controller;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.PageMaker;
import org.gm.domain.RecommendVO;
import org.gm.domain.SearchStoreVO;
import org.gm.domain.StoreStatisticsVO;
import org.gm.domain.StoreVO;
import org.gm.persistence.RecommendDAO;
import org.gm.service.MemberDAOService;
import org.gm.service.StoreDAOService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/store/")
public class StoreController {

	@Inject
	StoreDAOService sdao;
	
	@Inject
	RecommendDAO rdao;
	
	@CrossOrigin
	@RequestMapping(value="/storelist/{page}",method=RequestMethod.GET)//���� ���
	public Map<String, Object> storeList(@PathVariable("page") Integer page)throws Exception{
		
		Map<String, Object> map = new HashMap<>();
		
		PageMaker pageMaker= new PageMaker();
		pageMaker.setPage(page);
		pageMaker.setTotal(sdao.getTotalStore());
		
		map.put("storeList", sdao.storeList(page));
		map.put("pageMaker", pageMaker);
		
		return map;
	}
	
	@CrossOrigin
	@RequestMapping(value="/storelist/",method=RequestMethod.GET)//���� ���
	public Map<String, Object> storeList()throws Exception{
		
		Map<String, Object> map = new HashMap<>();
		
		PageMaker pageMaker= new PageMaker();
		pageMaker.setPage(1);
		pageMaker.setTotal(sdao.getTotalStore());
		
		map.put("storeList", sdao.storeList(1));
		map.put("pageMaker", pageMaker);
		
		return map;
	}
	
	@CrossOrigin
	@RequestMapping(value="/insert/",method=RequestMethod.POST)
	public Integer insertStore(@RequestBody StoreVO vo)throws Exception{
		
		System.out.println("inserting..........................................."+vo);
		sdao.insertStore(vo);
		return 0;
	}
	
	
	@CrossOrigin
	@RequestMapping(value="/specific/{sno}",method=RequestMethod.GET)//���� ���
	public StoreVO specificStore(@PathVariable("sno") Integer sno)throws Exception{
		
		return sdao.getOneStore(sno);
	}
	
	@CrossOrigin
	@RequestMapping(value="/update/",method=RequestMethod.POST)
	public Integer updateStore(@RequestBody StoreVO vo)throws Exception{
		
		System.out.println("here!!"+vo);
		vo.setState("N");
		System.out.println("here!!222222222"+vo);
		sdao.updateStore(vo);
		return 0;
	}
	
	@CrossOrigin
	@RequestMapping(value="/delete/{sno}",method=RequestMethod.GET)
	public void deleteStore(@PathVariable("sno") Integer sno)throws Exception{

		sdao.deleteStore(sno);
		
	}
	
	@CrossOrigin
	@RequestMapping(value="/restore/{sno}",method=RequestMethod.GET)
	public void restoreStore(@PathVariable("sno") Integer sno)throws Exception{

		sdao.restoreStore(sno);
		
	}
	
	@CrossOrigin
	@RequestMapping(value="/nullstorelist/{page}",method=RequestMethod.GET)
	public  Map<String, Object> storeNullList(@PathVariable("page") Integer page)throws Exception{

		Map<String, Object> map2 = new HashMap<>();
		
		PageMaker pageMaker= new PageMaker();
		pageMaker.setPage(page);
		pageMaker.setTotal(sdao.getNullStore());
		
		map2.put("storeList", sdao.storeNullList(page));
		map2.put("pageMaker", pageMaker);
		System.out.println("nullstorelist................................."+map2);
		return map2;
		
	}
	
	@CrossOrigin
	@RequestMapping(value="/largecategory/",method=RequestMethod.GET)
	public  Map<String, Object> getLcategoryList()throws Exception{

		Map<String, Object> map = new HashMap<>();
		

		map.put("categoryList", sdao.getLcategoryList());
		System.out.println("LargeCategoryList................................."+map);
		return map;
		
	}
	
	@CrossOrigin
	@RequestMapping(value="/mediumcategory/{lcno}",method=RequestMethod.GET)
	public  Map<String, Object> getMcategoryList(@PathVariable("lcno") Integer lcno)throws Exception{

		Map<String, Object> map = new HashMap<>();

		map.put("categoryList", sdao.getMcategoryList(lcno));
		System.out.println("MediumCategoryList................................."+map);
		return map;
		
	}
	
	
	@CrossOrigin
	@RequestMapping(value="/search/",method=RequestMethod.POST)
	public  Map<String, Object> searchList(@RequestBody SearchStoreVO vo)throws Exception{

		
		Map<String, Object> map = new HashMap<>();
		map.put("storeList", sdao.searchStoreList(vo));
		vo.setPage((vo.getPage()+10)/10);
		System.out.println("searching..............................."+vo);
		PageMaker pageMaker= new PageMaker();
		pageMaker.setPage(vo.getPage());
		pageMaker.setTotal(sdao.getSearchStore(vo));
		map.put("pageMaker", pageMaker);
		
		System.out.println("searching........to map........................"+map);
		return map;
		
	}
	
	@CrossOrigin
	@RequestMapping(value="/yearly/",method=RequestMethod.POST)
	public  Map<String, Object> yearlyList(@RequestBody StoreStatisticsVO vo)throws Exception{

		
		Map<String, Object> map = new HashMap<>();
		map.put("storeList", sdao.getMonthlyChart(vo));
		System.out.println("yearly........to map........................"+map);
		return map;
		
	}
	
	@CrossOrigin
	@RequestMapping(value="/daily/",method=RequestMethod.POST)
	public  Map<String, Object> dailyList(@RequestBody StoreStatisticsVO vo)throws Exception{

		
		Map<String, Object> map = new HashMap<>();
		map.put("storeList", sdao.getDailyChart(vo));
		System.out.println("daily........to map........................"+map);
		return map;
		
	}
	
	@CrossOrigin
	@RequestMapping(value="/recommend/",method=RequestMethod.POST)
	public  Map<String, Object> getList(RecommendVO vo)throws Exception{

		System.out.println("vo........"+vo);
		Map<String, Object> map = new HashMap<>();
		map.put("storeList", rdao.getList(vo));
		System.out.println("recommend........to map........................"+map);
		return map;
		
	}	 
}
