package org.gm.controller;

import java.util.List;

import javax.inject.Inject;

import org.gm.domain.DiaryVO;
import org.gm.persistence.DiaryDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(value="/diary")
public class DiaryController {
	
	@Inject DiaryDAO ddao;
	
	Logger logger = LoggerFactory.getLogger(DiaryController.class);
	
	@RequestMapping(value="/getOneDiary", method=RequestMethod.GET)
	public List<DiaryVO> getOneDayDiary(DiaryVO vo) throws Exception{
		logger.info("다이어리 문장들 가져와봥,,,,,,,,,,"+vo);
		return ddao.getOneDayDiary(vo);
	}
	
	@RequestMapping(value="/getOneScore", method=RequestMethod.GET)
	public DiaryVO getOneDayScore(DiaryVO vo) throws Exception{
		logger.info("다이어리 코멘트 가져와봥,,,,,,,,,,"+vo);
		return ddao.getScoreComment(vo);
	}
	

}
