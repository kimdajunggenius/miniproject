package org.gm.controller;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.gm.domain.EmoticonVO;
import org.gm.domain.PageMaker;
import org.gm.persistence.EmoticonDAO;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin
@RestController
@RequestMapping("/emoticon")
public class EmoticonController {
	
	
	@Inject
	EmoticonDAO Edao;
	
//	@Resource(name="uploadPath")
//	private String uploadPath;
//	
//	//파일업로드
//	@RequestMapping(value = "/uploadform", method=RequestMethod.POST)
//	public void uploadForm(MultipartFile file, Model model) throws Exception{
//		
//		System.out.println("originalName: " + file.getOriginalFilename());
//		System.out.println("Size: " + file.getSize());
//		System.out.println("contentType: " + file.getContentType());
//		
//		String savedName = uploadFile(file.getOriginalFilename(),file.getBytes());
//		
//		model.addAttribute("savedName", savedName);
//	}
//	//실제 업로드해준다
//	private String uploadFile(String originalName, byte[] fileData)throws Exception{
//		
//		UUID uid = UUID.randomUUID(); //고유한 값
//		String savedName = uid.toString() + "_"+originalName;
//		File target = new File(uploadPath,savedName);
//		
//		FileCopyUtils.copy(fileData, target);
//		
//		return savedName;
//	}
	
	
	//리스트뿌리기(dz)
	@RequestMapping(value="/list/{page}", method=RequestMethod.GET)
	public Map<String, Object> emoticonList(@PathVariable("page") Integer page ) throws Exception{
				
		Map<String, Object> map = new HashMap<>();
		PageMaker pageMaker= new PageMaker();
		pageMaker.setPage(page);
		pageMaker.setTotal(Edao.totalcount());// 총 갯수
		
		map.put("list", Edao.selectList(page));
		map.put("page", pageMaker);
		
		return map;
	}
	
	//조건검색
//	@RequestMapping(value="/like/1", method=RequestMethod.GET)
//	public Map<String, Object> emoticonLike() throws Exception{
//		
//		Map<String, Object> map = new HashMap<>();
//		Edao.selectLike(vo, page, word);
//		
//		return map;
//	}
	
	//추가하기
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public void insert(@RequestBody EmoticonVO vo) throws Exception{
		System.out.println("생성!!!!!!!!!?................" + vo);
		
		Edao.insert(vo);
	}
	
	//수정하기(dz)
	@RequestMapping(value="/modify", method=RequestMethod.POST)
	public void update(@RequestBody EmoticonVO vo) throws Exception{
		System.out.println("수정!!!!!!!!!?................" + vo);
		
		Edao.update(vo);

	}
	
	//삭제하기(dz)
	@RequestMapping(value="/delete", method=RequestMethod.PUT)
	public void delete(@ RequestBody EmoticonVO vo) throws Exception{
		System.out.println("삭제!!!!!!!!!!!?................" + vo);
		Edao.delete(vo);
	}
		

}
