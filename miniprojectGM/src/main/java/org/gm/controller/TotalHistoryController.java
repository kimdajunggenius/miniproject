package org.gm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.HistoryMemberChartVO;
import org.gm.domain.HistoryStoreChart;
import org.gm.domain.MonthTotalVO;
import org.gm.persistence.TotalHistoryDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/history")
public class TotalHistoryController {
	
	@Inject private TotalHistoryDAO dao;
	Logger logger = LoggerFactory.getLogger(TotalHistoryController.class);
	
	// for line graph
	@RequestMapping(value="/monthly_total", method=RequestMethod.GET)
	public List<MonthTotalVO> annuallyTotalLine(Integer year) throws Exception {
		return dao.getTotalMonthly(year);
	}
	@RequestMapping(value="/daily_total", method=RequestMethod.GET)
	public List<ChartVO> monthlyTotalLine(MonthTotalVO vo) throws Exception {
		return dao.getTotalDaily(vo);
	}
	
	// for pie graph (store)
	
	@RequestMapping(value = "/annuallyLar", method = RequestMethod.GET)
	public List<HistoryStoreChart> annuallyLarPie(String start, String end) throws Exception {
		
		logger.info("get annually Large Category data for drawing pie graph..........");
		return dao.getTotalPieAnnuallyLCategory(convert(start, end));
	}
	
	@RequestMapping(value = "/annuallyMed", method = RequestMethod.GET)
	public List<HistoryStoreChart> annuallyMedPie(String start, String end) throws Exception {
		return dao.getTotalPieAnnuallyMCategory(convert(start, end));
	}
	
	@RequestMapping(value = "/annuallyAddress", method = RequestMethod.GET)
	public List<HistoryStoreChart> annuallyLocPie(String start, String end) throws Exception {
		return dao.getTotalPieAnnuallyLocal(convert(start, end));
	}
	
	// for pie graph (member)
	
	@RequestMapping(value = "/annuallyPaytype", method = RequestMethod.GET)
	public List<HistoryMemberChartVO> annuallyPaytypePie(String start, String end) throws Exception {
		return dao.getTotalPieAnnuallyPaytype(convert(start, end));
	}
	
	@RequestMapping(value = "/annuallyGender", method = RequestMethod.GET)
	public List<HistoryMemberChartVO> annuallyGenderPie(String start, String end) throws Exception {
		return dao.getTotalPieAnnuallyGender(convert(start, end));
	}
	
	@RequestMapping(value = "/annuallyAge", method = RequestMethod.GET)
	public List<HistoryMemberChartVO> annuallyAgePie(String start, String end) throws Exception {
		return dao.getTotalPieAnnuallyAge(convert(start, end));
	}
	
	@RequestMapping(value = "/annuallyMemaddress", method = RequestMethod.GET)
	public List<HistoryMemberChartVO> annuallyMemaddressPie(String start, String end) throws Exception {
		return dao.getTotalPieAnnuallyMemaddress(convert(start, end));
	}
	
	public Map<String, String> convert (String start, String end){
		Map<String, String> period = new HashMap<>();
		period.put("start", start);
		period.put("end", end);
		return period;
	}

}
