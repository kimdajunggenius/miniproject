package org.gm.service;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.LarVO;
import org.gm.domain.MedVO;
import org.gm.domain.MemberVO;
import org.gm.domain.PageMaker;
import org.gm.domain.SearchCalendarVO;
import org.gm.domain.SearchMemVO;
import org.gm.dto.LoginDTO;
import org.gm.persistence.ChartDAO;
import org.gm.persistence.LarDAO;
import org.gm.persistence.MedDAO;
import org.gm.persistence.MemberDAO;
import org.gm.persistence.SearchMemDAO;
import org.ram.controller.HayunTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class MemberDAOServiceImpl implements MemberDAOService {
	
	private static final Logger logger = LoggerFactory.getLogger(MemberDAOServiceImpl.class);

	@Inject
	private MemberDAO memdao;

	@Inject
	private MedDAO mdao;
	
	@Inject
	private LarDAO ldao;
	
	@Inject
	private ChartDAO cdao;
	
	@Inject
	private SearchMemDAO srdao;
	
	
	
	//로그인체크
	@Override
	public MemberVO checkLoging(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		return memdao.checkLoging(vo);
	}

	@Override
	public void createMember(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		memdao.createMember(vo);
	}

	@Override
	public void updateMember(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		memdao.updateMember(vo);
	}

	@Override
	public void memberToAdmin(String mno) throws Exception {
		// TODO Auto-generated method stub
		memdao.memberToAdmin(mno);
	}

	@Override
	public void adminToMember(String mno) throws Exception {
		// TODO Auto-generated method stub
		memdao.adminToMember(mno);
	}

	@Override
	public void deleteMember(String mno) throws Exception {
		// TODO Auto-generated method stub
		memdao.deleteMember(mno);
	}

	@Override
	public List<MemberVO> getMemberList(Integer page) throws Exception {
		// TODO Auto-generated method stub
		return memdao.getMemberList(page);
	}

	@Override
	public MemberVO getMember(String mno) throws Exception {
		// TODO Auto-generated method stub
		return memdao.getMember(mno);
	}

	@Override
	public Integer getMemberTotal() throws Exception {
		// TODO Auto-generated method stub
		return memdao.getMemberTotal();
	}

//	��� �Ѵ� lar med chart 
	
	@Override
	public Map<String, Object> memLarMedChartList(String mno, String year, String month) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		
		map.put("memOneMonthMedList", mdao.memOneMonthMedList(mno, year, month));
		map.put("memOneMonthLarList", ldao.memOneMonthLarList(mno, year, month));
		map.put("memAvgChartNowMonthPreList", cdao.memAvgChartNowMonthPreList(mno));
		map.put("memChartAllList", cdao.memChartAllList(mno));
		map.put("memChartMonthList", cdao.memChartMonthList(mno, year, month));		 		
		
		return map;
	}
	
	@Override
	public Map<String, Object> defaultmemLarMedChartList(String mno) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		
		MemberVO vo = memdao.getMember(mno);
		Integer budget=800000;
		if(cdao.memBudget(mno)!=null){
			budget =cdao.memBudget(mno);
		}
		
		map.put("defaultMedList", mdao.defaultmemOneMonthMedList(mno));
		map.put("defaultLarList", ldao.defaultmemOneMonthLarList(mno));
		
		
		map.put("getMember", vo);
				 	
		
//		�ֵ� �ʱⰪ ��������  �����Ʈ���� ����
		map.put("defaultChartList",cdao.defaultmemChartMonthList(mno)); //����޲���������
		map.put("memAvgChartNowMonthPreList", cdao.memAvgChartNowMonthPreList(mno)); // ������� ���� �� ���
		

		
		map.put("budAvgChart",cdao.budAvgChart(budget));
		map.put("budGenAgeAvgChart",cdao.budGenAgeAvgChart(vo.getGender(), budget, vo.getBirthday()));
		
		return map;
	}
	/*
//	년도랑 월 값 받아서 사용자의 1달 정보 보기  ,년도랑 월 값 받아서 받은 월 값의 전달꺼까지 더한 평균 정보 보기  (바꾸어야합ㄴ디ㅏ)
	
	@Override
	public Map<String, Object> defaultmemLarMedChartList2(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		Integer budget=800000;
		if(cdao.memBudget(vo.getMno())!=null){
			budget =cdao.memBudget(vo.getMno());
		}
		
//		map.put("defaultMedList2", mdao.defaultmemOneMonthMedList2(vo);
//		map.put("defaultLarList2", ldao.defaultmemOneMonthLarList2(vo);
		
		map.put("defaultchartMonthList2",cdao.defaultmemChartMonthList2(vo));
		map.put("avgChartNowMonthPreList2",cdao.avgChartNowMonthPreList2(vo));

		return map;
	}
	*/
	
//	년도랑 월 값 받아서 사용자의 1달 정보 보기  ,년도랑 월 값 받아서 받은 월 값의 전달꺼까지 더한 평균 정보 보기  
	
	@Override
	public Map<String, Object> defaultmemLarMedChartList2(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		String gender = memdao.getMember(vo.getMno()).getGender();
		Date birthday = memdao.getMember(vo.getMno()).getBirthday();

		ChartVO vo2=cdao.chartMonthList(vo);
		logger.info("RAMSANG TEST DEFAULT"+"들어오는 VO : "+vo);
		logger.info("RAMSANG TEST DEFAULT"+"들어오는 VO2 : "+vo2);
	
		System.out.println("RAMSANG TEST DEFAULT"+"들어오는 VO : "+vo);
		System.out.println("RAMSANG TEST DEFAULT"+"들어오는 VO2 : "+vo2);


		/*if(vo2.getBudget()==null){
			vo2.setBudget(900000);
		}*/
		System.out.println("MemberDAOServiceImpl Gender= "+gender+" birthday= "+birthday);
		System.out.println("ChartVO2는 뭘까요 :"+vo2);
		map.put("defaultchartMonthList2",cdao.defaultmemChartMonthList2(vo));
		map.put("avgChartNowMonthPreList2",cdao.avgChartNowMonthPreList2(vo));
		map.put("budGenAgeAvgChart2", cdao.budGenAgeAvgChart2(vo2, gender, birthday));
		
		
		return map;
	}
	
	
	

//	�˻� ���� ���̵�
	


	@Override
	public Map<String, Object> searchMem(SearchMemVO vo) throws Exception {
		// TODO Auto-generated method stub
		Integer page = vo.getPage();
		if(page == null || page<1){
			page=1;
		}
		System.out.println("dddd");
		
		PageMaker pageMaker = new PageMaker();
		pageMaker.setPage(page);
		pageMaker.setTotal(srdao.searchCount(vo));
		Map<String, Object>map = new HashMap<>();
		map.put("sarchMem", srdao.searchMem(vo));
		map.put("pageMaker", pageMaker);
		return map;
	}

	@Override
	public Map<String, Object> searchCalChart(SearchCalendarVO vo) throws Exception {

		Map<String,Object>map = new HashMap<>();
		
		map.put("searchMedSumList",mdao.searchMedSumList(vo));
		map.put("searchLarSumList",ldao.searchLarSumList(vo));
		map.put("searchChartSumList",cdao.searchChartList(vo));
		return map;
	}

	@Override
	public Integer checkLogin(MemberVO vo) throws Exception {

		return memdao.checkLogin(vo);
	}

	@Override
	public Integer checkLoginFacebook(MemberVO vo) throws Exception {

		int check =memdao.checkLoginFacebook(vo);
		if(check ==0){
			memdao.facebookCreateMember(vo);
		}
				
		return 1;
		
	}

	/*@Override
	public MemberVO getMember(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		return memdao.getMember(vo);
	}*/
 

	
	
}
