package org.gm.service;

import java.util.List;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.HistoryVO;
import org.gm.domain.LarVO;
import org.gm.domain.MedVO;
import org.gm.persistence.ChartDAO;
import org.gm.persistence.HistoryDAO;
import org.gm.persistence.LarDAO;
import org.gm.persistence.MedDAO;
import org.springframework.stereotype.Service;

@Service
public class HistoryDAOServiceImpl implements HistoryDAOService {

	@Inject
	private HistoryDAO hdao;
	
	@Inject
	private MedDAO mdao;
	
	@Inject
	private LarDAO ldao;
	
	@Inject
	private ChartDAO cdao;
	
	@Override
	public List<HistoryVO> pageList(Integer page) throws Exception {
		// TODO Auto-generated method stub
		return hdao.pageList(page);
	}

	@Override
	public LarVO larList(LarVO vo) throws Exception {
		// TODO Auto-generated method stub
		return hdao.larList(vo);
	}

	@Override
	public MedVO medList(MedVO vo) throws Exception {
		// TODO Auto-generated method stub
		return hdao.medList(vo);
	}

//	�ߺз� ����Ʈ
	@Override
	public List<MedVO> memMedMonList(MedVO vo) throws Exception {
		// TODO Auto-generated method stub
		return mdao.memMedMonList(vo);
	}

//	��з� ����Ʈ
	@Override
	public List<LarVO> memLarMonList(LarVO vo) throws Exception {
		// TODO Auto-generated method stub
		return ldao.memLarMonthList(vo);
	}

	
//	��Ʈ ����Ʈ
	@Override
	public ChartVO memChartMonthList(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		return cdao.chartMonthList(vo);
	}

	@Override
	public ChartVO avgChartNowMonthPreList(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		return cdao.avgChartNowMonthPreList(vo);
	}

	@Override
	public void insert(HistoryVO vo) throws Exception {
		// TODO Auto-generated method stub
		hdao.insert(vo);
	}

	@Override
	public HistoryVO getOne(Integer hno) throws Exception {
		return hdao.getOne(hno);
	}

	
	@Override
	public void memHistoryListUpdate(HistoryVO vo) throws Exception {
		// TODO Auto-generated method stub
		
		hdao.memhistoryListUpdate(vo);
	}

	@Override
	public void memhistoryOneDelete(Integer hno) throws Exception {
		// TODO Auto-generated method stub
		hdao.memhistoryOneDelete(hno);
		
	}
	
	

	
	
}
