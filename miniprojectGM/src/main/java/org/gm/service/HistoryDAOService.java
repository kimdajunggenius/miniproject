package org.gm.service;

import java.util.List;

import org.gm.domain.ChartVO;
import org.gm.domain.HistoryVO;
import org.gm.domain.LarVO;
import org.gm.domain.MedVO;

public interface HistoryDAOService {

	public List<HistoryVO> pageList(Integer page)throws Exception;
	
	public LarVO larList(LarVO vo)throws Exception;
	
	public MedVO medList(MedVO vo)throws Exception;
	
//	�ߺз�����
	public List<MedVO> memMedMonList(MedVO vo)throws Exception;
	
//	��з� ����
	public List<LarVO> memLarMonList(LarVO vo)throws Exception;
	
//	��Ʈ ����
	public ChartVO memChartMonthList(ChartVO vo)throws Exception;
	
//	��Ʈ���� ���� �� ������ �� ���
	public ChartVO avgChartNowMonthPreList(ChartVO vo)throws Exception;
		
	public void insert(HistoryVO vo)throws Exception;
	
	public HistoryVO getOne(Integer hno) throws Exception;
	
	
	public void memHistoryListUpdate(HistoryVO vo)throws Exception;
	
	public void memhistoryOneDelete(Integer hno)throws Exception;
	
	
}
