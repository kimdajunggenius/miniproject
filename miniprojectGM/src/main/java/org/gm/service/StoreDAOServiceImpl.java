package org.gm.service;

import java.util.List;

import javax.inject.Inject;

import org.gm.domain.LcategoryVO;
import org.gm.domain.McategoryVO;
import org.gm.domain.SearchStoreVO;
import org.gm.domain.StoreStatisticsVO;
import org.gm.domain.StoreVO;
import org.gm.persistence.StoreDAO;
import org.springframework.stereotype.Service;

@Service
public class StoreDAOServiceImpl implements StoreDAOService{

	@Inject
	StoreDAO sdao;
	
	@Override
	public void insertStore(StoreVO vo) throws Exception {
		sdao.insertStore(vo);
	}

	@Override
	public void updateStore(StoreVO vo) throws Exception {
		sdao.updateStore(vo);
	}

	@Override
	public void deleteStore(Integer sno) throws Exception {
		sdao.deleteStore(sno);
	}

	@Override
	public Integer getTotalStore() throws Exception {
		
		return sdao.getTotalStore();
	}

	@Override
	public Integer getSearchStore(SearchStoreVO vo) throws Exception {
		return sdao.getSearchStore(vo);
	}

	@Override
	public List<StoreVO> storeList(Integer page) throws Exception {
		return sdao.storeList(page);
	}

	@Override
	public void restoreStore(Integer sno) throws Exception {
		sdao.restoreStore(sno);
	}

	@Override
	public List<StoreVO> storeNullList(Integer page) throws Exception {
		return sdao.storeNullList(page);
	}

	@Override
	public StoreVO getOneStore(Integer sno) throws Exception {
		return sdao.getOneStore(sno);
	}

	@Override
	public Integer getNullStore() throws Exception {
		return sdao.getNullStore();
	}

	@Override
	public List<LcategoryVO> getLcategoryList() throws Exception {
		return sdao.getLcategoryList();
	}

	@Override
	public List<McategoryVO> getMcategoryList(Integer lcno) throws Exception {
		return sdao.getMcategoryList(lcno);
	}

	@Override
	public List<StoreVO> searchStoreList(SearchStoreVO vo) throws Exception {	
		return sdao.searchStoreList(vo);
	}

	@Override
	public List<StoreStatisticsVO> getMonthlyChart(StoreStatisticsVO vo) throws Exception {
		return sdao.getMonthlyChart(vo);
	}

	@Override
	public List<StoreStatisticsVO> getDailyChart(StoreStatisticsVO vo) throws Exception {
		return sdao.getDailyChart(vo);
	}


}
