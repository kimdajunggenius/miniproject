package org.gm.service;
import java.util.List;

import javax.inject.Inject;

import org.gm.domain.MonthlyAverageVO;
import org.gm.persistence.MonthlyAverageDAO;
import org.springframework.stereotype.Service;

@Service
public class MonthlyAverageDAOServiceImpl implements MonthlyAverageDAOService {

	@Inject
	MonthlyAverageDAO mdao ;
	
	@Override
	public List<MonthlyAverageVO> getMonthlygender(String year, String month) throws Exception {
		// TODO Auto-generated method stub
		return mdao.getMonthlygender(year, month);
	}

	@Override
	public List<MonthlyAverageVO> getMonthlyAge(String year, String month) throws Exception {
		// TODO Auto-generated method stub
		return mdao.getMonthlyage(year, month);
	}

	@Override
	public List<MonthlyAverageVO> getMonthlyBudget(String year, String month) throws Exception {
		// TODO Auto-generated method stub
		return mdao.getMonthlybudget(year, month);
	}
	

}
