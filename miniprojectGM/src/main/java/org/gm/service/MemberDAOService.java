package org.gm.service;

import java.util.List;
import java.util.Map;

import org.gm.domain.ChartVO;
import org.gm.domain.LarVO;
import org.gm.domain.MedVO;
import org.gm.domain.MemberVO;
import org.gm.domain.SearchCalendarVO;
import org.gm.domain.SearchMemVO;
import org.gm.dto.LoginDTO;

public interface MemberDAOService {
	
	public void createMember(MemberVO vo)throws Exception;
	
	public void updateMember(MemberVO vo)throws Exception;
	
	public void memberToAdmin(String mno)throws Exception;
	
	public void adminToMember(String mno)throws Exception;
	
	public void deleteMember(String mno)throws Exception;
	
	public List<MemberVO> getMemberList(Integer page)throws Exception;
	
	public MemberVO getMember(String mno)throws Exception;
	
	public Integer getMemberTotal()throws Exception;
	
//	�����丮 ���� �ֵ�
	
	public Map<String, Object> memLarMedChartList(String mno, String year, String month)throws Exception;
	
	public Map<String,Object> defaultmemLarMedChartList(String mno)throws Exception;
	
// �˻� ���� ���̵�
	public Map<String,Object> searchMem(SearchMemVO vo)throws Exception;
	
	
//	�޷����� ��¥ �����ؼ� ��з� �ߺз� �������� 
	public Map<String,Object> searchCalChart(SearchCalendarVO vo)throws Exception;
	
//	checking login status (0: not login, 1: login)
	public Integer checkLogin(MemberVO vo) throws Exception;
	
	public Integer checkLoginFacebook(MemberVO vo) throws Exception;
	//lar2입니다
	public Map<String, Object> defaultmemLarMedChartList2(ChartVO vo) throws Exception;
	
	//로그인체크
	public MemberVO checkLoging(MemberVO vo) throws Exception;
	
//	public MemberVO getMember(MemberVO vo)throws Exception;
}
