package org.gm.service;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.persistence.ChartDAO;
import org.springframework.stereotype.Service;

@Service
public class ChartDAOServiceImpl implements ChartDAOService {
	
	@Inject
	private ChartDAO cdao;

	//첫번째달 예산이없다면 insert , 있다면 update
	@Override
	public void modifyOneBudget(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		//예산하나 가져오기
		Integer budget= cdao.memBudget(vo.getMno());
		System.out.println("dmdm 예산 가져와.............................");
		System.out.println(budget);
		if(budget==null){
			cdao.budgetInsert(vo);
		}else{
			cdao.budgetUpdate(vo);
		}
	}

	//두번째달 예산이없다면 insert , 있다면 update
	@Override
	public void modifyTwoBudget(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		
		Integer budget=cdao.nextBudget(vo.getMno());
		System.out.println("dmdm 두번째예산 가져와...........................");
		System.out.println(budget);
		if(budget==null){
			cdao.budgetInsert(vo);
		}else{
			cdao.budgetUpdate(vo);
		}
	}

	//세번째달 예산이 없다면 insert, 있다면 update
	@Override
	public void modifyThreeBudget(ChartVO vo) throws Exception {
		// TODO Auto-generated method stub
		

		Integer budget=cdao.nextNextBudget(vo.getMno());
		System.out.println("dmdm 세번째예산 가져와...........................");
		System.out.println(budget);
		if(budget==null){
			cdao.budgetInsert(vo);
		}else{
			cdao.budgetUpdate(vo);
		}
	}
	
	
	
	

}
