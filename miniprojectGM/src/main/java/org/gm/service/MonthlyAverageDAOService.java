package org.gm.service;

import java.util.List;

import org.gm.domain.MonthlyAverageVO;

public interface MonthlyAverageDAOService {
	
	public List<MonthlyAverageVO> getMonthlygender(String year, String month) throws Exception;
	public List<MonthlyAverageVO> getMonthlyAge(String year, String month) throws Exception;
	public List<MonthlyAverageVO> getMonthlyBudget(String year, String month) throws Exception;

}
