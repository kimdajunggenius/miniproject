package org.gm.service;

import org.gm.domain.ChartVO;

public interface ChartDAOService {
	
	// 예산이 없으면 insert하고 있으면 update하는 메소드
	public void modifyOneBudget(ChartVO vo) throws Exception;
	
	public void modifyTwoBudget(ChartVO vo)throws Exception;

	public void modifyThreeBudget(ChartVO vo)throws Exception;
}
