package org.gm.dto;

public class LoginDTO {
	
	private String mno,mpw;
	private boolean useCookie;
	public String getMno() {
		return mno;
	}
	public void setMno(String mno) {
		this.mno = mno;
	}
	public String getMpw() {
		return mpw;
	}
	public void setMpw(String mpw) {
		this.mpw = mpw;
	}
	public boolean isUseCookie() {
		return useCookie;
	}
	public void setUseCookie(boolean useCookie) {
		this.useCookie = useCookie;
	}
	@Override
	public String toString() {
		return "LoginDTO [mno=" + mno + ", mpw=" + mpw + ", useCookie=" + useCookie + "]";
	}
	
	

}
