package org.gm.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.MemberVO;
import org.gm.persistence.MemberDAO;
import org.gm.persistence.RecommendDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

public class MakeRecommend {

	private static final Logger logger = LoggerFactory.getLogger(MakeRecommend.class);

	@Inject
	private RecommendDAO rdao;

	@Inject
	private MemberDAO mdao;

	private String[] morningZone = { "아침", "04:00:00", "08:00:00", "08:00:00", "11:59:59" };
	private String[] afternoonZone = { "점심", "12:00:00", "16:00:00", "08:00:00", "19:59:59" };
	private String[] eveningZone = { "저녁", "20:00:00", "24:00:00", "00:00:00", "03:59:59" };

	@Scheduled(cron = "0 0 20 * * *")
	public void scheduleRun() {

		logger.info("사용자 가져오기...............");

		// 1.우리 앱의 사용자 mno 를 전부 가져옵니다.
		List<MemberVO> memberList = new ArrayList<>();
		try {
			memberList = mdao.getAllMemberList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Map<String, Object> tmp = new HashMap<>();
		String lar;

		// 2. STEP1 각 사용자가 가장좋아하는 가게 하나를 찾아봅시다.
		for (MemberVO memberVO : memberList) {
			try {
				// 아침 추천 만들기
				tmp = step1(memberVO.getMno(), morningZone);
				lar = tmp.get("lar").toString();
				step2(memberVO.getMno(), lar, morningZone);
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				tmp = step1(memberVO.getMno(), afternoonZone);
				lar = tmp.get("lar").toString();
				step2(memberVO.getMno(), lar, afternoonZone);
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				tmp = step1(memberVO.getMno(), eveningZone);
				lar = tmp.get("lar").toString();
				step2(memberVO.getMno(), lar, eveningZone);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// 1단계. 특정 사용자가 최근 30일 특정 시간대에 가장 좋아하는 가게중에 좋아요 1위이면서 가장 최근인 가게 하나 뽑기
	public Map<String, Object> step1(String mno, String[] timezone) throws Exception {
		logger.info(mno + "가 좋아하는 가게는요............");
		Map<String, Object> target = new HashMap<>();
		target.put("mno", mno);
		target.put("start1", timezone[1]);
		target.put("start2", timezone[2]);
		target.put("end1", timezone[3]);
		target.put("end2", timezone[4]);

		return rdao.getMemberFavoriteStore(target);
	}

	// 2단계. 특정 사용자가 최근 30일 아침에 가장 좋아한 가게를 좋아하는 다른 사용자의 mno, budget 뽑음
	public void step2(String mno, String lar, String[] timezone) {
		logger.info(mno + "가 가장 좋아하는 가게를 좋아하는 다른 사용자를 찾아보자...............");
		Map<String, Object> target = new HashMap<>();
		target.put("mno", mno);
		target.put("start1", timezone[1]);
		target.put("start2", timezone[2]);
		target.put("end1", timezone[3]);
		target.put("end2", timezone[4]);

		// 3. STEP2 사용자가 좋아하는 가게를 좋아하는 다른 사용자들을 찾아봅시다.(mno, budget)
		List<Map<String, Object>> memberList = new ArrayList<>();
		try {
			memberList = rdao.getResultMembers(target);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		for (Map<String, Object> map : memberList) {
			try {
				logger.info(map.get("mno") + "님이 이 가게를 좋아합니다.");
				step3(mno, lar, map, timezone);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	// 다른 사용자 한명이 좋아하는 가게를 뽑고 DB에 넣자
	// 3단계. 찾은 사람들이 가장 좋아하는 가게 찾기 (대분류는 비교 특정 사용자랑 같아야하고 그 특정 사용자가 안간 곳 중에서 찾기!)
	public void step3(String mno, String lar, Map<String, Object> another, String[] timezone){
		Map<String, Object> map = new HashMap<>();
		map.put("target", mno);
		map.put("another", another.get("mno"));
		map.put("start1", timezone[1]);
		map.put("start2", timezone[2]);
		map.put("end1", timezone[3]);
		map.put("end2", timezone[4]);
		map.put("lar", lar);

		Map<String, Object> recommendresult;
		try {
			recommendresult = rdao.getAnotherFavoriteStore(map);
			recommendresult.put("mno",mno);
			recommendresult.put("timezone", timezone[0]);
			rdao.insertRecommend(recommendresult);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
