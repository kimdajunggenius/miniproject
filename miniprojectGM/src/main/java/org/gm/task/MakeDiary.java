package org.gm.task;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.gm.domain.ChartVO;
import org.gm.domain.DiaryVO;
import org.gm.domain.MemberVO;
import org.gm.persistence.ChartDAO;
import org.gm.persistence.DiaryDAO;
import org.gm.persistence.MemberDAO;
import org.springframework.scheduling.annotation.Scheduled;

public class MakeDiary {
	
	@Inject private MemberDAO mdao;
	@Inject private DiaryDAO ddao;
	@Inject private ChartDAO cdao;
	
	private Map<String, String> dawn;
	private Map<String, String> morning;
	private Map<String, String> day;
	private Map<String, String> night;
	
	private Map<String, String> lar = new HashMap<>();
	private Map<Integer, String> good = new HashMap<>();
	private Map<Integer, String> bad = new HashMap<>();
	private Map<String, Map<Integer, String>> emoticon = new HashMap<>();
	
	DateFormat df;
	String yesterday;
	Calendar cal;
	public MakeDiary() {
		super();
		dawn = new HashMap<>();		morning = new HashMap<>();
		day = new HashMap<>();		night = new HashMap<>();
		lar = new HashMap<>();		good = new HashMap<>();
		bad = new HashMap<>();		emoticon = new HashMap<>();
		dawn.put("좋음", "피곤했지만 기운이 났다.^ ^");					dawn.put("나쁨", "피곤해 죽겠는데 미치게 만들었다.=_=");
		morning.put("좋음", "덕분에 기분 좋은 하루를 시작할 수 있었다.");	morning.put("나쁨", "아 침부터 기분이 거지 같았다.-_-^");
		day.put("좋음", "힘이 났다!");							day.put("나쁨", "힘이 빠졌다.ㅠ ㅠ");
		night.put("좋음", "기분 좋게 하루를 마감할 수 있었다.");			night.put("나쁨", "오늘 마무리가 너무 안좋았다...");
		
		lar.put("식비", "내 입을 ");				lar.put("문화생활비", "내 문화생활을 ");		lar.put("건강관리비", "내 건강을 ");		
		lar.put("품위유지비", "내 외모를 ");		lar.put("차량유지비", "내 애마를 ");		lar.put("주거생활비", "내 생활을 ");
		lar.put("교육비", "나의 지식을 ");			lar.put("인간관계비", "나의 인간관계를 ");	lar.put("유흥비", "나의 유흥을 ");
		lar.put("금융보험비", "나의 금융 관리를 ");	lar.put("저축", "나의 재산 관리를 ");		lar.put("생활편의", "내 생활에 필요한 것들을 ");
		
		good.put(1, "그냥 괜찮게 해주었고 ");		good.put(2, "흡족하게 해주었고 ");
		good.put(3, "즐거움으로 채워주었고 ");		good.put(4, "행복하게 만들어주었고 ");
		good.put(5, "정말 완벽하게 만족시켜주었고 ");
		
		bad.put(1, "그저 그렇게 만들어주었고 ");		bad.put(2, "언짢게 만들었고 ");
		bad.put(3, "불쾌하게 만들었고 ");			bad.put(4, "망쳐버렸고 ");
		bad.put(5, "최악으로 만들어버렸고 ");
		
		emoticon.put("좋음", good);			emoticon.put("나쁨", bad);
		
		df = new SimpleDateFormat("yyyy-MM-dd");
		cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1); 
		yesterday = df.format(cal.getTime());
	}

	@Scheduled(cron="0 5 0 * * *")
//	@Scheduled(cron="*/20 * * * * *")
	public void scheduleRun() {
		
		List<MemberVO> memberList = new ArrayList<>();
		try {
			memberList = mdao.getAllMemberList();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		for (MemberVO memberVO : memberList) {
			try {
				makeDiary(memberVO, "dawn");
				makeDiary(memberVO, "morning");
				makeDiary(memberVO, "day");
				makeDiary(memberVO, "night");
				makeScore(memberVO.getMno());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void makeDiary(MemberVO vo, String timezone) {
		Map<String, Object> map = new HashMap<>();
		map.put("mno", vo.getMno());
		map.put("date",	yesterday);
		List<Map<String, Object>> history = new ArrayList<>();
		try {
			if(timezone=="dawn"){
				System.out.println("새벽꺼 가져 올거시야");
				history = ddao.getOneDayDawn(map);
			}else if(timezone=="morning"){
				System.out.println("아침꺼 가져 올거시야");
				history = ddao.getOneDayMorning(map);
			}else if(timezone=="day"){
				System.out.println("낮꺼 가져 올거시야");
				history = ddao.getOneDayDay(map);
			}else if(timezone=="night"){
				System.out.println("밤꺼 가져 올거시야");
				history = ddao.getOneDayNight(map);
			}
			for (int i = 0; i < history.size(); i++) {
				history.get(i).put("lar_ment", lar.get(history.get(i).get("lar"))+emoticon.get(history.get(i).get("emotion")).get(history.get(i).get("stage")));
				int random = (int) (Math.random() * 3 + 1);
				history.get(i).put("emoticon_ment", history.get(i).get("word"+random));
				if(i==0){
					int stage = Integer.parseInt(history.get(i).get("stage").toString());
					if(stage == 5){
						if(timezone=="dawn"){
							history.get(i).put("time_ment", dawn.get(history.get(i).get("emotion")));
						}else if(timezone=="morning"){
							history.get(i).put("time_ment", morning.get(history.get(i).get("emotion")));
						}else if(timezone=="day"){
							history.get(i).put("time_ment", day.get(history.get(i).get("emotion")));
						}else if(timezone=="night"){
							history.get(i).put("time_ment", night.get(history.get(i).get("emotion")));
						}
					}
				}
				history.get(i).put("regdate", yesterday);
				ddao.insertDiary(history.get(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public void makeScore(String mno){
		ChartVO chartVO  = new ChartVO();
		chartVO.setMno(mno);
		String date = yesterday.substring(8, 10);
		List<Map<String, Object>> history = new ArrayList<>();
		DiaryVO vo = new DiaryVO().setMno(mno).setRegdate(yesterday);
		
		int avg=0, sum=0;
		double good=0, bad=0, hscore=0, escore=0;
		int cnt=0;
		
		try {	// 사용자 합계랑, 이전달까지 평균 합계를 가져와서 비교해야해
			sum = ddao.getSum(vo);
			chartVO = cdao.avgChartNowMonthPreList(chartVO);
			for(int i=1; i<=Integer.parseInt(date); i++){
				Method avgMethod = chartVO.getClass().getDeclaredMethod("getDay"+i);
				avg += (int) avgMethod.invoke(chartVO);
				System.out.println(avg);
			}
			
			hscore = ((double)sum/avg)*100.0 - 100.0;
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		try {
			// 일기 작성 날의 이모티콘 감정 데이터를 가져오고 점수를 계산
			history = ddao.getOneDayEmotion(vo);
			
			for (Map<String, Object> map : history) {
				if(map.get("emotion").toString().equals("좋음")){
					good += Double.parseDouble(map.get("stage").toString());
				}
				else if(map.get("emotion").toString().equals("나쁨")){
					bad += Double.parseDouble(map.get("stage").toString());
 				}
				cnt++;
			}
			
			escore = (good-bad)/cnt;
			
			System.out.println(mno+"님의 점수 결과................합계"+sum+"지난달까지평균"+avg+"좋음"+good+"나쁨"+bad+"소비점수"+hscore+"감정점수"+escore);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		vo.setEscore((int)escore).setHscore((int)hscore).setEcomment(makeEmotionMent(escore)).setHcomment(makeHistoryMent(hscore));
		
		try {
			ddao.insertScore(vo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String makeEmotionMent(double score){
		String ment = new String();
		if(score == 0.0){
			ment = "오늘은 평범한 하루였네요. 내일은 좋은일이 생기길 바래요. ^^";
		} else if(score > 0.0 && score <= 1.0){
			ment = "전체적으로 괜찮은 하루였군요. 내일도 화이팅!";
		} else if(score > 1.0 && score <= 2.0){
			ment = "기분좋은 하루를 보냈네요. 항상 긍정적인 생각하시고 웃으며살아요!";
		} else if(score > 2.0 && score <= 3.0){
			ment = "좋은일이 많았나봐요! 저까지 기분이 좋아지네요~";
		} else if(score > 3.0 && score <= 4.0){
			ment = "와우! 너무 신나는 하루였겠군요? 부러워요~";
		} else if(score > 4.0 && score <= 5.0){
			ment = "환상적인 하루였네요! 글을 읽는 저까지도 날아갈것 같아요~ 내일도 오늘 같길바래요!";
		} else if(score >= -1.0 && score < 0.0){
			ment = "오늘은 별로였나봐요. 항상 기분좋은날만 있는건 아니니깐요~";
		} else if(score >= -2.0 && score < -1.0){
			ment = "오늘 하루는 당황스러운 일이 좀 있었나보군요..";
		} else if(score >= -3.0 && score < -2.0){
			ment = "저런...기분이 안좋았나봐요. 그럴때일수록 밝은생각을 해봐요!";
		} else if(score >= -4.0 && score < -3.0){
			ment = "화가날수록 그 생각을 잊으려고 노력하는것도 좋은방법이에요. 내일은 좋은 일만 가득하길^^";
		} else if(score >= -5.0 && score < -4.0){
			ment = "글을 읽는 저까지도 속상하고 기분이 안 좋네요.. 좋은일이 10배로 돌아오길 빌어요~ 화이팅!";
		}
		
		return ment;
	}
	
	public String makeHistoryMent(double score){
		String ment = new String();
		if (score == 0.0){
			ment="결제 내역을 보니 평범하게 소비를 하셨네요. 조금만 노력하면 이전보다 좋을것 같아요";
		} else if (score >= -5.0 && score < 0.0){
			ment="결제 내역을 보니 괜찮은 소비를 하셨어요. 이런식으로 쭉 가면 소비개선에 많은도움이 될것같아요!";
		} else if (score >= -15.0 && score < -5.0){
			ment="결제 내역을 보니 소비를 잘 하셨군요. 더 잘 할 수 있어요!";
		} else if (score >= -35.0 && score < -15.0){
			ment="결제 내역을 보니 많이 절약하고 계시네요! 노력하는 모습이 멋집니다^.^";
		} else if (score >= -75.0 && score < -35.0){
			ment="결제 내역을 보니 절약하는데 도가 트신것 같은데요?! 너무 보기좋아요^^";
		} else if (score < -75.0){
			ment="결제 내역을 보니 할 말이 없네요.. 완벽하게 잘하고 계셔서요! ^^ 이제 제가 배워야 할 것 같아요! 다른사람들에게도 노하우를 알려주세요~";
		} else if (score > 0.0 && score <= 5.0){
			ment = "결제 내역을 보니 조금 아쉬운 하루였네요. 내일은 좀더 알뜰하게 소비해봐요~";
		} else if (score > 5.0 && score <= 15.0){
			ment = "결제 내역을 보니 지출에 대해서 조금더 신경을 쓸 필요가 있을것같아요";
		} else if (score > 15.0 && score <= 35.0){
			ment = "결제 내역을 보니 소비에 신경을 안쓰고 계세요.ㅠㅠ 계획을 세우고 꼭 필요한것인지 다시한번생각해봐요";
		} else if (score > 35.0 && score <= 75.0){
			ment = "결제 내역을 보니 막무가내식 소비를 하시는 것 같아요..ㅠㅠ 계속 이런식으로 지출하시면 큰일납니다!";
		} else if (score > 75.0){
			ment = "결제 내역을 보니 많이 안좋은데요.. 이런 소비 습관은 시급한 개선이 필요해요. 오늘 하루를 돌아보며 반성하시고 대책을 생각해봅시다. 이렇게 쓰시면 망하는 지름길이에요!!";
		}
		return ment;
	}
	
	
}
